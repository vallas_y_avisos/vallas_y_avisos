require 'test_helper'

class DifficultyQuotesControllerTest < ActionController::TestCase
  setup do
    @difficulty_quote = difficulty_quotes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:difficulty_quotes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create difficulty_quote" do
    assert_difference('DifficultyQuote.count') do
      post :create, difficulty_quote: { lapse: @difficulty_quote.lapse, name: @difficulty_quote.name, quantity: @difficulty_quote.quantity, type_lapse: @difficulty_quote.type_lapse }
    end

    assert_redirected_to difficulty_quote_path(assigns(:difficulty_quote))
  end

  test "should show difficulty_quote" do
    get :show, id: @difficulty_quote
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @difficulty_quote
    assert_response :success
  end

  test "should update difficulty_quote" do
    patch :update, id: @difficulty_quote, difficulty_quote: { lapse: @difficulty_quote.lapse, name: @difficulty_quote.name, quantity: @difficulty_quote.quantity, type_lapse: @difficulty_quote.type_lapse }
    assert_redirected_to difficulty_quote_path(assigns(:difficulty_quote))
  end

  test "should destroy difficulty_quote" do
    assert_difference('DifficultyQuote.count', -1) do
      delete :destroy, id: @difficulty_quote
    end

    assert_redirected_to difficulty_quotes_path
  end
end
