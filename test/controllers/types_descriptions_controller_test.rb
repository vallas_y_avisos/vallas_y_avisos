require 'test_helper'

class TypesDescriptionsControllerTest < ActionController::TestCase
  setup do
    @type_description = types_descriptions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:types_descriptions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create type_description" do
    assert_difference('TypeDescription.count') do
      post :create, type_description: {  }
    end

    assert_redirected_to type_description_path(assigns(:type_description))
  end

  test "should show type_description" do
    get :show, id: @type_description
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @type_description
    assert_response :success
  end

  test "should update type_description" do
    patch :update, id: @type_description, type_description: {  }
    assert_redirected_to type_description_path(assigns(:type_description))
  end

  test "should destroy type_description" do
    assert_difference('TypeDescription.count', -1) do
      delete :destroy, id: @type_description
    end

    assert_redirected_to types_descriptions_path
  end
end
