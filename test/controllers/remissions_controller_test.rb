require 'test_helper'

class RemissionsControllerTest < ActionController::TestCase
  setup do
    @remission = remissions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:remissions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create remission" do
    assert_difference('Remission.count') do
      post :create, remission: {  }
    end

    assert_redirected_to remission_path(assigns(:remission))
  end

  test "should show remission" do
    get :show, id: @remission
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @remission
    assert_response :success
  end

  test "should update remission" do
    patch :update, id: @remission, remission: {  }
    assert_redirected_to remission_path(assigns(:remission))
  end

  test "should destroy remission" do
    assert_difference('Remission.count', -1) do
      delete :destroy, id: @remission
    end

    assert_redirected_to remissions_path
  end
end
