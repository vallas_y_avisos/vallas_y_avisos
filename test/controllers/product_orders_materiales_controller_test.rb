require 'test_helper'

class ProductOrdersMaterialesControllerTest < ActionController::TestCase
  setup do
    @product_orders_material = product_orders_materiales(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:product_orders_materiales)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create product_orders_material" do
    assert_difference('ProductOrdersMaterial.count') do
      post :create, product_orders_material: { quantity: @product_orders_material.quantity }
    end

    assert_redirected_to product_orders_material_path(assigns(:product_orders_material))
  end

  test "should show product_orders_material" do
    get :show, id: @product_orders_material
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @product_orders_material
    assert_response :success
  end

  test "should update product_orders_material" do
    patch :update, id: @product_orders_material, product_orders_material: { quantity: @product_orders_material.quantity }
    assert_redirected_to product_orders_material_path(assigns(:product_orders_material))
  end

  test "should destroy product_orders_material" do
    assert_difference('ProductOrdersMaterial.count', -1) do
      delete :destroy, id: @product_orders_material
    end

    assert_redirected_to product_orders_materiales_path
  end
end
