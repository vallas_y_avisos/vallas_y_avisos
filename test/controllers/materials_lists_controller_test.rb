require 'test_helper'

class MaterialsListsControllerTest < ActionController::TestCase
  setup do
    @materials_list = materials_lists(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:materials_lists)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create materials_list" do
    assert_difference('MaterialsList.count') do
      post :create, materials_list: { cc: @materials_list.cc, date_delivery: @materials_list.date_delivery, date_elaboration: @materials_list.date_elaboration, denomination: @materials_list.denomination, fototelones_value: @materials_list.fototelones_value, length_comp: @materials_list.length_comp, op: @materials_list.op, prepared_by: @materials_list.prepared_by, product_description: @materials_list.product_description, quantity: @materials_list.quantity, size_comp: @materials_list.size_comp, width_comp: @materials_list.width_comp }
    end

    assert_redirected_to materials_list_path(assigns(:materials_list))
  end

  test "should show materials_list" do
    get :show, id: @materials_list
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @materials_list
    assert_response :success
  end

  test "should update materials_list" do
    patch :update, id: @materials_list, materials_list: { cc: @materials_list.cc, date_delivery: @materials_list.date_delivery, date_elaboration: @materials_list.date_elaboration, denomination: @materials_list.denomination, fototelones_value: @materials_list.fototelones_value, length_comp: @materials_list.length_comp, op: @materials_list.op, prepared_by: @materials_list.prepared_by, product_description: @materials_list.product_description, quantity: @materials_list.quantity, size_comp: @materials_list.size_comp, width_comp: @materials_list.width_comp }
    assert_redirected_to materials_list_path(assigns(:materials_list))
  end

  test "should destroy materials_list" do
    assert_difference('MaterialsList.count', -1) do
      delete :destroy, id: @materials_list
    end

    assert_redirected_to materials_lists_path
  end
end
