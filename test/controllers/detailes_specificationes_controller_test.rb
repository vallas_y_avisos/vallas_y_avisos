require 'test_helper'

class DetailesSpecificationesControllerTest < ActionController::TestCase
  setup do
    @detail_specification = detailes_specificationes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:detailes_specificationes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create detail_specification" do
    assert_difference('DetailSpecification.count') do
      post :create, detail_specification: {  }
    end

    assert_redirected_to detail_specification_path(assigns(:detail_specification))
  end

  test "should show detail_specification" do
    get :show, id: @detail_specification
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @detail_specification
    assert_response :success
  end

  test "should update detail_specification" do
    patch :update, id: @detail_specification, detail_specification: {  }
    assert_redirected_to detail_specification_path(assigns(:detail_specification))
  end

  test "should destroy detail_specification" do
    assert_difference('DetailSpecification.count', -1) do
      delete :destroy, id: @detail_specification
    end

    assert_redirected_to detailes_specificationes_path
  end
end
