require 'test_helper'

class SpecificationesControllerTest < ActionController::TestCase
  setup do
    @specification = specificationes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:specificationes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create specification" do
    assert_difference('Specification.count') do
      post :create, specification: { name: @specification.name }
    end

    assert_redirected_to specification_path(assigns(:specification))
  end

  test "should show specification" do
    get :show, id: @specification
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @specification
    assert_response :success
  end

  test "should update specification" do
    patch :update, id: @specification, specification: { name: @specification.name }
    assert_redirected_to specification_path(assigns(:specification))
  end

  test "should destroy specification" do
    assert_difference('Specification.count', -1) do
      delete :destroy, id: @specification
    end

    assert_redirected_to specificationes_path
  end
end
