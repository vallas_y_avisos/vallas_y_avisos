VallaYAviso::Application.routes.draw do
  
  resources :difficulty_quotes

  resources :types_descriptions
  resources :types
  resources :categories
  
  resources :agencies do
    get 'search', on: :collection
  end

  resources :binnacles do
  end

  resources :production_orders do
    get 'search', on: :collection
    get 'quotes_lists', on: :collection
    get 'new_orders', on: :collection
    get 'details', on: :member
    get 'review', on: :member
    get 'sent_to_cost', on: :member
    get 'sent_to_cost_by_price_adjustment', on: :member
    get 'sent_to_production', on: :member
    get 'ended', on: :member
    get 'ended_inventory', on: :member
    get 'ended_machine', on: :member
    get 'ended_warehouse', on: :member
    get 'billing', on: :member     
    get 'billed', on: :member     
    get 'list' => 'production_orders#list'
    resources :production_order_binnacles
  end

  resources :bills do
      get 'search', on: :collection
    end

  resources :remissions do
      get 'search', on: :collection
      get 'sent', on: :member 
    end

  resources :work_orders  do
    get 'search', on: :collection
    get 'deliver', on: :member 
    get 'validate', on: :member 
  end

  resources :quotes do
    get 'search', on: :collection    
    get 'details', on: :member 
    get 'customer_review', on: :member 
    get 'sent_to_cost', on: :member   
    get 'back_to_sales', on: :member 
    get 'canceled', on: :member 
    get 'quote_prices', on: :member 
    put 'sent_seller', on: :member 
    get 'delivered_customer', on: :member 
    get 'approved', on: :member 
    get 'rejected', on: :member 
    get 'cost_report', on: :member 
    get 'costs' => 'quotes#costs', :as => 'costs_registration'  
    put 'costs' => 'quotes#update_costs', :as => 'put_costs_registration'  
    post 'clients' => 'quotes#create_client',   on: :collection      
    get 'list_cost', on: :collection 
  end

  resources :product_orders_materiales do

  end

  resources :products do
    get 'show_category', on: :member 
  end

  resources :product_types do
    get 'show_product', on: :member 
  end 

  resources :products do
    get 'show_category', on: :member 
  end  

  resources :materials_lists do
    get 'sent_to_production', on: :member   
    get 'sent_to_store', on: :member 
  end

  get 'alerts' => 'alerts#show', :as => 'alerts'
  put 'alerts/:id' => 'alerts#update', :as => 'alerts_update'

  resources :departments
  resources :clients
  resources :advisers
  resources :comments 
  resources :materiales
  resources :types_descriptions


  devise_for :administradores, :skip => [:registrations]
    as :administrador do
      get 'administradores/edit' => 'devise/registrations#edit', :as => 'edit_administrador_registration'
      put 'administradores/:id' => 'devise/registrations#update', :as => 'administrador_registration'            
    end
    
  mount RailsAdmin::Engine => '/admin', :as => 'rails_admin'
  
  get 'usuarios/sign_out' => 'devise/sessions#destroy', as: :logout

   namespace :admin do
    resources :user
  end


   
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
