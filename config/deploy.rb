# config valid only for current version of Capistrano
lock '3.6.1'

set :application, 'vallas_y_avisos'
# TODO: change to use ssh keys
set :repo_url, 'git@bitbucket.org:vallas_y_avisos/vallas_y_avisos.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/home/app/vallas_y_avisos'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: 'log/capistrano.log', color: :auto, truncate: :auto

# Default value for :pty is false
set :pty, true

set :rvm_type, :user  
set :rvm_ruby_version, '2.0.0@vayas_y_avisos'

# Default value for :linked_files is []
append :linked_files, 'config/database.yml', 'config/secrets.yml'

# Default value for linked_dirs is []
# append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system'

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 2

set :slackistrano, {
 channel: '#general',
 webhook: 'https://hooks.slack.com/services/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'
}

desc 'Restart application'
task :restart do
  on roles(:app), in: :sequence, wait: 5 do
    execute :touch, release_path.join('tmp/restart.txt')
  end
end

after 'deploy:log_revision', 'restart'
