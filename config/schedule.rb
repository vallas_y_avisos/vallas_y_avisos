# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
every 10.minutes do
  rake "alerts:alerts_sent_to_cost"
end

every 15.minutes do
  rake "alerts:alerts_alerts_approved"
end

every 10.minutes do
  rake "alerts:alerts_delivered_customer"
end

every 1.days do
  rake "alerts:alerts_users_admin"
end

every 1.days do
  rake "alerts:alerts_users"
end

