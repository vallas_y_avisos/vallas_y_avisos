require File.expand_path('../boot', __FILE__)

require 'rails/all'


# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module VallaYAviso
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Bogota'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}').to_s]

    config.assets.paths << "#{Rails.root.to_s}/vendor/assets/stylesheets/theme/fonts"
    
    config.i18n.default_locale = :es

    config.serve_static_assets = true

    # Papiro Url para lanzar los expedientes
    #config.papiro_url = "http://papiro.rastrea.la"

    config.default_mail = "admin@vallasyavisos.com"


    Time::DATE_FORMATS[:default] = "%d/%m/%Y %I:%M %p"
    Date::DATE_FORMATS[:default] = "%d/%m/%Y"
    
    config.autoload_paths += %W(
      #{config.root}/app/decorators,
      #{config.root}/lib
    )
    
    config.to_prepare do
      Devise::SessionsController.layout "devise"
    #  Devise::RegistrationsController.layout proc { |c| (usuario_signed_in? || administrador_signed_in?) ? "application" : "devise" }
      Devise::RegistrationsController.layout proc { |c| (administrador_signed_in?) ? "application" : "devise" }
      Devise::ConfirmationsController.layout "devise"
      Devise::UnlocksController.layout "devise"
      Devise::PasswordsController.layout "devise"
    end

    # Configuración imagemagick, colocar aquí la salida de: which convert
    Paperclip.options[:command_path] = "/usr/bin/convert"

    #require 'pdfkit'
    #config.middleware.use "PDFKit::Middleware"
  end
end
