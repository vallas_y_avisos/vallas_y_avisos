# Fix compatibilidad del anmespace de papertrails en v 3.0.0-rc2
PaperTrail::Version.module_eval do
  self.abstract_class = true
end

class Version < PaperTrail::Version
  self.table_name = :versions
  self.sequence_name = :versions_id_seq
end