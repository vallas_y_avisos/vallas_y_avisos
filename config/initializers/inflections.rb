# Be sure to restart your server when you modify this file.

# Add new inflection rules using the following format. Inflections
# are locale specific, and you may define rules for as many different
# locales as you wish. All of these examples are active by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
#   inflect.uncountable %w( fish sheep )
# end

# Spanish inflector rules
ActiveSupport::Inflector.inflections(:en) do |inflect|
  inflect.plural /([aeiou])([A-Z]|_|$)/, '\1s\2'
  inflect.plural /([rlnd])([A-Z]|_|$)/, '\1es\2'
  inflect.plural /([aeiou])([A-Z]|_|$)([a-z]+)([rlnd])($)/, '\1s\2\3\4es\5'
  inflect.plural /([rlnd])([A-Z]|_|$)([a-z]+)([aeiou])($)/, '\1es\2\3\4s\5'
  inflect.plural /([rlnd])([A-Z]|_|$)([a-z]+)([rlnd])($)/, '\1es\2\3\4es\5'

  inflect.singular /([aeiou])s([A-Z]|_|$)/, '\1\2'
  inflect.singular /([rlnd])es([A-Z]|_|$)/, '\1\2'
  inflect.singular /([aeiou])s([A-Z]|_)([a-z]+)([rlnd])es($)/, '\1\2\3\4\5'
  inflect.singular /([rlnd])es([A-Z]|_)([a-z]+)([aeiou])s($)/, '\1\2\3\4\5'
  inflect.singular /([rlnd])es([A-Z]|_)([a-z]+)([rlnd])es($)/, '\1\2\3\4\5'

  inflect.irregular 'agency', 'agencies'  
  inflect.irregular 'order', 'orders'  
  inflect.irregular 'adviser', 'advisers'  
  inflect.irregular 'remission', 'remissions'
  inflect.irregular 'remission_detail', 'remission_details'
  inflect.irregular 'work_order_detail', 'work_order_details'
  inflect.irregular 'user', 'users'
  inflect.irregular 'production_order_binnacle', 'production_order_binnacles'
  inflect.irregular 'production_order_anexe', 'production_order_anexes'
  inflect.irregular 'production_order_comment', 'production_order_comments'
  inflect.irregular 'work_order_comment', 'work_order_comments'
  inflect.irregular 'production_order_binnacle_anexe', 'production_order_binnacle_anexes'
  inflect.irregular 'bill', 'bills'
  inflect.irregular 'bill_detail', 'bill_details'
  inflect.irregular 'type_description', 'types_descriptions'
  inflect.irregular 'quote_detail_type', 'quote_details_types'
  inflect.irregular 'quote_detail', 'quote_details'
  inflect.irregular 'statu_quotes', 'status_quotes'
  inflect.irregular 'history_order_production_status', 'history_order_production_statuses'
  inflect.irregular 'binnacle', 'binnacles'
  

  
 

end

# These inflection rules are supported but not enabled by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.acronym 'RESTful'
# end
