source 'https://rubygems.org'

ruby "2.0.0"
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.1'

# Use postgresql as the database for Active Record
gem 'pg'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

gem 'acts_as_commentable'

# Devise
gem "devise"
# gem "protected_attributes"
gem 'devise_security_extension', :git => 'https://github.com/phatworx/devise_security_extension.git'
# gem "easy_captcha"

# Simple forms
gem "simple_form"

gem 'datetimepicker-rails', github: 'zpaulovics/datetimepicker-rails', branch: 'master', submodules: true

# Rails Admin
gem 'rails_admin' #, github: "sferik/rails_admin"

# Rails Bootstrap
# gem 'anjlab-bootstrap-rails', :require => 'bootstrap-rails',
                              # :github => 'anjlab/bootstrap-rails'

# Use CanCan for Authorization
gem "cancan"

# Use PaperTrail for auditing
gem 'paper_trail', '>= 3.0.0.rc2'

# Use Paperclip for upload files
gem 'paperclip'

# Use AASM for Ruby state machines
gem 'aasm'

# foreign key helper
gem 'foreigner'

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.1.2'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]

# Handle nested forms
gem 'cocoon', '~> 1.2.5'

# Rails Template Handler
gem 'thinreports-rails'

# HTML to PDF
#gem 'pdfkit', '~> 0.6.0'
gem 'wkhtmltopdf-binary'
gem 'wicked_pdf'

gem 'rails_12factor', group: :production

gem 'chart-js-rails'

gem 'whenever'

group :development do
  gem 'capistrano', '~> 3.6'
  gem 'capistrano-rails', '~> 1.2'
  gem 'capistrano-rvm'
  gem 'capistrano-bundler', '~> 1.2'
  gem 'slackistrano'
  gem "highline"
  gem 'byebug'
end

gem 'puma'

gem "business_time"
