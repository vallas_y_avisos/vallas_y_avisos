
$(document).on('ready page:load', function () { 
 
  $(".field-select").select2({
    placeholder: "Seleccione",
    allowClear: true
  });

  $('#remission_production_order_id').change(function(){ 

      if($(this).val()) {
        $.get('/production_orders/'+$(this).val()+'/details.json',
          function(data) {                                                                      
            $("#remission_client_name").val(data.client_name);                               
            $("#remission_client_phone").val(data.client_phone);
            $("#remission_client_adress").val(data.client_address);
            $("#remission_reference").val(data.quote_reference);
          });
        }
  });
});

