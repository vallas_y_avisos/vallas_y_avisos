$(document).on('ready page:load', function () { 

  /*$(".order_product").toggle("slow");*/

    $(".field-select").select2({
      placeholder: "Seleccione",
      allowClear: true
    });   

   /*busca del cliente*/
    $('#materials_list_client').change(function(){ 
      if($(this).val()){
        $.get('/clients/'+$(this).val()+'.json',
          function(data) {             
            $("#materials_list_client_nit").val(data.nit);  
          });
        $.get('/production_orders/'+$(this).val()+'/list',
          function(data) {             
            /*$(".order_product").toggle("fast");*/
            $('#materials_list_production_order_id').empty();
            $('#materials_list_production_order_id').append("<option value=''>Seleccione la Orden de Produccion</option>");
            $.each(data, function(key, element) {
              //console.log(element.number);
              $('#materials_list_production_order_id').append("<option value='" + element.id + "'>"+ "N#" + element.number + " Fecha: " + element.date_elaboration +"</option>");
            });
          });          
      }
    });

    /*fin del cliente*/  

      $('#materials_list_quantity').keyup(function() {
        if($(this).val()<0){
          alert( "Debe Ingresar un Numero Positivo" );
          $( "#materials_list_quantity" ).val('');            
          $( "#materials_list_quantity" ).focus();
         }
      });
      //*Validando campos numericos*/

      $('#materials_list_quantity').click(function() {
        if($(this).val()<0){
          alert( "Debe Ingresar un Numero Positivo" );
          $( "#materials_list_quantity" ).val('');          
          $( "#materials_list_quantity" ).focus();
         }
      });   

      $('#materials_list_length_comp').keyup(function() {
        if($(this).val()<0){
          alert( "Debe Ingresar un Numero Positivo" );
          $( "#materials_list_length_comp" ).val('');            
          $( "#materials_list_length_comp" ).focus();
         }
      });

      $('#materials_list_length_comp').click(function() {
        if($(this).val()<0){
          alert( "Debe Ingresar un Numero Positivo" );
          $( "#materials_list_length_comp" ).val('');          
          $( "#materials_list_length_comp" ).focus();
         }
      });  


      $('#materials_list_width_comp').keyup(function() {
        if($(this).val()<0){
          alert( "Debe Ingresar un Numero Positivo" );
          $( "#materials_list_width_comp" ).val('');            
          $( "#materials_list_width_comp" ).focus();
         }
      });

      $('#materials_list_width_comp').click(function() {
        if($(this).val()<0){
          alert( "Debe Ingresar un Numero Positivo" );
          $( "#materials_list_width_comp" ).val('');          
          $( "#materials_list_width_comp" ).focus();
         }
      });  


      $('#quantity_material').keyup(function() {
        if($(this).val()<0){
          alert( "Debe Ingresar un Numero Positivo" );
          $( "#quantity_material" ).val('');            
          $( "#quantity_material" ).focus();
         }
      });

      $('#quantity_material').click(function() {
        if($(this).val()<0){
          alert( "Debe Ingresar un Numero Positivo" );
          $( "#quantity_material" ).val('');          
          $( "#quantity_material" ).focus();
         }
      });        



//*Fin Validando campos numericos*/

   /*busco los datos de la orden de produccion*/
    $('#materials_list_production_order_id').change(function(){ 
      if($(this).val()){
        $.get('/production_orders/'+$(this).val()+'/details.json',
          function(data) { 
          //console.log(data);
            $("#materials_list_adviser_name").val(data.adviser_name);  
            $("#materials_list_product_description").val(data.quote_reference); 
            
          });          
      }
    });



    /*Autocompletar*/
    $(function() {   
      var cadena = '[';
      $.get('/materiales.json',
        function(data) { 
          $.each(data, function(key,element) { 
          var material_id = element.url.split('/');
              material_id = material_id[4].split('.json');                                   
            cadena = cadena + '{"label":"'+element.description+'","id":'+material_id[0]+'},';
          });
          cadena = cadena + '{}]';    
          parsedTest = JSON.parse(cadena); 
          $( "#material" ).autocomplete({
            source: parsedTest
        });       
      });
    });
    /*fin de autocompletar*/   

      /*Agregar Detalle*/
    $(".add-item-materiales").click(function () {      
      var trs = ($('#tabla >tbody >tr').length)-1;
      var material = $("#material").val();
      var unit_material = $("#materials_list_add_unit").val(); 
      var cantidad = $("#quantity_material").val();
      var description = $("#description").val(); 
      var observation = $("#observation").val(); 
      var reference = $("#reference").val(); 
     
      if(cantidad){
        if(material){          
          //var bloque =  '<tr><td><div class="nested-fields well detalle"><div class="row-fluid"><div class="span12"><div class="control-group string optional materials_list_materials_name"><label class="string optional control-label" for="materials_list_materiales_details_attributes_'+trs+'_materials_name">Material</label><div class="controls"><input class="string optional width-order-large" id="materials_list_materiales_details_attributes_'+trs+'_materials_name" name="materials_list[materiales_details_attributes]['+trs+'][materials_name]" readonly="readonly" type="text" value="'+material+'" /></div></div></div></div><div class="row-fluid"><div class="span6"><div class="control-group integer optional materials_list_quantity"><label class="integer optional control-label" for="materials_list_materiales_details_attributes_'+trs+'_quantity">Cantidad</label><div class="controls"><input class="numeric integer optional" id="materials_list_materiales_details_attributes_'+trs+'_quantity" name="materials_list[materiales_details_attributes]['+trs+'][quantity]" readonly="readonly" step="1" type="number" value="'+cantidad+'" /></div></div></div><div class="span6"><div class="control-group string optional materials_list_materials_unit"><label class="string optional control-label" for="materials_list_materiales_details_attributes_'+trs+'_materials_unit">Unidad</label><div class="controls"><input class="string optional" id="materials_list_materiales_details_attributes_'+trs+'_materials_unit" name="materials_list[materiales_details_attributes]['+trs+'][materials_unit]" readonly="readonly" type="text" value="'+unit_material+'" /></div></div></div></div><hr><div class="row-fluid"><div class="span12"><div class="control-group text optional materials_list_description"><label class="text optional control-label" for="materials_list_materiales_details_attributes_'+trs+'_description">Descripcion</label><div class="controls"><textarea class="text optional width-order-large" id="materials_list_materiales_details_attributes_'+trs+'_description" name="materials_list[materiales_details_attributes]['+trs+'][description]">'+description+'</textarea></div></div></div></div><div class="row-fluid"><div class="span12"><div class="control-group text optional materials_list_observation"><label class="text optional control-label" for="materials_list_materiales_details_attributes_'+trs+'_observation">Observaciones</label><div class="controls"><textarea class="text optional width-order-large" id="materials_list_materiales_details_attributes_'+trs+'_observation" name="materials_list[materiales_details_attributes]['+trs+'][observation]">  '+observation+'</textarea></div></div></div></div><div class="row-fluid"><div class="span12"><div class="control-group text optional materials_list_materials_reference"><label class="text optional control-label" for="materials_list_materiales_details_attributes_'+trs+'_materials_reference">Referencia</label><div class="controls"><textarea class="text optional width-order-large" id="materials_list_materiales_details_attributes_'+trs+'_materials_reference" name="materials_list[materiales_details_attributes]['+trs+'][materials_reference]">'+reference+'</textarea></div></div></div></div><div class="row-fluid"><div class="span11"></div><div class="span1"><a class="borrar dynamic" ><i class="icon-remove"></i></a></div></div><hr></div></td></tr>';  
          var input_material =  '<div class="control-group text optional materials_list_materials_name"><div class="controls"><textarea  id="materiales_details_attributes'+trs+'_materials_name" name="materials_list[materiales_details_attributes]['+trs+'][materials_name]"  class="text optional width-order-large">'+material+'</textarea></div></div>';
          var input_cantidad =  '<div class="control-group integer optional materials_list_quantity"><div class="controls"><input  type="number" class="numeric integer optional" id="materiales_details_attributes'+trs+'_quantity" name="materials_list[materiales_details_attributes]['+trs+'][quantity]" value="'+cantidad+'"></div></div>';
          var input_unit_material = '<div class="control-group string optional materials_list_materials_unit"><div class="controls"><input  id="materiales_details_attributes'+trs+'_materials_unit" name="materials_list[materiales_details_attributes]['+trs+'][materials_unit]" value="'+unit_material+'" type="text" class="string optional"></div></div>';
          var input_description =  '<div class="control-group text optional materials_list_description"><div class="controls"><textarea id="materiales_details_attributes'+trs+'_description" name="materials_list[materiales_details_attributes]['+trs+'][description]" class="text optional width-order-large" >'+description+'</textarea></div></div>';
          var input_observation =  '<div class="control-group text optional materials_list_observation"><div class="controls"><textarea  id="materiales_details_attributes'+trs+'_observation" name="materials_list[materiales_details_attributes]['+trs+'][observation]" class="text optional width-order-large" >'+observation+'</textarea></div></div>';
          var input_reference =  '<div class="control-group text optional materials_list_materials_reference"><div class="controls"><textarea   id="materiales_details_attributes'+trs+'_materials_reference" name="materials_list[materiales_details_attributes]['+trs+'][materials_reference]" class="text optional width-order-large">'+reference+'</textarea></div></div>';
          //var fila = '<tr><td>'+cantidad+input_cantidad+input_unit_material+input_material+input_description+input_observation+input_reference+'</td><td>'+unit_material+'</td><td>'+material+'</td><td>'+description+'</td><td>'+observation+'</td><td>'+reference+'</td><td ><a class="borrar dynamic" ><i class="icon-remove"></i></a></td></tr>';
          var fila = '<tr><td>'+input_material+'</td><td>'+input_unit_material+'</td><td>'+input_cantidad+'</td><td>'+input_description+'</td><td>'+input_observation+'</td><td>'+input_reference+'</td><td ><a class="borrar dynamic" ><i class="icon-remove"></i></a></td></tr>';
          $('#tabla >tbody >tr:last').after(fila);
          $("#material").val('');  
          $("#quantity_material").val(''); 
          $("#materials_list_add_unit").val(''); 
          $("#description").val(''); 
          $("#observation").val('');
          $("#reference").val('');  
          //console.log(bloque);
        /*  $('#tabla >tbody >tr:last').after(bloque);
          $("#material").val('');  
          $("#quantity_material").val(''); 
          $("#materials_list_add_unit").val(''); 
          $("#description").val(''); 
          $("#observation").val('');
          $("#reference").val(''); */ 
          }else{
            alert('Ingrese el Nombre del Material');
          }
      }else{
       alert('Ingrese la Cantidad');
      }
      return false;
    });
    /*fin agregar detalle*/ 
   //new_materials_list

  $(".new_materials_list > .icon-remove").delegate('.borrar','click',function (){ 
    $(this).parents('.detalle').remove();
  });
  
  $(".edit_materials_list > .icon-remove").click(function (){ 
    $(this).parents('.detalle').remove();
    return false;
  }); 


    $(".new_materials_list > .guardar-materials-lists").click(function () {
     // alert('entro');
      if(!$("#materials_list_production_order_id").val())
      {
        alert('Debe Seleccionar la orden de Produccion');
        return false;
      }
      if(!$("#materials_list_denomination").val())
      {
        alert('Debe Ingresar la Denominacion');
        return false;
      }
      if(!$("#materials_list_prepared_by").val())
      {
        alert('Debe Ingresar por quien fue preparado');
        return false;
      }
      if(!$("#materiales_details_attributes0_quantity").val())
      {
        alert('Debe Ingresar al menos un material');
        return false;
      }
      
    });

    $(".edit_materials_list > .guardar-materials-lists").click(function () {
      //alert('entro');

      if(!$("#materials_list_production_order_id").val())
      {
        alert('Debe Seleccionar la orden de Produccion');
        return false;
      }
      if(!$("#materials_list_denomination").val())
      {
        alert('Debe Ingresar la Denominacion');
        return false;
      }
      if(!$("#materials_list_prepared_by").val())
      {
        alert('Debe Ingresar por quien fue preparado');
        return false;
      }

     if($("#materials_list_materiales_details_attributes_0_materials_name").length == 0)
      {
        alert('Debe Ingresar al menos un material');
        return false;
      }
      
    });


  /*fin del document*/  
  //    edit_materials_list
  });