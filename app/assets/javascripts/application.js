// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require layout/libs
//= require turbolinks
//= require cocoon
//= require highcharts/highcharts
//= require fileinput
//= require fileinput_locale_es
//= require moment.min
//= require locale/es
//= require_tree .
//= require_self
//= require_select2
//= require_datatable
//= require Chart
//= require loading_spinner
//= require bootstrap-datetimepicker
//= require pickers


function listaralertas(){
    $.ajax({
        url: "/alerts",
        dataType: "JSON",
        timeout: 10000,
        success: function(res){          
          $.each(res, function(index) {                         
              $(".count-alerts").html(res.length);
              var cotizacion = $.strPad(res[index].id, 6, '0');
              if(res[index].description && res[index].type_alert=='Recordatorio'){                              
                $("#recordatorio").append('<li id='+res[index].id+'><a  href="#"><i class="icon-remove"></i> Cotizacion N# '+cotizacion+' <a  href="/quotes/'+res[index].model_id+'/edit" class="alert-menu" >'+res[index].description+'</a></li>');
              };
              if(res[index].description && res[index].type_alert=='Orden-Produccion'){                              
                $("#ordenes").append('<li id='+res[index].id+'><a  href="#"><i class="icon-remove"></i> Cotizacion N# '+cotizacion+' <a href="/quotes/'+res[index].model_id+'/edit" class="alert-menu" >'+res[index].description+'</a></li>');
              };
              if(res[index].description && res[index].type_alert=='Tiempo-Aprobar'){                              
                $("#tiempos").append('<li id='+res[index].id+'><a  href="#"><i class="icon-remove"></i> Cotizacion N# '+cotizacion+' </a><a href="/quotes/'+res[index].model_id+'/edit" class="alert-menu" >'+res[index].description+' N# de Dias=> '+res[index].days+'</a></li>');
              };
          });
        }
     });
};


$(document).on('ready page:load', function () { 

  $.strPad = function(i,l,s) {
    var o = i.toString();
    if (!s) { s = '0'; }
    while (o.length < l) {
      o = s + o;
    }
    return o;
  };

  listaralertas();   

 });
