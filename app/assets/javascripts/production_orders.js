//# Place all the behaviors and hooks related to the matching controller here.
//# All this logic will automatically be available in application.js.
//# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on('ready page:load', function () {

  $('.production_order_anexes')
    .on('cocoon:after-insert', function() {
      $('.file').fileinput({
        language: 'es',
        showUpload: false,
      });
    })

 	$('#production_order_client_id').change(function(){ 
      if($(this).val())         {  
        $.get('/clients/'+$(this).val()+'.json',
          function(data) {                                       
            $("#production_order_nit_client").val(data.nit);                               
            $("#production_order_address_client").val(data.address);                               
            $("#production_order_phone_client").val(data.phone);                               
          });
        }
  });

    $('#tablequoteorder').DataTable({
        "ordering": false,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No hay registros encontrados",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros activos",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sSearch":        "Buscar:",
            "oPaginate": {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
            }
        }
    });
    
    $("#search_number, #search_client, #search_adviser, #search_agency").select2({
      allowClear: true
    });
   	
    $(".field-select").select2({
      placeholder: "Seleccione",
      allowClear: true
    });

    $('#production_order_actual_delivey_date').datetimepicker();

    $("#tabla_productos .quantity").change(function() {
      var total = 0;
      $('#tabla_productos .quantity').each(function(){
        total += parseFloat($(this).val()) * parseFloat($(this).parent().parent().parent().children().first().text());
      });
      $('.table-total-general').text(total+" COP")
    });

    $("#tabla_productos .price").change(function() {
      var total = 0;
      
      total += parseFloat($(this).val()) * parseFloat($(this).parent().parent().parent().prev().find("input").val());

      $(this).parent().parent().parent().next().text(total+" COP");
    });

});

