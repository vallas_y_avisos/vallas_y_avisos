//  $(document).ready(function() {

  
$(document).on('ready page:load', function () { 

    $('.quotes_anexes')
      .on('cocoon:after-insert', function() {
        $('.file').fileinput({
          language: 'es',
          showUpload: false,
        });
      })

    $('#tablequotes').DataTable({
        "ordering": false,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No hay registros encontrados",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros activos",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sSearch":        "Buscar:",
            "oPaginate": {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
            }
        }
    });

    $('#quote_type_descriptions').select2();
    
    $("#search_number, #search_client, #search_adviser, #search_agency").select2({
      allowClear: true
    });    

    $(".field-select").select2({
      placeholder: "Seleccione",
      allowClear: true
    }); 

    $('#client_id').change(function(){ 
        if($(this).val())         {
          $.get('/clients/'+$(this).val()+'.json',
            function(data) {              
              $("#quote_client_email").val(data.email);                               
              $("#quote_client_contact").val(data.contact);                               
              $("#quote_client_office").val(data.office);                         
              $("#quote_client_phone").val(data.phone);  
              $("#quote_client_address").val(data.address);  
              $("#quote_client_nit").val(data.nit);  
              });
        }
    });

    $('#modal-botton').click(function () {
      $('#myModal').removeClass("modal-position")
    });

    $('#adviser_id').change(function(){ 
        if($(this).val())         {
          $.get('/advisers/'+$(this).val()+'.json',
            function(data) {
            //console.log(data);
              $('#quote_assessor_code').empty();                           
              $("#quote_assessor_code").val(data.assessor_code);                               
            });
        }
    });   

/*validar la cantidad*/

      $('#quote_search_quantity').keyup(function() {
        if($(this).val()<0){
          alert( "Debe Ingresar un Numero Positivo" );
          $( "#quote_search_quantity" ).val('');            
          $( "#quote_search_quantity" ).focus();
         }
      });

  /*fin validar la cantidad*/    

  /*Registro de Cliente*/
 $(".add_client").click(function () {
    var direccion = $("#client_address").val();
    var client = '{"client":{"name":"'+$("#client_name").val()+'", "nit":"'+$("#client_nit").val()+'", "contact":"'+$("#client_contact").val()+'", "office":"'+$("#client_office").val()+'", "email":"'+$("#client_email").val()+'", "phone":"'+$("#client_phone").val()+'", "address":"'+direccion+'"}}';            
    client = JSON.parse(client);    
    //console.log(client);
      
      $.ajax({
        url:  "/clients", 
        data: client,
        type: "POST",              
        dataType: "json",
          success: function(response) {
           
            $.get('/clients/'+response.id+'.json',
              function(data) { 
                var opt = "<option value=" +response.id+ " selected ='selected'>" + data.name+ " </option>";
                $(".cliente").html(opt); 
                $("#quote_client_email").val(data.email);                               
                $("#quote_client_contact").val(data.contact);                               
                $("#quote_client_office").val(data.office);                         
                $("#quote_client_phone").val(data.phone);  
                $("#quote_client_address").val(data.address);  
                $("#quote_client_nit").val(data.nit);  
              });
            $('.client .control-group').removeClass("error");
            $('.client .control-group input').val("")            
            $('#myModal').modal('toggle');
            alert('El Cliente fue creado satisfactoriamente');                       
          },
          error: function(xhr, textStatus, errorThrown) {

            $('.client .control-group').removeClass("error");
            $.each(xhr.responseJSON, function(i, item) {
              $(".client_"+i).addClass("error")
            });
            alert('Hay errores en el formulario, por favor verifique');

          }
      });      
    return false;    
  }); 

  /*Agregar Detalle*/
    $(".add-item").click(function () {
      /*busco los valores de todos los select*/
      var $select = jQuery('#detalle-producto'),
      $select = $select.find('select');  
      $cadena_detalle = 'Especificaciones: ';

      selected = $('#quote_type_descriptions').select2('data');

      $.each(selected, function(key, element) { 
          $cadena_detalle += element.text + ", ";
      });
      if($cadena_detalle=='Especificaciones: ') $cadena_detalle = '';

      var msj = 'Debe seleccionar';

      //console.log('aqui',$('#adviser_id').val());

      if($('#client_id').length > 0 && $('#quote_method_payment').length > 0){

        var cliente = $('#client_id').val();
        var forma_de_pago = $('#quote_method_payment').val();
        //var product_id = $("#product_id").val();

        var quote_commission_quote = $("#quote_commission_quote").val(); 
        var quote_reference = $("#quote_reference").val(); 
        var quote_form_delivery = $("#quote_form_delivery").val(); 

        if(!cliente) msj = msj + '  el cliente ';
        if(!forma_de_pago) msj = msj + ' la Forma de Pago ';
        if(!quote_commission_quote) msj = msj + ' la Comision ';
        if(!quote_reference) msj = msj + ' la Referencia ';

      }

      var product_id = $("#quote_search_product").val();
      var cantidad = $(".cantidad").val();
      var descripcion = $("#description").val();

      if (msj == 'Debe seleccionar'){
        if(cantidad){
          if(product_id){
            var product_precio = $("#productPrice").val(); 
            
            if($("#quote_type").val())
              var quote_type = $( "#quote_type option:selected" ).text();
            else
               var quote_type = '';
            //var product_name = $("#productName").val(); 
            var product_name = $( "#quote_search_product option:selected" ).text();
            if(quote_type!='') product_name += ', '+ quote_type
              if($cadena_detalle!='') product_name += ', '+ $cadena_detalle
            if(!product_precio) product_precio = 0;     
            var valor_total = product_precio * cantidad;
            var trs = ($('#tabla >tbody >tr').length);
            var input_product_description =  '<input type="hidden" class="pro-id" id="quote_quote_details_attributes_'+trs+'_description" name="quote[quote_details_attributes]['+trs+'][description]" value="'+descripcion+'">';
            var input_product_cantidad =  '<input type="hidden" id="quote_quote_details_attributes_'+trs+'_quantity" name="quote[quote_details_attributes]['+trs+'][quantity]" value="'+cantidad+'">';
            var input_product_precio =  '<input type="hidden" id="quote_quote_details_attributes_'+trs+'_price" name="quote[quote_details_attributes]['+trs+'][price]" value="'+product_precio+'">';
            var input_product_product_id =  '<input type="hidden" id="quote_quote_details_attributes_'+trs+'_product_id" name="quote[quote_details_attributes]['+trs+'][product_id]" value="'+product_id+'">';
            var input_especificaciones =  '<input type="hidden" id="quote_quote_details_attributes_'+trs+'_specificationes" name="quote[quote_details_attributes]['+trs+'][specificationes]" value="'+product_name+'">';
            //console.log(input_product_id, 'producto id');
            var fila = '<tr><td>'+product_name+'</td><td>'+descripcion+'</td><td>'+cantidad+input_product_cantidad+input_product_description+input_product_precio+input_product_product_id+input_especificaciones+'</td><td ><a class="borrar dynamic" ><i class="icon-remove"></i></a></td></tr>';
            $('.table-quote-details >tbody').append(fila);
            $("#description").val('');  
            $(".cantidad").val(''); 
            $("#product").val('');      
            $("#quote_type_descriptions").select2("val", "");
            }else{
              alert('Seleccione el Producto')
            }
        }else{
         alert('Ingrese la Cantidad')
        } 
      }else{
         alert(msj)
      } 
      return false;
    });
    /*fin agregar detalle*/

    //$(".remove_fields").click(function(){
    $("#tabla").delegate('.borrar','click',function (){ 
      $(this).parents('tr').remove();
    });

    //$(".remove_fields").click(function(){
    $(".icon-remove").click(function (){ 
      $(this).parents('tr').remove();
      return false;
    });   

    $('#category_id').change(function(){
     $.get('/products/'+$(this).val()+'/show_category.json',     
        function(data) {
            //console.log(data);
            $('#quote_search_product').empty();
            $('#quote_search_product').append("<option value=''>Seleccione el Producto</option>");
            $.each(data, function(key, element) {
                //console.log(element.nombre);
                $('#quote_search_product').append("<option value='" + element.id + "'>" + element.name + "</option>");
            });
        });
    });
    

    $('#quote_search_product').change(function(){
     $.get('/product_types/'+$(this).val()+'/show_product.json',     
        function(data) {          
            $('#quote_type').empty();
            $('#quote_type').append("<option value=''>Seleccione el Tipo</option>");
            $.each(data, function(key, element) {        
                $('#quote_type').append("<option value='" + element.id + "'>" + element.name + "</option>");
            });
        });
    }); 

    $('#quote_type').change(function(){
     $.get('/types_descriptions.json?type_id='+$(this).val(),
        function(data) {          
            $('#quote_type_descriptions').empty();
            $.each(data, function(key, element) {        
                $('#quote_type_descriptions').append("<option value='" + element.id + "'>" + element.description + "</option>");
            });
            $('#quote_type_descriptions').select2();
        });
    }); 


    /*fin de la funcion para armar el detalle*/ 


   /*edit de quote*/
 
    function pad (str, max) {
      if(str){
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
      }else{
        return false;
      }
   }           

  // $('#quote_number').val(pad($('#quote_id').val(),6)); 

  /*Gestion de Costos*/


  /*calcular total*/  

  $(".price").blur(function(){    
    var i = $(this).closest('tr').attr("id") ;   
    var cantidad = $("."+i).val();    
    var total =  parseFloat($(this).val() *  cantidad);
    $("#total"+i).text(formatNumber(total) +' COP');
    var total_g = parseFloat($(".total_general").text()); 
    if(!total_g) total_g = 0;
    totalizar(); 
    //var valor = formatNumber($(this).val());
    //$(".price").text(valor);
  });

  function formatNumber(number){

    var str = number + ''; 
    x = str.split('.'); 
    x1 = x[0]; x2 = x.length > 1 ? '.' + x[1] : ''; 
    var rgx = /(\d+)(\d{3})/; 
    while (rgx.test(x1)) { 
        x1 = x1.replace(rgx, '$1' + '.' + '$2'); 
    } 
    return x1 + x2 

  }


   function totalizar(){
    var total_product = $('#tabla >tbody >tr').length; 
    var total_general_product = 0;
    for (var i=0; i<total_product; i++) {
      var cantidad = $("."+i).val();    
      var precio =  $("#quote_quote_details_attributes_"+i+"_price").val();
      var total = parseFloat(cantidad) * parseFloat(precio)
      total_general_product+=total;     
    }   
    total_general_product = total_general_product || 0
    $(".total_general").text(formatNumber(total_general_product)+' COP'); 
    
  };
  
  totalizar();

  /*fin Gestion de Costos*/

   // console.log($("#quote_quote_details_attributes_0_id").length, 'aqui');

      $(".new_quote > .guardar-quote").click(function () {
      //alert('entro');
    
        if($(".table-quote-details > tbody > tr").length==0)
        {
          alert('Debe Ingresar al menos un Producto');
          return false;
        }
      
      }); 

      $(".edit_quote > .guardar-quote").click(function () {
      
       if($(".table-quote-details > tbody > tr").length==0)
        {
          alert('Debe Ingresar al menos un Producto');
          return false;
        }
      
      });       

  $('#new_quote').submit(function() {
    $(this).find("button[type='submit']").prop('disabled',true);
  });
      

  /*fin del document*/      
});

