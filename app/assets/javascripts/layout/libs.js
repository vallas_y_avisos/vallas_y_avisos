// This is a manifest file that'll be compiled into libs.js, which will include all the files
// listed below.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery-migrate.min
//= require theme/scripts/plugins/system/jquery-ui-touch-punch/jquery.ui.touch-punch.min
//= require theme/scripts/plugins/system/modernizr
//= require theme/scripts/plugins/other/jquery-slimScroll/jquery.slimscroll.min
//= require theme/scripts/plugins/other/holder/holder
//= require theme/scripts/plugins/forms/pixelmatrix-uniform/jquery.uniform.min
//= require theme/scripts/plugins/other/jquery.ba-resize
//= require theme/scripts/plugins/charts/easy-pie/jquery.easy-pie-chart
//= require jquery.bootstrap.wizard
//= require dependent_select
//= require_tree .
