
$(document).on('ready page:load', function () { 

  var seller_count = parseInt($('#ChartByDepartament').attr('data-seller'));
  var cost_count = parseInt($('#ChartByDepartament').attr('data-cost'));
  var production_order_count = parseInt($('#ChartByDepartament').attr('data-production-order'));

  var ctx_by_departament = $("#ChartByDepartament");
  var data = {
    labels: [
        "En Ventas",
        "En Costo",
        "En Produccion"
    ],
    datasets: [
        {
            data: [seller_count, cost_count, production_order_count],
            backgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56"
            ],
            hoverBackgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56"
            ]
        }]
  };

  if($("#ChartByDepartament").length > 0){
    var PieByDepartament = new Chart(ctx_by_departament,{
      type: 'pie',
      data: data,
      options: {
          responsive: false
      }
    });    
  }


  var accepted_count = parseInt($('#QuotesAccept').attr('data-accepted'));
  var rejected_count = parseInt($('#QuotesAccept').attr('data-rejected'));
  var in_process_count = parseInt($('#QuotesAccept').attr('data-in_process'));

  var ctx_by_quotes_accepts = $("#QuotesAccept");
  var data = {
    labels: [
        "Cantidad Aceptadas",
        "Cantidad Rechazadas",
        "Cantidad En Proceso"
    ],
    datasets: [
        {
            data: [accepted_count, rejected_count, in_process_count],
            backgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56"
            ],
            hoverBackgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56"
            ]
        }]
  };

  if(ctx_by_quotes_accepts.length > 0){
    var PieByQuotesAccepts = new Chart(ctx_by_quotes_accepts,{
      type: 'pie',
      data: data,
      options: {
          responsive: false
      }
    });    
  }


  var accepted_sum = parseInt($('#QuotesAmount').attr('data-accepted'));
  var rejected_sum = parseInt($('#QuotesAmount').attr('data-rejected'));
  var in_process_sum = parseInt($('#QuotesAmount').attr('data-in_process'));

  var ctx_by_quotes_amount = $("#QuotesAmount");
  var data = {
    labels: [
        "Monto Aceptadas",
        "Monto Rechazadas",
        "Monto En Proceso"
    ],
    datasets: [
        {
            data: [accepted_sum, rejected_sum, in_process_sum],
            backgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56"
            ],
            hoverBackgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56"
            ]
        }]
  };

  if(ctx_by_quotes_amount.length){
    var PieByQuotesAmount = new Chart(ctx_by_quotes_amount,{
      type: 'pie',
      data: data,
      options: {
          responsive: false
      }
    });    
  }

});



