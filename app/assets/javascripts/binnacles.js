//  $(document).ready(function() {
$(document).on('ready page:load', function () { 

  $(".field-select").select2({
      placeholder: "Seleccione",
      allowClear: true
    });

  $('#production_order_binnacle_production_order_id').change(function(){ 
      if($(this).val())         {  
        $.get('/production_orders/'+$(this).val()+'/details.json',
          function(data) {
            //console.log(data.quote_status);
             var fecha = new Date(data.date);
             var fecha_quote = fecha.toLocaleDateString();
             var hora_quote = fecha.toLocaleTimeString();


            $("#production_order_binnacle_status").val(data.production_order_status);
            $("#production_order_binnacle_num_production_order").val(data.production_order_number);  
            $("#production_order_binnacle_quote_id").val(data.quote_number);                                                     
            $("#production_order_binnacle_agency_name").val(data.agency_name);                               
            $("#production_order_binnacle_adviser_name").val(data.adviser_name);                               
            $("#production_order_binnacle_reference").val(data.quote_reference);
            
            
            var enlace_order = "<a class='btn btn-info btn-lg active' target='_blank' href='/production_orders/"+data.production_order_id+".pdf' title='Ir a Orden de Producción'>Ver Orden de Producción</a>";
            //console.log(enlace_cotizacion,'aqui');
            $(".enlace_orden").html(enlace_order);
          });
        }
  });

/*edit*/
/*
  if($('#quote_id').val()){  
    $.get('/quotes/'+$('#quote_id').val()+'/details.json',
      function(data) {
        $("#comment_date").val(data.date);                                         
        $("#comment_agency_name").val(data.agency_name);                               
        $("#comment_client_name").val(data.client_name);                               
        $("#comment_phone_client").val(data.client_phone);
        $("#comment_adviser_name").val(data.adviser_name);
        $("#comment_comission").val(data.commission_quote);
        $("#comment_nit_client").val(data.client_nit);
        $("#comment_address_client").val(data.client_address);
        $("#comment_reference").val(data.quote_reference);
      });
  };*/
   
/*fin edit*/
  /*fin del document*/      
  });