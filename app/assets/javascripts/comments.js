//  $(document).ready(function() {
$(document).on('ready page:load', function () { 

  $(".field-select").select2({
      placeholder: "Seleccione",
      allowClear: true
    });

  $('#comment_quote_id').change(function(){ 
      if($(this).val())         {  
        $.get('/quotes/'+$(this).val()+'/details.json',
          function(data) {
            //console.log(data.quote_status);
             var fecha = new Date(data.date);
             var fecha_quote = fecha.toLocaleDateString();
             var hora_quote = fecha.toLocaleTimeString();

            $("#comment_date").val(fecha_quote);  
            $("#comment_time").val(hora_quote);                                                     
            $("#comment_agency_name").val(data.agency_name);                               
            $("#comment_client_name").val(data.client_name);                               
            $("#comment_phone_client").val(data.client_phone);
            $("#comment_adviser_name").val(data.adviser_name);
            $("#comment_comission").val(data.commission_quote+'%');
            $("#comment_nit_client").val(data.client_nit);
            $("#comment_address_client").val(data.client_address);
            $("#comment_reference").val(data.quote_reference);
            var status = '';

            if (data.quote_status == 'elaborated')
              status = 'Elaborada';

            if (data.quote_status == 'customer_review')          
              status = 'En Revisión del Cliente';  

            if (data.quote_status == 'sent_to_cost')          
              status = 'Enviada a Costos';

            if (data.quote_status == 'canceled')          
              status = 'Cancelada';      

            if (data.quote_status == 'quote_prices')
              status = 'Precios Asignados';

            if (data.quote_status == 'sent_seller')
              status = 'Enviada al Vendedor';

            if (data.quote_status == 'delivered_customer')
              status = 'Enviada al Cliente';

            if (data.quote_status == 'approved')
              status = 'Aprobada';

            if (data.quote_status == 'rejected')
              status = 'Rechazada';    

            if (data.quote_status == 'cost_report')
              status = 'Informe de Costos';

            $("#comment_status").val(status);
            
            if(data.quote_status=='elaborated' || data.quote_status=='customer_review' || data.quote_status=='sent_to_cost')
              var enlace_cotizacion = "<a class='btn btn-info btn-lg active' target='_blank' href='/quotes/"+data.id+"' title='Ir a Cotizacion'>Ver Cotizacion</a>";
            else
              var enlace_cotizacion = "<a class='btn btn-info btn-lg active' target='_blank' href='/quotes/"+data.id+"' title='Ir a Cotizacion'>Ver Cotizacion</a>";
            //console.log(enlace_cotizacion,'aqui');
            $(".enlace").html(enlace_cotizacion);
          });
        }
  });

/*edit*/
/*
  if($('#quote_id').val()){  
    $.get('/quotes/'+$('#quote_id').val()+'/details.json',
      function(data) {
        $("#comment_date").val(data.date);                                         
        $("#comment_agency_name").val(data.agency_name);                               
        $("#comment_client_name").val(data.client_name);                               
        $("#comment_phone_client").val(data.client_phone);
        $("#comment_adviser_name").val(data.adviser_name);
        $("#comment_comission").val(data.commission_quote);
        $("#comment_nit_client").val(data.client_nit);
        $("#comment_address_client").val(data.client_address);
        $("#comment_reference").val(data.quote_reference);
      });
  };*/
   
/*fin edit*/
  /*fin del document*/      
  });