# Define la máquina de estados para las solicitudes.
module WorkOrdersStatus

  def self.included(receiver)
    receiver.extend ClassMethods
    receiver.class_eval do
      include InstanceMethods
      include AASM
      
      # Define la máquina de estados para las ordenes de produccion
       aasm_column :status_order

       aasm_initial_state :elaborated
       
       aasm_state :elaborated, :initial => true
       aasm_state :delivered
       aasm_state :validated

       aasm_event :deliver do
          transitions :from => :elaborated, :to => :delivered
       end
        aasm_event :validate do
          transitions :from => :delivered, :to => :validated
       end
    end
  end

  module ClassMethods

  end

  module InstanceMethods

    def notificar_cambio_estatus
      # Para Enviar Correo al Cambiar el Estado
      #SolicitanteMailer.pendiente_email(self).deliver
    end
   
  end

end