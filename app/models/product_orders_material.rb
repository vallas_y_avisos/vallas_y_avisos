class ProductOrdersMaterial < ActiveRecord::Base
  self.table_name = "production_orders_materiales"  
  belongs_to :production_order

  attr_accessor :search_material, :client, :client_nit, :add_quantity, :add_unit, :add_description, :add_observation, :production_order, :material

  scope :by_client, ->(client) {where("client_id = ?",client)}


  def self.search_by_query(query)
    result = self.all    
    if !query["client"].nil? && !query["client"].empty?
      result = result.by_client(query["client"])
    end
    result
  end
end
