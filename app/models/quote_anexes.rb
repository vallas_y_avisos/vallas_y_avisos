class QuoteAnexes < ActiveRecord::Base
  self.table_name = "quotes_anexes"
  belongs_to :quote    
  has_attached_file :anexo
end
