# Define la máquina de estados para las solicitudes.
module QuotesStatus

  def self.included(receiver)
    receiver.extend ClassMethods
    receiver.class_eval do
      include InstanceMethods
      include AASM
      
      # Define la máquina de estados para las ordenes de produccion
       aasm_column :status

       aasm_initial_state :elaborated
       
       #aasm_state :elaborated, :initial => true, :before_enter => :registrar_historico 
       aasm_state :elaborated, :initial => true
       aasm_state :customer_review
       aasm_state :sent_to_cost
       aasm_state :back_to_sales
       aasm_state :canceled
       aasm_state :quote_prices
       aasm_state :sent_seller
       aasm_state :delivered_customer
       aasm_state :approved
       aasm_state :rejected
       aasm_state :cost_report

       aasm_event :customer_review    do
          transitions :from => :elaborated, :to => :customer_review, after: :registrar_historico
       end       


       aasm_event :sent_to_cost do
          transitions :from => [:elaborated,:back_to_sales],  :to => :sent_to_cost, after: :registrar_historico
       end

       aasm_event :canceled do
          transitions :from => [:customer_review, :elaborated, :sent_to_cost], :to => :canceled, after: :registrar_historico
       end

       aasm_event :quote_prices do
          transitions :from => :sent_to_cost, :to => :quote_prices, after: :registrar_historico
       end

       aasm_event :back_to_sales do
          transitions :from => :sent_to_cost, :to => :back_to_sales, after: :registrar_historico
       end       

       aasm_event :sent_seller do
          transitions :from => :quote_prices, :to => :sent_seller, after: :registrar_historico
       end  

       aasm_event :delivered_customer do
          transitions :from => :sent_seller, :to => :delivered_customer, :after => :guardar_fecha_fin_costos, after: :registrar_historico
       end         

       aasm_event :approved do
          transitions :from => :delivered_customer, :to => :approved, after: :registrar_historico
       end           

       aasm_event :rejected do
          transitions :from => :delivered_customer, :to => :rejected, after: :registrar_historico
       end     

       aasm_event :cost_report do
          transitions :from => :rejected, :to => :cost_report, after: :registrar_historico
       end

    end
  end

  module ClassMethods

  end

  module InstanceMethods

    def guardar_fecha_fin_costos
      self.date_delivered_customer = Date.today
      self.save
    end

  end

end
