class Agency < ActiveRecord::Base
	has_many :quotes


	#scope
	scope :by_name, ->(name) {where("lower(name) like ?","%#{name.downcase}%")}

  # Rails Admin config
  rails_admin do
    list do
      field :name
      field :email
      field :phone
    end

    edit do
      field :name
      field :email
      field :phone
      field :address
    end
  end  


	def self.search_by_query(query)
    result = self.all
    if !query["name"].nil? && !query["name"].empty?
    	result = result.by_name(query["name"])
   	end
    result
  end

end
