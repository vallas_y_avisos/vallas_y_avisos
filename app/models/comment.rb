class Comment < ActiveRecord::Base

  include ActsAsCommentable::Comment

  belongs_to :commentable, :polymorphic => true
  belongs_to :quote

  default_scope -> { order('created_at DESC') }

  attr_accessor :quote_id, :nit_client, :address_client, :phone_client, :agency_name, :client_name, :adviser_name, :comission, :reference, :date, :status, :num_production_order, :production_order_id, :time, :fecha, :hora, :numero, :quote_status, :quote_status_edit
  # NOTE: install the acts_as_votable plugin if you
  # want user to vote on the quality of comments.
  #acts_as_voteable

  # NOTE: Comments belong to a user
  belongs_to :administrador

  validates :title,
    length: { maximum: 50 },
    presence: true
  validates :comment,
    presence: true
  validates :quote_id,
    presence: true
  
 scope :by_commentable_id, ->(commentable_id) {where("commentable_id = ?",commentable_id)}    

  def self.search_by_query(query)
    result = self.all
    if !query["commentable_id"].nil? && !query["commentable_id"].empty?
      result = result.by_status(query["commentable_id"])
    end
    result
  end

 scope :by_status, ->(status) {where("status = ?",status)}
    
  def quote_status                      
        if self.estatus === 'elaborated'
          return'Elaborada'
        end   
        if self.estatus === 'customer_review'          
          return'En Revisión del Cliente'
        end                    
        if self.estatus === 'sent_to_cost'          
          return'Enviada a Costos'
        end  
        if self.estatus === 'canceled'          
          return'Cancelada'
        end                    
        if self.estatus === 'quote_prices'
          return'Precios Asignados'
        end            
        if self.estatus === 'sent_seller'
          return'Enviada al Vendedor'
        end
        if self.estatus === 'delivered_customer'
          return'Enviada al Cliente'
        end
        if self.estatus === 'approved'
          return'Aprobada'
        end
        if self.estatus === 'rejected'
          return'Rechazada'
        end       
        if self.estatus === 'cost_report'
          return'Informe de Costos'
        end 
    end  

  def quote_status_edit                     
        if self.status === 'elaborated'
          return'Elaborada'
        end   
        if self.status === 'customer_review'          
          return'En Revisión del Cliente'
        end                    
        if self.status === 'sent_to_cost'          
          return'Enviada a Costos'
        end  
        if self.status === 'canceled'          
          return'Cancelada'
        end                    
        if self.status === 'quote_prices'
          return'Precios Asignados'
        end            
        if self.status === 'sent_seller'
          return'Enviada al Vendedor'
        end
        if self.status === 'delivered_customer'
          return'Enviada al Cliente'
        end
        if self.status === 'approved'
          return'Aprobada'
        end
        if self.status === 'rejected'
          return'Rechazada'
        end       
        if self.status === 'cost_report'
          return'Informe de Costos'
        end 
    end       


end
