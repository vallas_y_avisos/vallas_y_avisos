class DifficultyQuote < ActiveRecord::Base
  belongs_to :quote

  def difficulty                        
     return "#{self.name} (#{self.quantity} #{self.lapse} #{self.type_lapse})"
  end    

  def difficulty_lapse                      
     return "#{self.quantity} #{self.lapse} #{self.type_lapse}"
  end     

  def difficulty_production
     return "#{self.name}"
  end    

  rails_admin do
    list do
      field :name
      field :quantity
      field :lapse
    end

    edit do
      field :name
      field :quantity
      field :lapse
      field :type_lapse
      field :is_quote
      field :is_production
    end
  end  

end
