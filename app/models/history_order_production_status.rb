class HistoryOrderProductionStatus < ActiveRecord::Base
  self.table_name = "history_order_production_statuses"
  belongs_to :order_production
  belongs_to :administrador


  def self.sent_date production_order_id
  	HistoryOrderProductionStatus.where(production_order_id: production_order_id, status:'sent').first.try(:created_at)
  end

  def self.sent_to_billing_date production_order_id
  	HistoryOrderProductionStatus.where(production_order_id: production_order_id, status:'sent_to_billing').first.try(:created_at)
  end

  def self.sent_to_cost_date production_order_id
  	HistoryOrderProductionStatus.where(production_order_id: production_order_id, status:'sent_to_cost').first.try(:created_at)
  end

  
end
