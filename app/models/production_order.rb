class ProductionOrder < ActiveRecord::Base
  has_paper_trail :class_name => 'Version'
  include ProductionOrdersStatus
	self.table_name = "production_orders"
  has_attached_file :spreadsheet

  default_scope { order(created_at: :desc) }

  acts_as_commentable 
  has_attached_file :inventory_material_destruction_attachment

	belongs_to :quote
	belongs_to :administrador
  belongs_to :difficulty_quote
  has_many :production_order_anexes, :dependent => :destroy, class_name: "ProductionOrderAnexe", :foreign_key => "production_order_id"
  has_many :quote_details, :primary_key => "quote_id", foreign_key: "quote_id", :dependent => :destroy
	
	has_many :production_order_binnacles, :class_name => "ProductionOrderBinnacle", :foreign_key => "production_order_id"
  has_many :production_order_comments, :class_name => "ProductionOrderComment", :foreign_key => "production_order_id"
  has_many :work_orders
  has_many :production_orders_materiales
  has_many :remissions
  has_many :remission_details, :through => :remissions, :dependent => :destroy
  has_many :bills
  has_many :bill_details, :through => :bills, :dependent => :destroy
  has_many :history_order_production_statuses, :dependent => :destroy
  

  accepts_nested_attributes_for :production_order_anexes,
                              allow_destroy: true,
                              update_only: true   

  accepts_nested_attributes_for :quote_details,
                              allow_destroy: true,
                              update_only: true

  accepts_nested_attributes_for :production_order_comments,
                              allow_destroy: true,
                              update_only: true

	attr_accessor :nit_client, :address_client, :phone_client, :client_name, :adviser_name, :comission, :reference, :quote_number, :status, :way_pay
  attr_accessor :search_material, :client, :client_nit, :add_quantity, :add_unit, :add_description, :add_observation, :production_order_id, :total_amount, :administrador_id_before, :validates_quantity
  #validate
  
  validates :quote_id, :site_installation, :address, :ask_for, :phone, :specifications, :date_elaboration, :presence => true

  
  validates :number_photos,
    numericality: { only_integer: true, greater_than: 0, allow_blank: true },
    presence: true

  validates :angle,
    numericality: { only_integer: true, greater_than: 0, allow_blank: true }

  validates :sheet,
    numericality: { only_integer: true, greater_than: 0, allow_blank: true }

  validates :unit_value,
    numericality: { only_integer: true, greater_than: 0, allow_blank: true }

  validates_uniqueness_of :number, :case_sensitive => false

  validate :check_if_at_less_one_quantity, if: "validates_quantity"

	#scope
	scope :by_number, ->(number) {where("number = ?",number)}
  scope :by_status, ->(status) {where("status_order = ?",status)}
	scope :by_contract_number, ->(contract_number) {where("contract_number = ?",contract_number)}
	scope :by_agency, ->(agency) {joins(:quote).where("quotes.agency_id = ?",agency)}
	scope :by_adviser, ->(adviser) {joins(:quote).where("quotes.adviser_id = ?",adviser)}
	scope :by_client, ->(client) {joins(:quote).where("quotes.client_id = ?",client)}
	scope :by_quote, ->(quote) {where("quote_id = ?",quote)}
  scope :sent, -> {where("status_order = 'sent'")}

  before_save :asign_start_date_production

	def self.search_by_query(query)
    result = self.all
    if !query["agency"].nil? && !query["agency"].empty?
    	result = result.by_agency(query["agency"])
   	end
   	 if !query["adviser"].nil? && !query["adviser"].empty?
    	result = result.by_adviser(query["adviser"])
   	end
   	if !query["client"].nil? && !query["client"].empty?
    	result = result.by_client(query["client"])
   	end
   	if !query["quote"].nil? && !query["quote"].empty?
    	result = result.by_quote(query["quote"])
   	end
   	if !query["number"].nil? && !query["number"].empty?
    	result = result.by_number(query["number"])
   	end
    if !query["status"].nil? && !query["status"].empty?
      result = result.by_status(query["status"])
    end
   	if !query["contract_number"].nil? && !query["contract_number"].empty?
    	result = result.by_contract_number(query["contract_number"])
   	end
    result
  end

  def asign_start_date_production
    if self.administrador_id_before.nil? && !self.administrador_id.nil? && self.start_date_production.to_s.empty?
      self.start_date_production = Time.zone.now
    end
  end

  def order_status
    if self.status_order === 'sent_to_production'
      return 'Enviada a Producción'
    elsif self.status_order === 'review'
      return 'Revisada'
    elsif self.status_order === 'sent_to_cost'
      return 'Enviada a Costo'
    elsif self.status_order === 'billing'
      return 'Enviada a Facturación'
    elsif self.status_order === 'billed'
      return 'Facturada'
    elsif self.status_order === 'end'
      return 'Terminada'
    elsif self.status_order === 'sent_to_billing'
      return 'Facturación'
    elsif self.status_order === 'elaborated'
      return 'Elaborada'
    else
      return 'Indefinido'
    end
  end	

  def status
    status_order
  end

  def can_end?
    if actual_delivey_date
      if actual_delivey_date < Date.today
        true
      else
        if reason.to_s.empty?
          false
        else
          true
        end
      end
    else
      false
    end
  end

  def can_inventory_end?
    !inventory_elements_out.nil? && !inventory_elements_in.nil? && !inventory_material_destruction.nil?
  end

  def can_warehouse_end?
    !warehouse_aditional_material.nil? && !warehouse_material_changed.nil?
  end

  def can_machine_end?
    !machine_aditional_material.nil? && !machine_material_changed.nil? && !machine_machine_changed.nil? && !machine_art.nil?
  end

  def check_if_at_less_one_quantity
    quote_details.each do |qd|
      return true if qd.op_quantity > 0
    end
    errors.add(:base, "Debe Agregar al menos una cantidad")
  end


end


