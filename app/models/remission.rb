class Remission < ActiveRecord::Base
	self.table_name = "remissions"
  include RemissionsStatus
	belongs_to :administrador
	has_many :remission_details, dependent: :destroy 
	belongs_to :production_order

  before_create :generate_number

	accepts_nested_attributes_for :remission_details,
                                reject_if: lambda { |a| a[:product_id].blank? && a[:quantity].blank?},
                                allow_destroy: true

    validates :date_remission, :presence => true  
    

    attr_accessor :client_name, :client_adress, :client_phone, :reference

     #scope
    scope :by_production_order, ->(production_order) {where("production_order_id = ?",production_order)}
    scope :by_client, ->(client) {joins(:production_order).joins({production_order: [:quote]}).where("quotes.client_id = ?",client)}
	scope :by_quote, ->(quote) {joins(:production_order).where("production_orders.quote_id = ?",quote)}

    #*****************************+TERMINAR VALIDAIOM**************************

    def generate_number
      self.number_remission = (Remission.last.id + 1).to_s.rjust(5,"0")      
    end    

    def remission_status
      if self.status == 'sent'
        return "Enviada"
      else
        return "Elaborada"
      end
    end

    def self.search_by_query(query)
    	result = self.all
    	if !query["production_order"].nil? && !query["production_order"].empty?
    		result = result.by_production_order(query["production_order"])
   		end
   		if !query["client"].nil? && !query["client"].empty?
    		result = result.by_client(query["client"])
   		end
   		if !query["quote"].nil? && !query["quote"].empty?
    		result = result.by_quote(query["quote"])
   		end

    	result
  end
end

