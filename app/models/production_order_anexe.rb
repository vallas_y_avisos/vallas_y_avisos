class ProductionOrderAnexe < ActiveRecord::Base
  self.table_name = "production_order_anexes"
  belongs_to :production_order

  has_attached_file :anexo
end