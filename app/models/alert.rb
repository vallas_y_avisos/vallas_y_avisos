class Alert < ActiveRecord::Base
  has_many :alert_users

  def get_document
    @obj ||= find_object
  end
  
private
  
  def find_object
    if Object.const_defined?(model.capitalize)
      klaz = Object.const_get(model.capitalize)
      klaz.find(model_id)
    else
      nil
    end    
  end

end
