class MaterialsList < ActiveRecord::Base  
  has_paper_trail :class_name => 'Version'
  include MaterialsListStatus
  has_many :materiales_details, dependent: :destroy
  accepts_nested_attributes_for :materiales_details,
                                allow_destroy: true,
                                update_only: true 

  validates :production_order_id, :date_delivery, :denomination, :prepared_by, :presence => true                                 

  attr_accessor :material_id, :quantity, :materials_unit, :description, :observation, :materials_reference 
  attr_accessor :search_material, :client, :client_nit, :add_quantity, :add_unit, :add_description, :add_observation,  :add_reference, :production_order, :material, :fecha, :adviser_name , :adviser                            

  scope :by_status, ->(status) {where("status = ?",status)}

  def self.search_by_query(query)
    result = self.all
    if !query["status"].nil? && !query["status"].empty?
      result = result.by_status(query["status"])
    end      
    result
  end

  def materials_list_status                      
      if self.status === 'elaborated'
        return'Elaborada'
      end   
      if self.status === 'sent_to_production'          
        return'Enviada a Produccion'
      end                    
      if self.status === 'sent_to_store'          
        return'Enviada Almacen'
      end                     
  end    
end
