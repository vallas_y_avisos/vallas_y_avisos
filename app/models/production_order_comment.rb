class ProductionOrderComment < ActiveRecord::Base
	self.table_name = "production_order_comments"

	belongs_to :production_order, :foreign_key => "production_order_id"
	belongs_to :administrador
	
end
