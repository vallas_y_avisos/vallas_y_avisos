class Category < ActiveRecord::Base
   has_many :products
   self.table_name = "categories"

  validates :name, presence: true  
    # Rails Admin config
  rails_admin do
    list do
      field :name      
    end

    edit do
      field :name     
    end
  end     
end
