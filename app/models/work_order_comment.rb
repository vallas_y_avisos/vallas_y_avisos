class WorkOrderComment < ActiveRecord::Base
	self.table_name = "work_order_comments"

	belongs_to :work_order, :foreign_key => "work_order_id"
	belongs_to :administrador
	
end
