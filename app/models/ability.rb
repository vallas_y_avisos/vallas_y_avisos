# Definición de habilidades de cada rol
# para los Administradores
class Ability
  include CanCan::Ability
  
  # user es una instancia de Administrador
  def initialize(user)
    # TODO: Cambiar: Esto es para evitar quedarse sin acceso 
    # mientras se terminan de definir las habilidades de cada rol.
    can :access, :rails_admin # Todos loas admini deben ingresar a rails admin
    can :dashboard # Todos pueden ingresar al dashboard?
    
    # Ver detalles de accesibilidad de Rails Admin en:
    # https://github.com/sferik/rails_admin/wiki/CanCan
    
    # Par controladores no Rails Admin, usar 
    # En el controlador load_and_authorize_resource
    # Detalles en: https://github.com/ryanb/cancan/wiki/authorizing-controller-actions    

    if user && user.is?(:vendedor)
      #************************Quotes************************
      can :create, Quote
      can :update, Quote 
      #Esto indica que puede eliminar quotes si t solo si el usuario creo la quote
      can :destroy, Quote do |quote|
        quote.try(:Administrador) == user || user.is?(:vendedor)
      end
      #************************ProductionOrder************************
      can :read, ProductionOrder
      can :create, ProductionOrder
      can :update, ProductionOrder 
      #Esto indica que puede eliminar ProductionOrder si t solo si el usuario creo la orden
      can :destroy, ProductionOrder do |orden|
        orden.try(:Administrador) == user || user.is?(:vendedor)
      end

      #********************** ProductionOrderBinnacle ******************************
      can :read, ProductionOrderBinnacle
      can :create, ProductionOrderBinnacle
      can :update, ProductionOrderBinnacle
      can :destroy, ProductionOrderBinnacle

      #****************OJO Validar quienes hacen esto***********************
      can :review, ProductionOrder
      can :sent_to_cost, ProductionOrder

      #************************Product************************
      can :read, Product
      can :create, Product
      can :update, Product 
      #Esto indica que puede eliminar quotes si t solo si el usuario creo la quote
      can :destroy, Product do |p|
        p.try(:Administrador) == user || user.is?(:vendedor)
      end
      
      #************************Client************************
      can :read, Client
      can :create, Client
      can :update, Client 
      #Esto indica que puede eliminar quotes si t solo si el usuario creo la quote
      can :destroy, Client do |c|
        c.try(:Administrador) == user || user.is?(:vendedor)
      end

      #************************Agency************************
      can :read, Agency
      can :create, Agency
      can :update, Agency 
      #Esto indica que puede eliminar quotes si t solo si el usuario creo la quote
      can :destroy, Agency do |a|
        a.try(:Administrador) == user || user.is?(:vendedor)
      end

      #************************Adviser************************
      can :read, Adviser
      can :create, Adviser
      can :update, Adviser 
      #Esto indica que puede eliminar quotes si t solo si el usuario creo la quote
      can :destroy, Adviser do |a|
        a.try(:Administrador) == user || user.is?(:vendedor)
      end

      can :read, Comment

    end       

    if user && user.is?(:costo)
      can :read, Quote
      can :update, Quote
      can :read, ProductionOrder
      can :update, ProductionOrder

      can :read, ProductionOrderBinnacle
      can :create, ProductionOrderBinnacle
      can :update, ProductionOrderBinnacle
      can :destroy, ProductionOrderBinnacle
      
     

      #************************MaterialsList************************
    #  can :create, MaterialsList
    #  can :update, MaterialsList 
      #Esto indica que puede eliminar WorkOrder si t solo si el usuario creo la orden
    #  can :destroy, MaterialsList do |m|
    #    m.try(:Administrador) == user || user.is?(:costo)
    #  end
      can :read, Remission
      can :create, Remission
      
      can :read, WorkOrder
      can :deliver, WorkOrder
      can :validate, WorkOrder
      can :sent, ProductionOrder
    end

    if user && (user.is?(:produccion) || user.is?(:coordinador_produccion))
      #************************MaterialsList************************
     # can :create, MaterialsList
     # can :update, MaterialsList 
      #Esto indica que puede eliminar WorkOrder si t solo si el usuario creo la orden
     # can :destroy, MaterialsList do |m|
     #   m.try(:Administrador) == user || user.is?(:produccion)
     # end

      can :read, Quote

      can :billing, ProductionOrder
      can :billed, ProductionOrder
      can :update, ProductionOrder 

       #************************WorkOrder************************
      can :read, WorkOrder
      can :create, WorkOrder
      can :update, WorkOrder 
      #Esto indica que puede eliminar WorkOrder si t solo si el usuario creo la orden
      can :destroy, WorkOrder do |order|
        order.try(:Administrador) == user || user.is?(:costo)
      end 

      can :read, ProductionOrder

      can :read, ProductionOrderBinnacle
      can :create, ProductionOrderBinnacle
      can :update, ProductionOrderBinnacle
      can :destroy, ProductionOrderBinnacle

      can :read, Remission
      can :create, Remission
      can :update, Remission
      can :destroy, Remission

      can :read, Bill
      can :create, Bill
      can :update, Bill
      can :destroy, Bill
      
    end

    if user && user.is?(:almacen)
      can :read, Quote
      can :read, ProductionOrder
      can :read, WorkOrder
      #************************MaterialsList************************
    #  can :create, MaterialsList
    #  can :update, MaterialsList 
      #Esto indica que puede eliminar WorkOrder si t solo si el usuario creo la orden
    #  can :destroy, MaterialsList do |m|
    #    m.try(:Administrador) == user || user.is?(:almacen)
    #  end
      can :read, Remission
      can :create, Remission

      
    end

    if user && user.is?(:maquina)
      can :read, Quote
      can :read, ProductionOrder

      can :read, ProductionOrderBinnacle
      can :create, ProductionOrderBinnacle
      can :update, ProductionOrderBinnacle
      can :destroy, ProductionOrderBinnacle

      can :read, Remission
      can :create, Remission
    end

    if user && user.is?(:inventario)
      can :read, Quote
      can :read, ProductionOrder

      can :read, ProductionOrderBinnacle
      can :create, ProductionOrderBinnacle
      can :update, ProductionOrderBinnacle
      can :destroy, ProductionOrderBinnacle

      can :read, Remission
      can :create, Remission
    end

    if user && user.is?(:facturacion)
      can :read, Quote
      can :read, ProductionOrder
    end

    if user && user.is?(:admin)
      # Habilidades específicas de Administrador
      can :manage, :all
    end

    if user && user.is?(:seguimiento)
      can :read, Comment
      can :read, Quote
      
      can :read, ProductionOrderBinnacle
      can :create, ProductionOrderBinnacle
      can :update, ProductionOrderBinnacle
      can :destroy, ProductionOrderBinnacle
    end

    if user && user.is?(:analista)
      can :read, Quote
      can :update, Quote
      can :read, Comment

      can :read, ProductionOrder

      can :read, ProductionOrderBinnacle
      can :create, ProductionOrderBinnacle
      can :update, ProductionOrderBinnacle
      can :destroy, ProductionOrderBinnacle
    end        


  end  
end