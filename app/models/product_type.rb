class ProductType < ActiveRecord::Base
  belongs_to :type
  belongs_to :product

  validates :type, :product, presence: true  
    # Rails Admin config
  rails_admin do
    list do
      field :type 
      field :product
    end

    edit do
      field :type
      field :product
    end
  end   
end

