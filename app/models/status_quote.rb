class StatusQuote < ActiveRecord::Base
  self.table_name = "status_quotes"
  belongs_to :quote


  def self.aproved_date quote_id
  	StatusQuote.where(quote_id: quote_id, status:'approved').first.try(:created_at)
  end
end
