class QuoteDetail < ActiveRecord::Base
  self.table_name = "quote_details"
  after_create :registrar_historico
	belongs_to :quote
  belongs_to :product
  belongs_to :type
  has_many :quote_details_types


  scope :accepted, -> {joins(:quote).where("quotes.status in ('approved')")}
  scope :rejected, -> {joins(:quote).where("quotes.status in ('rejected')")}
  scope :in_process, -> {joins(:quote).where("quotes.status not in ('rejected', 'approved')")}  

  validate :check_op_quantities, on: :update

  #validates_presence_of :description, :price, :quantity
  #validates_uniqueness_of :product_id

  # validates :price,
  #  numericality: {greater_than: 0, allow_blank: true },
  #  presence: true

  # validates :quantity,
  #  numericality: {greater_than: 0, allow_blank: true },
  #  presence: true  

public

  def check_op_quantities
    check = (op_quantity <= quantity)
    errors.add(:op_quatity, "La cantidad de la orden de produccion no puede ser mayor a la cotizada") unless check
    check
  end

  def registrar_historico
   
    #byebug
   if quote.status == 'elaborated'
    ActionCorreo.cotizacion_email_elaborate(quote_id, quote.status, quote.administrador).deliver
   end  
      
  end   
end
