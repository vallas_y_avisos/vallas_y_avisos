# Define la máquina de estados para las solicitudes.
module MaterialsListStatus

  def self.included(receiver)
    receiver.extend ClassMethods
    receiver.class_eval do
      include InstanceMethods
      include AASM
      
      # Define la máquina de estados para las ordenes de produccion
       aasm_column :status

       aasm_initial_state :elaborated
       
       aasm_state :elaborated, :initial => true
       aasm_state :sent_to_production
       aasm_state :sent_to_store
    

       aasm_event :sent_to_production do
          transitions :from => :elaborated, :to => :sent_to_production
       end       

       aasm_event :sent_to_store do
          transitions :from => :sent_to_production, :to => :sent_to_store
       end
       

    end
  end

  module ClassMethods

  end

  module InstanceMethods

    def notificar_cambio_estatus
      # Para Enviar Correo al Cambiar el Estado
      #SolicitanteMailer.pendiente_email(self).deliver
    end
   
  end

end