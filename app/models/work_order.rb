class WorkOrder < ActiveRecord::Base
  include WorkOrdersStatus
	belongs_to :administrador
	belongs_to :production_order
	has_many :work_order_details, dependent: :destroy 
  has_many :work_order_comments, :class_name => "WorkOrderComment", :foreign_key => "work_order_id"

	accepts_nested_attributes_for :work_order_details,
                                reject_if: lambda { |a| a[:description].blank? && a[:quantity].blank? && a[:price].blank? },
                                allow_destroy: true
  accepts_nested_attributes_for :work_order_comments,
                              allow_destroy: true,
                              update_only: true

    attr_accessor :nit_client, :address_client, :phone_client, :client_name, :adviser_name, :comission, :reference, :quote_number, :production_order_number 

    #validate
  validates :cost_center,
    length: { maximum: 100 },
    presence: true
  validates :application_date, :cost_center, :execution_date, :contractor, :production_order_id, :name, :presence => true

    #scope
    scope :by_production_order, ->(production_order) {where("production_order_id = ?",production_order)}
    scope :by_client, ->(client) {joins(:production_order).joins({production_order: [:quote]}).where("quotes.client_id = ?",client)}
		scope :by_quote, ->(quote) {joins(:production_order).where("production_orders.quote_id = ?",quote)}

    def self.search_by_query(query)
    	result = self.all
    	if !query["production_order"].nil? && !query["production_order"].empty?
    		result = result.by_production_order(query["production_order"])
   		end
   		if !query["client"].nil? && !query["client"].empty?
    		result = result.by_client(query["client"])
   		end
   		if !query["quote"].nil? && !query["quote"].empty?
    		result = result.by_quote(query["quote"])
   		end
    	result
  end

  def order_status
    if self.status_order === 'delivered'
      return 'Entregado'
    elsif self.status_order === 'validated'
      return 'Validado'
    else
      return 'Elaborado'
    end
  end 
end



