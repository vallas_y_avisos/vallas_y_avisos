# Define la máquina de estados para las solicitudes.
module ProductionOrdersStatus

  def self.included(receiver)
    receiver.extend ClassMethods
    receiver.class_eval do
      include InstanceMethods
      include AASM
      
      # Define la máquina de estados para las ordenes de produccion
       aasm_column :status_order

       aasm_initial_state :elaborated
       
       aasm_state :elaborated, :initial => true
       aasm_state :review
       aasm_state :sent_to_cost
       aasm_state :sent_to_production
       aasm_state :sent_to_billing
       aasm_state :end
       aasm_state :billed

       aasm_event :check do
          transitions :from => :elaborated, :to => :review
       end
        aasm_event :sent_to_cost do
          transitions :from => :elaborated, :to => :sent_to_cost
       end

       aasm_event :to_production do
          transitions :from => :sent_to_cost, :to => :sent_to_production
       end

       aasm_event :price_adjustment do
          transitions :from => :sent_to_production, :to => :sent_to_cost
       end

       aasm_event :billing do
          transitions :from => :end, :to => :sent_to_billing
       end

       aasm_event :billed do
          transitions :from => :sent_to_billing, :to => :billed
       end

       aasm_event :ended do
          transitions :from => :sent_to_production, :to => :end
       end

    end
  end

  module ClassMethods

  end

  module InstanceMethods

    def notificar_cambio_estatus
      # Para Enviar Correo al Cambiar el Estado
      #SolicitanteMailer.pendiente_email(self).deliver
    end
   
  end

end