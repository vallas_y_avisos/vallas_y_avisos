class TypeDescription < ActiveRecord::Base
  belongs_to :type

  validates :type, :description, presence: true  

  rails_admin do
    list do
      field :description
      field :type
    end

    edit do
      field :description
      field :type
    end
  end     

  def name
    description
  end
end
