class Client < ActiveRecord::Base
  has_many :quotes
  #has_paper_trail :class_name => 'Version'
  validates :name, :nit, :email, :contact,  presence: true
  validates_format_of :email, :with => /@/

  after_create :set_code


  # Rails Admin config
  rails_admin do
    list do
      field :code
      field :name
      field :nit
      field :contact
    end

    edit do
      field :name
      field :nit
      field :email
      field :agency
      field :position
      field :contact
      field :office
      field :phone
      field :city
      field :address
    end
  end   

  def set_code
    self.code = sprintf('%03d', id)
    save
  end


end
