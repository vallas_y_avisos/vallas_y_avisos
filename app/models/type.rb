class Type < ActiveRecord::Base
  has_paper_trail :class_name => 'Version'
  has_many :product_types
  has_many :products, through: :product_types
  has_many :types_descriptions
  
  validates :name, presence: true  
    # Rails Admin config
  rails_admin do
    list do
      field :name      
    end

    edit do
      field :name     
    end
  end   
end
