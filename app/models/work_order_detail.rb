class WorkOrderDetail < ActiveRecord::Base
	belongs_to :work_order
	belongs_to :product

	validates_presence_of :description, :price, :quantity

	 validates :price,
    numericality: {greater_than: 0, allow_blank: true },
    presence: true

     validates :quantity,
    numericality: {greater_than: 0, allow_blank: true },
    presence: true
end
