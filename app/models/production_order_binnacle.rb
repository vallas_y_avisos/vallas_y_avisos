class ProductionOrderBinnacle < ActiveRecord::Base
	self.table_name = "production_order_binnacles"

	belongs_to :production_order, :foreign_key => "production_order_id"
	belongs_to :administrador
	has_many :production_order_binnacle_anexes

	attr_accessor :quote_id, :nit_client, :address_client, :phone_client, :agency_name, :administrador_name, :client_name, :adviser_name, :comission, :reference, :date, :status, :num_production_order, :time, :fecha, :hora, :numero, :quote_status, :quote_status_edit

	accepts_nested_attributes_for :production_order_binnacle_anexes,
                               # reject_if: lambda { |a| a[:type_attachments_id].blank? },
                                allow_destroy: true

end
