# Define la máquina de estados para las solicitudes.
module RemissionsStatus

  def self.included(receiver)
    receiver.extend ClassMethods
    receiver.class_eval do
      include InstanceMethods
      include AASM
      
      # Define la máquina de estados para las ordenes de produccion
       aasm_column :status

       aasm_initial_state :elaborated
       
       aasm_state :elaborated, :initial => true
       aasm_state :sent
      

       aasm_event :sent_status do
          transitions :from => :elaborated, :to => :sent
       end

    end
  end

  module ClassMethods

  end

  module InstanceMethods

    def notificar_cambio_estatus
      # Para Enviar Correo al Cambiar el Estado
      #SolicitanteMailer.pendiente_email(self).deliver
    end
   
  end

end