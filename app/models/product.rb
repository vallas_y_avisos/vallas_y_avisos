class Product < ActiveRecord::Base
  has_paper_trail :class_name => 'Version'
	has_many :work_order_details
	has_many :remissions_details, class_name: "RemissionDetail"
  has_many :quote_details
  belongs_to :category, class_name: "Category", foreign_key: "categorie_id"
  has_many :product_types
  has_many :types, through: :product_types


  validates :name, :category, presence: true

  # Rails Admin config
  rails_admin do
    list do
      field :name
      field :category
    end

    edit do
      field :name
      field :category
    end
  end  

  def self.products_of_quote quote
    array_products = []
    if quote.quote_details.any?
      quote.quote_details.each do |q|
        array_products << q.product.id
      end
      products = self.where("id IN (?)", array_products)
    else
      products = []
    end
    products
  end

end
