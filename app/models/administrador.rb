class Administrador < ActiveRecord::Base
  # Lista de roles
  ROLES = %w[admin, vendedor, costo, produccion, almacen, maquina, inventario, facturacion, seguimiento, analista, coordinador_produccion]
    
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  belongs_to :department
  belongs_to :adviser
  has_many :work_orders
  has_many :remissions
  has_many :quotes
  has_many :production_orders
  has_many :comments
  has_many :binnacle_orders_productions
  has_many :history_order_production_statuses

  #attr_accessor :validates_seller

  validates :email, :nombre, :department, :rol,  presence: true
  
  
  #validates :adviser, presence: { if: :{self.rol == 'vendedor'}, message: 'is forgotten.' }, confirmation: true
  #validates :adviser, presence: { if: {self.rol == 'vendedor'} }, confirmation: true 

         
  has_paper_trail :class_name => 'Version'

  def roles=(roles)
    self.roles_mask = (roles & ROLES).map { |r| 2**ROLES.index(r) }.inject(0, :+)
  end
  
  def roles
    ROLES.reject do |r|
      ((roles_mask || 0) & 2**ROLES.index(r)).zero?
    end
  end
  
  def is?(role)
    self.rol == role.to_s || self.rol == 'admin'
  end

  def active_for_authentication?
    #remember to call the super
    #then put our own check to determine "active" state using 
    #our own "is_active" column
    super and self.is_active?
  end

  # Rasil Admin config
  rails_admin do
    list do
      field :email
      field :nombre
      field :department
      field :rol
      field :is_active
    end

    edit do
      field :email
      field :nombre
      field :password
      field :department
      field :is_active
      field :rol do
        render do
          bindings[:view].render :partial => "roles_form_fields", :locals => {:f => bindings[:form]}
        end
      end
      field :adviser
    end
  end  

  def name
    nombre
  end

  #Adviser.create(:name => self.nombre,  :email=>self.email) 


end
