class ProductionOrderBinnacleAnexe < ActiveRecord::Base
	self.table_name = "production_order_binnacle_anexes"
	belongs_to :production_order_binnacle, :foreign_key => "production_order_binnacle_id"
	
	has_attached_file :anexo
end
