class Adviser < ActiveRecord::Base
  has_paper_trail :class_name => 'Version'
  self.table_name = "advisers"
	belongs_to :client
  has_many :administrador
	has_many :quotes

  validates :name, :phone, :email,  presence: true

  after_create :set_code

  # Rails Admin config
  rails_admin do
    list do
      field :assessor_code
      field :name
      field :email
      field :phone
    end

    edit do
      field :name
      field :email
      field :phone
      field :address
      field :client
    end
  end  

  def set_code
    self.assessor_code = sprintf('%03d', id)
    save
  end


end
