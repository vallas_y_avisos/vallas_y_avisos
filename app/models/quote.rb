class Quote < ActiveRecord::Base
  include QuotesStatus
  has_paper_trail :class_name => 'Version'
  has_attached_file :spreadsheet
  after_create :set_number
  #acts_as_commentable
  belongs_to :client
  belongs_to :administrador
  #belongs_to :coordinator, class_name: "Coordinator", foreign_key: "coordinator_id"
  #belongs_to :coordinator, class_name: "Administrador", primary_key: "coordinator_id"
  belongs_to :coordinator, class_name: "Administrador", foreign_key: "coordinator_id"
  belongs_to :adviser
  belongs_to :agency
  belongs_to :difficulty_quote
  has_many :quote_details, dependent: :destroy
  has_many :production_orders, dependent: :destroy
  has_many :comments, as: :commentable
  has_many :quotes_anexes, dependent: :destroy
  has_many :status_quotes
  has_many :estado, dependent: :destroy
  #esta clase estado la tuve que crear ya que status_quotes me da error cuando trato de usarla es algo con el nombre

  accepts_nested_attributes_for :quotes_anexes,
                                allow_destroy: true,
                                update_only: true   
                               

  accepts_nested_attributes_for :quote_details,                                
                                allow_destroy: true,
                                update_only: true

  default_scope { order(created_at: :desc) }
   #def quotes_anexes_array=(array)
   #  array.each do |file|
   #  quotes_anexe.build(:quotes_anexes => file)
   #  end
   #end                                 
   # en la vista = quotes_anexes.file_field :quotes_anexes_array, :class=>'file', :id=>'file-es',  :multiple => true, 'data-preview-file-type' => "any"             

  #validate
  validates_uniqueness_of :number, :case_sensitive => false, on: :update
  validates :number, presence: true, on: :update
  validates :method_payment,
    length: { maximum: 100 },
    presence: true
  validates :commission_quote, :client_id, :reference, :method_payment, :presence => true
  validates :adviser_id, :presence => {:message => 'Debe asociar al usuario el asesor con el perfil administrador'}
  validates :quote_details, presence: true

 
  #, :if => "self.parent.status == 'validate_quote_details'"


  attr_accessor :client_email, :client_contact, :client_office, :client_phone, :client_address, :commission, :search_description, :search_product, :search_quantity, :add_comment, :assessor_code, :client_nit, :estatus, :name, :numero, :fecha, :hora, :edit_anexes_attributes
  attr_accessor :category, :type, :type_descriptions, :validates_cost, :validates_sent_seller, :adviser_name

  #scope
  scope :by_number, ->(number) {where("number = ?",number)}
  scope :by_status, ->(status) {where("status = ?",status)}
  scope :by_reference, ->(reference) {where("lower(reference) like ?","%#{reference.downcase}%")}
  scope :by_agency, ->(agency) {where("agency_id = ?",agency)}
  scope :by_adviser, ->(adviser) {where("adviser_id = ?",adviser)}
  scope :by_client, ->(client) {where("client_id = ?",client)}
  scope :in_cost, -> {where("status in ('sent_to_cost','quote_prices')")}
  scope :in_seller, -> {where("status in ('elaborated', 'customer_review', 'sent_seller', 'delivered_customer')")}
  scope :in_production_order, -> {where("status in ('approved')")}
  scope :accepted, -> {where("status in ('approved')")}
  scope :rejected, -> {where("status in ('rejected')")}
  scope :in_process, -> {where("status not in ('rejected', 'approved')")}  
  

  validates :coordinator_id, :difficulty_quote_id , presence: true, if: "validates_cost"
  validates :cost_observations,  length: { minimum: 10 }, presence: true, if: "validates_sent_seller"   
  validate :check_if_at_less_one_cost, if: "validates_cost"
#  validates :password, presence: { if: :password_required?, message: 'is forgotten.' }, confirmation: true
               

 

  def self.search_by_query(query)
    result = self.all
    if !query["agency"].nil? && !query["agency"].empty?
      result = result.by_agency(query["agency"])
    end
     if !query["adviser"].nil? && !query["adviser"].empty?
      result = result.by_adviser(query["adviser"])
    end
     if !query["administrador"].nil? && !query["administrador"].empty?
      result = result.by_administrador(query["administrador"])
    end    
    if !query["client"].nil? && !query["client"].empty?
      result = result.by_client(query["client"])
    end
    if !query["reference"].nil? && !query["reference"].empty?
      result = result.by_reference(query["reference"])
    end
    if !query["status"].nil? && !query["status"].empty?
      result = result.by_status(query["status"])
    end    
    if !query["number"].nil? && !query["number"].empty?
      result = result.by_number(query["number"])
    end
    result
  end
  
  def quote_status                      
      if self.status === 'elaborated'
        return'Elaborada'
      end   
      if self.status === 'customer_review'          
        return'En Revisión del Cliente'
      end                    
      if self.status === 'sent_to_cost'          
        return'Enviada a Costos'
      end  
      if self.status === 'canceled'          
        return'Cancelada'
      end                    
      if self.status === 'quote_prices'
        return 'Activa en Costos'
      end            
      if self.status === 'sent_seller'
        return 'Enviada a Ventas'
      end
      if self.status === 'delivered_customer'
        return 'En Estudio'
      end
      if self.status === 'approved'
        return'Aprobada'
      end
      if self.status === 'rejected'
        return'Rechazada'
      end       
      if self.status === 'cost_report'
        return'Informe de Costos'
      end 
      if self.status === 'back_to_sales'
        return'Devuelta a Ventas'
      end       
  end       

  def valor_total
    self.quote_details.sum("quote_details.quantity * quote_details.price")
  end

  def set_number
    self.number = id.to_s.rjust(6, "0")
    save
  end

  def expected_end
    if difficulty_quote and begin_analysis
      difficulty_quote.quantity.business_days.after(begin_analysis)
    else
      nil
    end
  end

  def time_cost    
    dias = 1
    sent_to_cost=' '
    quote_prices=' '
    sent_seller=' '  
    if self.difficulty_quote_id   
      self.estado.each do |x|
          if x.status=='sent_to_cost'
            sent_to_cost = x.created_at 
          end 
          if x.status=='quote_prices' 
            quote_prices = x.created_at 
          end
          if x.status=='sent_seller' 
            sent_seller = x.created_at 
          end 
      end  
      if quote_prices.to_s==' ' && sent_seller.to_s==' '
        dias =  1 
      else 
        if sent_seller.to_s==' ' && (Time.zone.now.strftime '%d/%m/%Y') == (quote_prices.strftime '%d/%m/%Y')
          dias = 1
        elsif sent_seller.to_s==' ' && (Time.zone.now.strftime '%d/%m/%Y') != (quote_prices.strftime '%d/%m/%Y')       
              dias = ((Time.zone.now - quote_prices)  /86400 ).to_i  
           elsif (sent_seller.strftime '%d/%m/%Y') == (quote_prices.strftime '%d/%m/%Y')   
              dias = 1
            else
              dias = dias = ((sent_seller - quote_prices)  /86400 ).to_i      
           end  
      end  
    else
        dias = ''
    end  
   return  dias
  end

  def time_sale   
    dias = 1
    elaborated=nil
    sent_to_cost=nil
    sent_to_cost2=nil
    back_to_sales=nil   
    cantidad = 0  
    self.estado.each do |x|
        if x.status=='elaborated'
          elaborated = x.created_at 
        end 
        if x.status=='sent_to_cost' &&  cantidad == 0 
          sent_to_cost = x.created_at 
          cantidad = cantidad + 1
        else
           sent_to_cost2 = x.created_at 
        end
        if x.status=='back_to_sales' 
          back_to_sales = x.created_at 
        end 
    end  
    
    #byebug
    if elaborated

      if back_to_sales
         dias = 1
         dias1 = 0
         dias2 = 0       
         #byebug
       
         if sent_to_cost2 && elaborated
            dias1 = ((sent_to_cost2 - elaborated)  /86400 ).to_i  
         end

         if sent_to_cost
            dias2 = ((sent_to_cost - back_to_sales)  /86400 ).to_i  
         end     

         dias = dias1 + dias2
      else
        if sent_to_cost==nil  && (Time.zone.now.strftime '%d/%m/%Y') == (elaborated.strftime '%d/%m/%Y')
          dias = 1       
        elsif sent_to_cost==nil && (Time.zone.now.strftime '%d/%m/%Y') != (elaborated.strftime '%d/%m/%Y')
          dias = ((Time.zone.now - elaborated)  /86400 ).to_i        
        else         
          if sent_to_cost && (sent_to_cost.strftime '%d/%m/%Y') == (elaborated.strftime '%d/%m/%Y')
            dias = 1
          elsif sent_to_cost              
                   dias = dias = ((sent_to_cost - elaborated)  /86400 ).to_i
          end                
        end 

      end  #back
    end
      
   return  dias
  end  

  def estado_cost     
    difficulty_quote = self.try(:difficulty_quote).try(:quantity)
    if difficulty_quote
       if difficulty_quote.to_i >= self.time_cost.to_i
          estado = 'Se Cumplio' 
       else   
          estado = 'Se Incumplio' 
       end 
    else
       estado = ''  
    end  
    return  estado        
  end  

  def sent_to_cost      
    sent_to_cost=nil       
    self.estado.each do |x|
        if x.status=='sent_to_cost'
          sent_to_cost = x.created_at 
        end 
    end  
   return  sent_to_cost
  end  


  def quote_prices    
    quote_prices=nil   
    self.estado.each do |x|
        if x.status=='quote_prices'
          quote_prices = x.created_at 
        end 
    end  
   return  quote_prices
  end    

  def sent_seller    
    sent_seller=nil
    self.estado.each do |x|
        if x.status=='sent_seller'
          sent_seller = x.created_at 
        end 
    end  
   return  sent_seller
  end   


  def days_to_end
    ee = expected_end
    if ee
      deis = (ee.to_date.mjd - Date.today.mjd).to_i
      if deis < 0 
        0
      else
        deis
      end
    else
      0
    end
    
  end

  def registrar_historico      
      status_quotes = StatusQuote.where('quote_id=?',id)
      user_costos = Administrador.where("rol='costo'")

      if status == 'customer_review' || status == 'sent_to_cost' || status == 'sent_seller' || status == 'back_to_sales'
        ActionCorreo.cotizacion_email(id, status, administrador, client, status_quotes, user_costos).deliver
      end

      if status == 'delivered_customer'  || status == 'approved' || status == 'rejected' || status == 'cost_report'
        ActionCorreo.cotizacion_email_costos_cliente(id, status, administrador, client, status_quotes, user_costos).deliver
      end
  end 

  def check_if_at_less_one_cost
    quote_details.each do |qd|
      return true if qd.price.to_i > 0
    end
    errors.add(:base, "Debe Agregar al menos un precio")
  end

end
