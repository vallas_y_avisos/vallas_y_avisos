class RemissionDetail < ActiveRecord::Base
	belongs_to :remission
	belongs_to :product

	validates_presence_of :product_id, :quantity

     validates :quantity,
    numericality: {greater_than: 0, allow_blank: true },
    presence: true

    validate :products_count

    attr_accessor :production_order_id

    def products_count
      if self.production_order_id.present?  
        order = ProductionOrder.find(self.production_order_id) 
        cant = order.remission_details.where(product:self.product).sum(:quantity)
        cant_quotes = order.quote.quote_details.where(product:self.product).sum(:quantity)
        if self.quantity + cant > cant_quotes
            errors.add :quantity,"La cantidad suministrada supera a la cantidad solicitada."
        end
      end
    end
end
