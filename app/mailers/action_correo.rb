class ActionCorreo < ActionMailer::Base
  def bienvenido_email()   
    mail(from: "no-reply@vallasyavisos.com", to: 'marquezdigna83@gmail.com', subject: 'Prueba de envio de Correo')
  end

  def enviar_alertas_email(alerts, user)
    @alerts = alerts
    #byebug
    @user = user
    if   @user.email 
      email_with_name1 =@user.email
      email_with_name2 =''
      mail(from: "no-reply@vallasyavisos.me", to: [email_with_name1, email_with_name2], subject: 'Alertas Vallas y Avisos')
    end
  end  

  def cambio_estatus_email(id, status, user)
    @id = id
    @status = status
    #byebug
    @user = user
    if   @user.email 
      email_with_name1 =@user.email
      email_with_name2 =''
      mail(from: "no-reply@vallasyavisos.me", to: [email_with_name1, email_with_name2], subject: 'Cambio de Estatus - Vallas y Avisos')
    end
  end   

  def cotizacion_email(id, status, user, client, status_quotes, user_costos)
    @quote = Quote.find(id)
    @detalle = @quote_details = QuoteDetail.joins('LEFT OUTER JOIN products  ON products.id = quote_details.product_id').select('quote_details.id, quote_details.quantity, quote_details.price, quote_details.description, quote_details.product_id, products.name').where(quote_id: id).reorder('')        
    @costos_email = []
    email_quote_prices = ''
     
     if  status == 'customer_review'
      @costos_email[0]=user.email
      @costos_email[1]=client.email
     end 

     if  status == 'sent_to_cost'
      @costos_email[0]=user.email
      #@costos_email[1]=client.email
      i=1
      user_costos.each do  |usercosto|       
        @costos_email[i] = usercosto.email
        i =i+1
      end
     end   
    
    status_quotes.each do  |quote_prices|       
      if quote_prices.status == 'quote_prices'
         id_user=quote_prices.administrador_id
         user_status=Administrador.find(id_user)
         email_quote_prices=user_status.email
         #byebug
      end
    end      

    if  status == 'sent_seller'  || status == 'back_to_sales'

     @costos_email[0]=user.email   
     @costos_email[1]= email_quote_prices
   
    end       
   
    @user = user
    if   @costos_email 
      mail(from: "no-reply@vallasyavisos.me", to: @costos_email, subject: 'Cambio de Estatus - Vallas y Avisos')
    end
  end    

def cotizacion_email_elaborate(id, status, user)
    @quote = Quote.find(id)
    @detalle = @quote_details = QuoteDetail.joins('LEFT OUTER JOIN products  ON products.id = quote_details.product_id').select('quote_details.id, quote_details.quantity, quote_details.price, quote_details.description, quote_details.product_id, products.name').where(quote_id: id).reorder('')        
    @costos_email = []


     if status == 'elaborated'
      @costos_email[0]=user.email
     end                 

    #byebug
    
    @user = user
    if   @costos_email 
      mail(from: "no-reply@vallasyavisos.me", to: @costos_email, subject: 'Cotizacion Elaborada - Vallas y Avisos')
    end
  end   

def cotizacion_email_costos(id, user)
    @quote = Quote.find(id)
    @detalle = @quote_details = QuoteDetail.joins('LEFT OUTER JOIN products  ON products.id = quote_details.product_id').select('quote_details.id, quote_details.quantity, quote_details.price, quote_details.description, quote_details.product_id, products.name').where(quote_id: id).reorder('')        
    @costos_email = []
    
    @costos_email[0]=user.email
               
    #byebug
    
    @user = user
    if   @costos_email 
      mail(from: "no-reply@vallasyavisos.me", to: @costos_email, subject: 'Precios Asignados a la Cotizacion - Vallas y Avisos')
    end
  end  


 def cotizacion_email_costos_cliente(id, status, user, client, status_quotes, user_costos)
    @quote = Quote.find(id)
    @detalle = @quote_details = QuoteDetail.joins('LEFT OUTER JOIN products  ON products.id = quote_details.product_id').select('quote_details.id, quote_details.quantity, quote_details.price, quote_details.description, quote_details.product_id, products.name').where(quote_id: id).reorder('')        
    @costos_email = []
    email_quote_prices = ''  
    subject_email = ''

    status_quotes.each do  |quote_prices|       
      if quote_prices.status == 'quote_prices'
         id_user=quote_prices.administrador_id
         user_status=Administrador.find(id_user)
         email_quote_prices=user_status.email
         #byebug
      end
    end             

    if  status == 'delivered_customer' 
     #byebug
     @costos_email[0]=user.email   
     @costos_email[1]= email_quote_prices # no esta enviado al de costo
     @costos_email[2]=client.email
     subject_email = 'Cotizacion Enviada al Cliente - Vallas y Avisos'
       
    end  

    if  status == 'approved' || status == 'rejected'

     @costos_email[0]=user.email   
     @costos_email[1]= email_quote_prices
     @costos_email[2]=client.email

     if  status == 'approved'
       subject_email = 'Cotizacion Aprobada - Vallas y Avisos'    
     end  

     if  status == 'rejected'
       subject_email = 'Cotizacion No Aprobada - Vallas y Avisos'    
     end        
   
    end     

    if  status == 'cost_report'

     @costos_email[0]=user.email   
     @costos_email[1]= email_quote_prices
     subject_email = 'Enviar a Informe de Costos - Vallas y Avisos'
    end        
      

    #byebug
    
    @user = user
    if   @costos_email 
      mail(from: "no-reply@vallasyavisos.me", to: @costos_email, subject: subject_email)
    end
  end   


  def materials_list_email(id, status)
    @materials_list = MaterialsList.joins('LEFT OUTER JOIN production_orders  ON production_orders.id = materials_lists.production_order_id LEFT OUTER JOIN quotes on production_orders.quote_id = quotes.id  LEFT OUTER JOIN  advisers on advisers.id = quotes.adviser_id').select('materials_lists.*, production_orders.number, production_orders.administrador_id,production_orders.number, advisers.name as advisers_name').find(id)        
    @materiales_details = MaterialDetails.where(materials_list_id: id).order(id: :asc) 
    #byebug
    if(@materials_list.administrador_id)
        @user= Administrador.find(@materials_list.administrador_id)
        @materials_list_email = []
        @materials_list_email[0]=@user.email 
        subject_email = ''

         if  status == 'elaborated'
           subject_email = 'Hoja de Calculo Elaborada - Vallas y Avisos'
         end  

         if  status == 'sent_to_production'
           subject_email = 'Hoja de Calculo Enviada a Produccion - Vallas y Avisos'
         end


         if  status == 'sent_to_store'
           subject_email = 'Hoja de Calculo Enviada Almacen - Vallas y Avisos'
         end                                 
         
        if   @materials_list_email 
          mail(from: "no-reply@vallasyavisos.me", to: @materials_list_email, subject: subject_email)
        end
      end   
    end
end