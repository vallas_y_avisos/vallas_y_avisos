require "rails_admin/helpers/application_decorator"

module ApplicationHelper
  include BootstrapHelpers::AlertHelper
      
  def flash_messages
    html = ""
    flash.each do |key, value|
      html << content_tag(:div, 
        link_to("×", "#", :'data-dismiss' => "alert", :class => "close") + raw(value),
        :class => "alert alert-#{key}")
    end
    
    html.html_safe
  end

  # Crea un enlace con icono
  # Para un boton de acción (solo icono) especificar html_option[:class] la clase  btn-action
  def image_link_to(name=nil, options={}, html_options = {})
    css_class = html_options.delete(:class) || ""
    css_class << " btn-icon" if css_class.match('btn-icon').nil? && css_class.match('btn-action').nil?
    css_class << " btn glyphicons"
    html_options[:class]= css_class
    link_to(options,html_options ){ content_tag('i') + name }    
  end

  def user_or_admin_logout_path
    scope = current_user.class.to_s.downcase
    main_app.send("destroy_#{scope}_session_path") rescue false
  end
  
  def simple_vertical_form_for(record, options={}, &block)
    result = simple_form_for(record, options, &block)
    result.gsub(/#{SimpleForm.form_class}/, "simple_form").html_safe
  end
  
  def dir_images_reports
    Rails.root.join('app', 'views', 'reportes', 'images').to_s + "/"
  end

  def dir_images_reports
    Rails.root.join('app', 'views', 'reportes', 'images').to_s + "/"
  end

end
