class DifficultyQuotesController < ApplicationController
  before_action :set_difficulty_quote, only: [:show, :edit, :update, :destroy]

  # GET /difficulty_quotes
  # GET /difficulty_quotes.json
  def index
    @difficulty_quotes = DifficultyQuote.all
  end

  # GET /difficulty_quotes/1
  # GET /difficulty_quotes/1.json
  def show
  end

  # GET /difficulty_quotes/new
  def new
    @difficulty_quote = DifficultyQuote.new
  end

  # GET /difficulty_quotes/1/edit
  def edit
  end

  # POST /difficulty_quotes
  # POST /difficulty_quotes.json
  def create
    @difficulty_quote = DifficultyQuote.new(difficulty_quote_params)

    respond_to do |format|
      if @difficulty_quote.save
        format.html { redirect_to @difficulty_quote, notice: 'Difficulty quote was successfully created.' }
        format.json { render action: 'show', status: :created, location: @difficulty_quote }
      else
        format.html { render action: 'new' }
        format.json { render json: @difficulty_quote.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /difficulty_quotes/1
  # PATCH/PUT /difficulty_quotes/1.json
  def update
    respond_to do |format|
      if @difficulty_quote.update(difficulty_quote_params)
        format.html { redirect_to @difficulty_quote, notice: 'Difficulty quote was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @difficulty_quote.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /difficulty_quotes/1
  # DELETE /difficulty_quotes/1.json
  def destroy
    @difficulty_quote.destroy
    respond_to do |format|
      format.html { redirect_to difficulty_quotes_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_difficulty_quote
      @difficulty_quote = DifficultyQuote.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def difficulty_quote_params
      params.require(:difficulty_quote).permit(:name, :quantity, :lapse, :type_lapse)
    end
end
