class WelcomeController < ApplicationController
  
  def index    
    @alerts = AlertUser.joins(:alert).includes(:alert).where("administrador_id=?",current_user.id).order('alerts.created_at')
  end

end