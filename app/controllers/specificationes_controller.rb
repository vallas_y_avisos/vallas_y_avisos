class SpecificationesController < ApplicationController
  before_action :set_specification, only: [:show, :edit, :update, :destroy]

  # GET /specificationes
  # GET /specificationes.json
  def index
    @specificationes = Specification.all
  end

  # GET /specificationes/1
  # GET /specificationes/1.json
  def show
  end

  # GET /specificationes/new
  def new
    @specification = Specification.new
  end

  # GET /specificationes/1/edit
  def edit
  end

  # POST /specificationes
  # POST /specificationes.json
  def create
    @specification = Specification.new(specification_params)

    respond_to do |format|
      if @specification.save
        format.html { redirect_to @specification, notice: 'Specification was successfully created.' }
        format.json { render action: 'show', status: :created, location: @specification }
      else
        format.html { render action: 'new' }
        format.json { render json: @specification.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /specificationes/1
  # PATCH/PUT /specificationes/1.json
  def update
    respond_to do |format|
      if @specification.update(specification_params)
        format.html { redirect_to @specification, notice: 'Specification was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @specification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /specificationes/1
  # DELETE /specificationes/1.json
  def destroy
    @specification.destroy
    respond_to do |format|
      format.html { redirect_to specificationes_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_specification
      @specification = Specification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def specification_params
      params.require(:specification).permit(:name)
    end
end
