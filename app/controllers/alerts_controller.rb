class AlertsController < ApplicationController

  # PATCH/PUT /alerts/1
  # PATCH/PUT /alerts/1.json
  def update
    @AlertUser = AlertUser.find(params[:id])

    respond_to do |format|      
      if @AlertUser.update_attributes(viewed: 'TRUE')  
          @AlertUser = AlertUser.joins('LEFT OUTER JOIN alerts  ON alerts.id = alert_users.alert_id').select('alerts.id , alerts.type_alert, alerts.description, alerts.model_id, alerts.days').where("administrador_id=? and alert_users.viewed='FALSE'",current_user.id)
          @count_alert = @AlertUser.count       
          format.json {  render :json => @count_alert }  
      end
    end

  end


  def show
      @alerts = AlertUser.joins('LEFT OUTER JOIN alerts  ON alerts.id = alert_users.alert_id').select('alert_users.id , alerts.type_alert, alerts.description, alerts.model_id, alerts.days').where("administrador_id=? and alert_users.viewed='FALSE'",current_user.id).order('type_alert')
      render :json => @alerts
  end




end
