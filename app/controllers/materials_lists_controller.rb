class MaterialsListsController < ApplicationController
  before_action :set_materials_list, only: [:show, :edit, :update, :destroy, :sent_to_production, :sent_to_store]
  before_action :configure_list  

  # GET /materials_lists
  # GET /materials_lists.json
  def index
    #@materials_lists = MaterialsList.all
    #@materials_lists = MaterialsList.joins('LEFT OUTER JOIN production_orders  ON production_orders.id = materials_lists.production_order_id').select('materials_lists.*, production_orders.number').all
    @materials_lists = MaterialsList.joins('LEFT OUTER JOIN production_orders  ON production_orders.id = materials_lists.production_order_id LEFT OUTER JOIN quotes  ON quotes.id = production_orders.quote_id LEFT OUTER JOIN clients  ON clients.id = quotes.client_id').select('materials_lists.id, materials_lists.date_delivery, materials_lists.prepared_by,materials_lists.status, materials_lists.created_at , production_orders.number, quotes.id as quotes_id, clients.name').all
  end

  # GET /materials_lists/1
  # GET /materials_lists/1.json
  def show
    @materials_list.materiales_details
  end

  # GET /materials_lists/new
  def new
    @materials_list = MaterialsList.new    
    @materials_list.materiales_details.build
  end

  # GET /materials_lists/1/edit
  def edit
    @materials_list.materiales_details
    #byebug     
  end

  # POST /materials_lists
  # POST /materials_lists.json
  def create
    #byebug
    @materials_list = MaterialsList.new(materials_list_params)

    respond_to do |format|
      if @materials_list.save
        format.html { redirect_to materials_lists_path, notice: 'La Hoja de Calculo fue creada satisfactoriamente.' }
        format.json { render action: 'show', status: :created, location: @materials_list }
      else
        format.html { render action: 'new' }
        format.json { render json: @materials_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /materials_lists/1
  # PATCH/PUT /materials_lists/1.json
  def update
    respond_to do |format|
      if @materials_list.update(materials_list_params)
         #byebug
        @materials_list.materiales_details.delete_all
        if params[:materials_list]['materiales_details_attributes']          
          params[:materials_list]['materiales_details_attributes'].each do |key, value|                         
             #byebug
             @materials_list.materiales_details.create(value)       
          end            
        end  
        format.html { redirect_to materials_lists_path, notice: 'La Hoja de Calculo fue Actualizada satisfactoriamente...' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @materials_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /materials_lists/1
  # DELETE /materials_lists/1.json
  def destroy
    @materiales_details.delete_all
    @materials_list.destroy
    respond_to do |format|
      format.html { redirect_to materials_lists_path }
      format.json { head :no_content }
    end
  end

  #GET /materials_lists/:id/sent_to_production
  def sent_to_production
    if @materials_list.elaborated?
      @materials_list.sent_to_production!          
       ActionCorreo.materials_list_email(@materials_list.id, @materials_list.status).deliver  
      redirect_to materials_lists_path, notice: 'Enviada al Departamento de Produccion.'
    else
      render action: 'edit' 
    end
  end  

  #GET /materials_lists/:id/sent_to_production
  def sent_to_store
    if @materials_list.sent_to_production?
      @materials_list.sent_to_store!   
      ActionCorreo.materials_list_email(@materials_list.id, @materials_list.status).deliver    
      redirect_to materials_lists_path, notice: 'Enviada al Departamento de Almacen.'
    else
      render action: 'edit' 
    end
  end    

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_materials_list
      #@materials_list = MaterialsList.find(params[:id])    
      @materials_list = MaterialsList.joins('LEFT OUTER JOIN production_orders  ON production_orders.id = materials_lists.production_order_id LEFT OUTER JOIN quotes on production_orders.quote_id = quotes.id  LEFT OUTER JOIN  advisers on advisers.id = quotes.adviser_id').select('materials_lists.*, production_orders.number, production_orders.number, advisers.name as advisers_name').find(params[:id])        
      @materiales_details = MaterialDetails.where(materials_list_id: params[:id]).order(id: :asc) 
    end

    def configure_list
      @clients = Client.all       
      @production_orders = ProductionOrder.all
    end    

    # Never trust parameters from the scary internet, only allow the white list through.
    def materials_list_params
      params.require(:materials_list).permit(:production_order_id, :op, :date_delivery, :cc, :denomination, :size_comp, :quantity, :length_comp, :width_comp, :date_elaboration, :prepared_by, :fototelones_value, :product_description,
        materiales_details_attributes: [:id, :materials_name, :quantity, :materials_unit, :description, :observation, :materials_reference, :_destroy])
    end
end
