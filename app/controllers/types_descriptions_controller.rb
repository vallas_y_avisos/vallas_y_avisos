class TypesDescriptionsController < ApplicationController
  before_action :set_type_description, only: [:show, :edit, :update, :destroy]

  # GET /types_descriptions
  # GET /types_descriptions.json
  def index
    if params[:type_id]
      @types_descriptions = TypeDescription.where(type_id: params[:type_id])
    else
      @types_descriptions = TypeDescription.all
    end
  end

  # GET /types_descriptions/1
  # GET /types_descriptions/1.json
  def show
  end

  # GET /types_descriptions/new
  def new
    @type_description = TypeDescription.new
  end

  # GET /types_descriptions/1/edit
  def edit
  end

  # POST /types_descriptions
  # POST /types_descriptions.json
  def create
    @type_description = TypeDescription.new(type_description_params)

    respond_to do |format|
      if @type_description.save
        format.html { redirect_to @type_description, notice: 'Type description was successfully created.' }
        format.json { render action: 'show', status: :created, location: @type_description }
      else
        format.html { render action: 'new' }
        format.json { render json: @type_description.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /types_descriptions/1
  # PATCH/PUT /types_descriptions/1.json
  def update
    respond_to do |format|
      if @type_description.update(type_description_params)
        format.html { redirect_to @type_description, notice: 'Type description was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @type_description.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /types_descriptions/1
  # DELETE /types_descriptions/1.json
  def destroy
    @type_description.destroy
    respond_to do |format|
      format.html { redirect_to types_descriptions_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_type_description
      @type_description = TypeDescription.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def type_description_params
      params[:type_description]
    end
end
