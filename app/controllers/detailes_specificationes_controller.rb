class DetailesSpecificationesController < ApplicationController
  before_action :set_detail_specification, only: [:show, :edit, :update, :destroy]

  # GET /detailes_specificationes
  # GET /detailes_specificationes.json
  def index
    @detailes_specificationes = DetailSpecification.all
  end

  # GET /detailes_specificationes/1
  # GET /detailes_specificationes/1.json
  def show
  end

  # GET /detailes_specificationes/new
  def new
    @detail_specification = DetailSpecification.new
  end

  # GET /detailes_specificationes/1/edit
  def edit
  end

  # POST /detailes_specificationes
  # POST /detailes_specificationes.json
  def create
    @detail_specification = DetailSpecification.new(detail_specification_params)

    respond_to do |format|
      if @detail_specification.save
        format.html { redirect_to @detail_specification, notice: 'Detail specification was successfully created.' }
        format.json { render action: 'show', status: :created, location: @detail_specification }
      else
        format.html { render action: 'new' }
        format.json { render json: @detail_specification.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /detailes_specificationes/1
  # PATCH/PUT /detailes_specificationes/1.json
  def update
    respond_to do |format|
      if @detail_specification.update(detail_specification_params)
        format.html { redirect_to @detail_specification, notice: 'Detail specification was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @detail_specification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /detailes_specificationes/1
  # DELETE /detailes_specificationes/1.json
  def destroy
    @detail_specification.destroy
    respond_to do |format|
      format.html { redirect_to detailes_specificationes_url }
    end
  end

  
  def show_specification 
     @DetailSpecificationes = DetailSpecification.joins('LEFT OUTER JOIN specificationes  ON specificationes.id = detailes_specificationes.specification_id').select('specificationes.id, specificationes.name').where(detail_id: params[:id]).reorder('')        
    # render json: @DetailSpecificationes
    respond_to do |format|  ## Add this
      format.json { render json: @DetailSpecificationes , status: :ok}
    end 
  end    

  def show_specification_all 
     @DetailSpecificationes = DetailSpecification.joins('LEFT OUTER JOIN specificationes  ON specificationes.id = detailes_specificationes.specification_id').select('detailes_specificationes.detail_id,specificationes.id, specificationes.name').reorder('')        
    # render json: @DetailSpecificationes
    respond_to do |format|  ## Add this
      format.json { render json: @DetailSpecificationes , status: :ok}
    end 
  end    

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_detail_specification
      @detail_specification = DetailSpecification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def detail_specification_params
      params.require(:detail_specification).permit(:specification_id, :detail_id)
    end
end
