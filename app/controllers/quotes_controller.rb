class QuotesController < ApplicationController
  before_action :set_quote, only: [:customer_review, :sent_to_cost, :back_to_sales, :canceled, :quote_prices, :sent_seller, :delivered_customer,  :approved, :rejected, :cost_report, :show, :edit, :update, :destroy, :details]
  before_action :configure_list

  # GET /quotes
  # GET /quotes.json
  def index
    if current_user.is?(:costo) && !current_user.is?(:admin)
       @quotes = Quote.where("status in ('sent_to_cost','quote_prices') and coordinator_id is null ")
    elsif current_user.is?(:vendedor) && !current_user.is?(:admin)
       @quotes = Quote.where(administrador_id: current_user.id)
    else
       @quotes = Quote.all
    end
    #@quotes = Quote.joins('LEFT OUTER JOIN clients  ON clients.id = quotes.client_id').select('quotes.id, quotes.created_at, quotes.status, clients.name').reorder('')
    #@quotes = Quote.joins("left outer join status_quotes on status_quotes.quote_id=quotes.id").select('status_quotes.*, quotes.*').reorder('')
  end

  def list_cost
    if current_user.is?(:admin) 
      @quotes = Quote.all
    elsif !current_user.is?(:analista) 
      @quotes = Quote.where("status in ('sent_to_cost','quote_prices', 'delivered_customer', 'approved', 'rejected', 'sent_seller', 'back_to_sales')")
    elsif current_user.is?(:analista) 
      @quotes = Quote.where("status in ('sent_to_cost','quote_prices', 'delivered_customer', 'approved', 'rejected', 'sent_seller', 'back_to_sales')").where(coordinator_id: current_user.id)
    end    
  end

  # GET /quotes/1
  # GET /quotes/1.json
  def show
    @detalle = @quote_details = QuoteDetail.joins('LEFT OUTER JOIN products  ON products.id = quote_details.product_id').select('quote_details.id, quote_details.quantity, quote_details.price, quote_details.description, quote_details.product_id, quote_details.specificationes, products.name').where(quote_id: params[:id])
    #byebug
    respond_to do |format|
      format.html { render action: 'show' }
      format.pdf { render pdf: "presupuesto_#{params[:id]}" }
    end

  end

  def details
    @quote_detail = {"id" => @quote.id, 
                      "agency_name" => @quote.agency.try(:name), 
                      "date" => @quote.try(:created_at),
                      "client_name" => @quote.client.try(:name), 
                      "client_phone" => @quote.client.try(:phone), 
                      "adviser_name" => @quote.adviser.try(:name), 
                      "adviser_assessor_code" => @quote.adviser.try(:assessor_code),                       
                      "adviser_agency_id" => @quote.adviser.try(:agency_id),                       
                      "commission_quote" => @quote.try(:commission_quote), 
                      "client_nit" => @quote.client.try(:nit), 
                      "client_address" => @quote.client.try(:address), 
                      "quote_status" => @quote.try(:status), 
                      "quote_reference" => @quote.try(:reference)}  
    
    respond_to do |format|  ## Add this
      format.json { render json: @quote_detail , status: :ok}
    end 
  end

  # GET /quotes/new
  def new    
    @quote = Quote.new
    @detalle = @quote.quote_details
    @quote.quotes_anexes.build 
    @client = Client.new  

  end

  # GET /quotes/1/edit
  def edit
    @quote.quote_details
    if @quote.quotes_anexes.count > 0
      @quote.quotes_anexes
      #byebug
    else
      @quote.quotes_anexes.build 
    end  
    #@detalle = QuoteDetail.where(quote_id: params[:id]).order(id: :asc) 
    @detalle = @quote_details = QuoteDetail.joins('LEFT OUTER JOIN products  ON products.id = quote_details.product_id').select('quote_details.id, quote_details.quantity, quote_details.price, quote_details.description, quote_details.specificationes, quote_details.product_id, products.name').where(quote_id: params[:id])

  end

    # GET /quotes/1/costs
  def costs   
    @quote = Quote.find(params[:quote_id])      
    @detalle = QuoteDetail.joins(:product).where(quote_id: params[:quote_id])

    # save firt time analist came in.   
    if @quote.begin_analysis.nil? && current_user.is?(:analista) && @quote.coordinator_id == current_user.id && !current_user.is?(:admin)
      @quote.begin_analysis = Time.now
      @quote.save
    end 
   
  end

  # PATCH/PUT /quotes/1
  # PATCH/PUT /quotes/1.json
  def update_costs
    respond_to do |format|      
      @quote = Quote.find(params[:quote_id]) 
      @quote.validates_cost=true
      if @quote.update(quote_params)  

        if @quote.sent_to_cost? 
           @quote.quote_prices!  
           StatusQuote.create :status =>@quote.status, :quote_id =>@quote.id , :administrador_id =>current_user.id
           ActionCorreo.cotizacion_email_costos(@quote.id, current_user).deliver
           format.html { redirect_to list_cost_quotes_path, notice: 'Los Costos de la Cotización se actualizaron satisfactoriamente.' }
           format.json { head :no_content }           

        elsif @quote.quote_prices?
          #proceso para validar la fecha
          @quote.validates_cost=false
       
          if @quote.estado_cost == 'Se Incumplio'
            @quote.validates_sent_seller=true 
          else
            @quote.validates_sent_seller=false      
          end
            
          #byebug
          if @quote.sent_seller!
            StatusQuote.create :status =>@quote.status, :quote_id =>@quote.id , :administrador_id =>current_user.id              
            format.html { redirect_to list_cost_quotes_path, notice: 'La Cotizacion fue enviada a Ventas.'  }
            format.json { head :no_content }
          else
            format.html { redirect_to list_cost_quotes_path, notice: 'La Cotizacion No pudo ser enviada, Verifique que dejo la observacion por demora en el envio.'  }
          end
        end

      ##fin
      else
        format.html { render action: 'costs' }
        format.json { render json: @quote.errors, status: :unprocessable_entity }
      end
    end
  end  

  # GET /quotes/search
  def search
    @quotes = Quote.search_by_query(params[:search])
    render :index
  end


  # POST /quotes
  # POST /quotes.json
  def create
    @quote = Quote.new(quote_params)
    @quote.validates_cost=false
    @quote.validates_sent_seller=false
    @quote.administrador = current_user
    respond_to do |format|
      if @quote.save
       StatusQuote.create :status =>@quote.status, :quote_id =>@quote.id , :administrador_id =>current_user.id
        if  params[:quote]['quotes_anexes_attributes']
          params[:quote]['quotes_anexes_attributes'].each do |key, value|        
              value['anexo'].each do |picture|     
                 @quote.quotes_anexes.create(:anexo=> picture)       
              end  
          end  
        end  

      #byebug
        format.html { redirect_to quotes_path, notice: 'La Cotización fue creada satisfactoriamente.' }
        format.json { render action: 'show', status: :created, location: @quote }
      else
        @detalle = @quote.quote_details
        format.html { render action: 'new' }
        format.json { render json: @quote.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /quotes/1
  # PATCH/PUT /quotes/1.json
  def update
    
    respond_to do |format|
      if @quote.update(quote_params)
       #@quote.quotes_anexes.delete_all

        id_anexos = [] 
        i=0
        if  params[:quote]['edit_anexes_attributes']
          params[:quote]['edit_anexes_attributes'].each do |key, value|        
             id_anexos[i]=value["id"]
             i=i+1
          end  
        end 

        @quotes_anexes_del = QuoteAnexes.where(quote_id: @quote.id).where.not(id: id_anexos).select('quotes_anexes.id')
        QuoteAnexes.where(id: @quotes_anexes_del).delete_all

        if  params[:quote]['quotes_anexes_attributes']
          params[:quote]['quotes_anexes_attributes'].each do |key, value|        
             if value['anexo']
                value['anexo'].each do |picture| 
                  if  picture  
                      @quote.quotes_anexes.create(:anexo=> picture)       
                  end 
                end 
             end   
          end  
        end          
        #byebug
        @quote.quote_details.delete_all
        if params[:quote]['quote_details_attributes']          
          params[:quote]['quote_details_attributes'].each do |key, value|                         
             #byebug
             @quote.quote_details.create(value)       
          end            
        end  
        format.html { redirect_to quotes_path, notice: 'La Cotización fue actualizada satisfactoriamente.' }
        format.json { head :no_content }
      else
        @detalle = @quote.quote_details
        format.html { render action: 'edit' }
        format.json { render json: @quote.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /quotes/1
  # DELETE /quotes/1.json
  def destroy
    #byebug
    @quote_details.delete_all
    @quotes_anexes.delete_all 
    #@quote.quote_details.build
    @status_quote.delete_all
    @quote.destroy        
    #byebug
    #redirect_to  
    respond_to do |format|
      format.html { redirect_to @quote, notice: 'La Cotización fue eliminada satisfactoriamente.' }
      format.json { head :no_content }
    end
  end

  #estatus de cotizacion

  #GET /quotes/:id/customer_review
  def customer_review
    if @quote.elaborated?
      @quote.customer_review!   
      StatusQuote.create :status =>@quote.status, :quote_id =>@quote.id , :administrador_id =>current_user.id  
      redirect_to quotes_path, notice: 'La Cotizacion fue Revisada por el cliente.'
    else
      render action: 'index' 
    end
  end

  #GET /quotes/:id/sent_to_cost
  def sent_to_cost
    if @quote.elaborated? or @quote.back_to_sales? 
      @quote.validates_cost=false
      @quote.validates_sent_seller=false
      @quote.sent_to_cost!    
      StatusQuote.create :status =>@quote.status, :quote_id =>@quote.id , :administrador_id =>current_user.id 
      redirect_to quotes_path, notice: 'La Cotizacion fue enviada a Costos.'
    else
      render action: 'index' 
    end
  end


  #GET /quotes/:id/canceled
  def canceled
    if @quote.elaborated?
      @quote.canceled!
      StatusQuote.create :status =>@quote.status, :quote_id =>@quote.id , :administrador_id =>current_user.id 
      redirect_to quotes_path, notice: 'La Cotizacion fue Cancelada.'
    else
      render action: 'index' 
    end
  end  

  #GET /quotes/:id/back_to_sales
  def back_to_sales 
    if @quote.sent_to_cost?
      @quote.back_to_sales!    
      StatusQuote.create :status =>@quote.status, :quote_id =>@quote.id , :administrador_id =>current_user.id 
      redirect_to quotes_path, notice: 'La Cotizacion fue Devuelta a Ventas.'
    else
      render action: 'index' 
    end
  end  

   #GET quote_prices
  def quote_prices
    if @quote.sent_to_cost?
      @quote.validates_cost=true
      @quote.validates_sent_seller=false
      @quote.quote_prices!      
      redirect_to list_cost_quotes_path, notice: 'Precios Asignados a la Cotizacion.'      
    else
      render action: 'index' 
    end
  end  


   #GET sent_seller #OJO YA NO LA USO PASO A EL UPDATE DE COSTO
  def sent_seller   
    @quote = Quote.find(params[:id]) 
    #byebug
    if @quote.quote_prices?   
      @quote.sent_seller!
      StatusQuote.create :status =>@quote.status, :quote_id =>@quote.id , :administrador_id =>current_user.id
      # @quote.delivered_customer!
      # StatusQuote.create :status =>@quote.status, :quote_id =>@quote.id , :administrador_id =>current_user.id
      redirect_to list_cost_quotes_path, notice: 'La Cotizacion fue enviada al Vendedor.'
    else
      render action: 'index' 
    end
  end  


   #GET delivered_customer
  def delivered_customer
    if @quote.sent_seller?
      @quote.delivered_customer!
      StatusQuote.create :status =>@quote.status, :quote_id =>@quote.id , :administrador_id =>current_user.id
      redirect_to quotes_path, notice: 'La Cotizacion fue enviada al Cliente.'      
    else
      render action: 'index' 
    end
  end    

 #GET approved
  def approved
    if @quote.delivered_customer?
      @quote.approved!
      StatusQuote.create :status =>@quote.status, :quote_id =>@quote.id , :administrador_id =>current_user.id
      redirect_to quotes_path, notice: 'La Cotizacion fue Aprobada.'      
    else
      index
      render action: 'index' 
    end
  end

 #GET rejected
  def rejected
    #byebug
    if @quote.delivered_customer?
      @quote.rejected!
      StatusQuote.create :status =>@quote.status, :quote_id =>@quote.id , :administrador_id =>current_user.id
      comment = Comment.create title: "Seguimiento Cotizacion - Rechazada", comment: "Rechazada por el cliente", :commentable_id =>@quote.id, commentable_type: "Quote", :quote_id =>@quote.id
      redirect_to edit_comment_path(comment), notice: 'La Cotizacion no Fue Aprobada, por favor agregue la razón del rechazo.' 
    else
      index
      render action: 'index' 
    end
  end 

 #GET cost_report
  def cost_report
    if @quote.rejected?
      @quote.cost_report!
      StatusQuote.create :status =>@quote.status, :quote_id =>@quote.id , :administrador_id =>current_user.id
      redirect_to list_cost_quotes_path, notice: 'La Cotizacion Cambio a Informe de Costos.'      
    else
      index
      render action: 'index' 
    end
  end     

  #fin de estatus de la cotizacion

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quote
      @quote = Quote.find(params[:id])
      @quote_details = QuoteDetail.where(quote_id: params[:id]).order(id: :asc)            
      @quotes_anexes = QuoteAnexes.where(quote_id: params[:id]).order(id: :asc)            
      @status_quote = Estado.where(quote_id: params[:id]).order(id: :asc)            
      

    end

    def configure_list
      @agencies = Agency.all
      @advisers_c = Adviser.where(id: current_user.adviser_id)
      @advisers = Adviser.all
      @difficulties = DifficultyQuote.where(is_quote: true).order(id: :asc)
      @administradores = Administrador.where(rol: "analista").order(id: :desc)
      #@administradores = Administrador.where(" administradores.rol in  ('costo','analista')").order(id: :desc)
      @clients = Client.all     
      @products = Product.all   
      @quotes_list = Quote.all
      #@comment = Comment.where(commentable_id: params[:id]) 
      @comments = Comment.joins('LEFT OUTER JOIN administradores  ON administradores.id = comments.administrador_id').select('comments.id, comments.commentable_id, comments.comment, comments.created_at, administradores.nombre').where(commentable_id: params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def quote_params
      params.require(:quote).permit(:number, :reference, :commission_quote, :form_delivery, :method_payment, :date_delivery_costs, :adviser_id, :adviser_name, :client_id, :agency_id, :difficulty_quote_id, :coordinator_id, :cost_observations, :spreadsheet,
        quote_details_attributes: [ :id, :description, :quantity, :price, :product_id, :specificationes, :_destroy ],
        quotes_anexes_attributes: [ :id, :anexo, :_destroy],
        edit_anexes_attributes: [ :id, :_destroy] )
    end

end
