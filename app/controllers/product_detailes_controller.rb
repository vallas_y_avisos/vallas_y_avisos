class ProductDetailesController < ApplicationController
  before_action :set_product_detail, only: [:show, :edit, :update, :destroy]

  # GET /product_detailes
  # GET /product_detailes.json
  def index
    @product_detailes = ProductDetail.all
  end

  # GET /product_detailes/1
  # GET /product_detailes/1.json
  def show
  end

  # GET /product_detailes/new
  def new
    @product_detail = ProductDetail.new
  end

  # GET /product_detailes/1/edit
  def edit
  end

  # POST /product_detailes
  # POST /product_detailes.json
  def create
    @product_detail = ProductDetail.new(product_detail_params)

    respond_to do |format|
      if @product_detail.save
        format.html { redirect_to @product_detail, notice: 'Product detail was successfully created.' }
        format.json { render action: 'show', status: :created, location: @product_detail }
      else
        format.html { render action: 'new' }
        format.json { render json: @product_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /product_detailes/1
  # PATCH/PUT /product_detailes/1.json
  def update
    respond_to do |format|
      if @product_detail.update(product_detail_params)
        format.html { redirect_to @product_detail, notice: 'Product detail was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @product_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_detailes/1
  # DELETE /product_detailes/1.json
  def destroy
    @product_detail.destroy
    respond_to do |format|
      format.html { redirect_to product_detailes_url }
      format.json { head :no_content }
    end
  end

  def show_details     
     @ProductDetailes = ProductDetail.joins('LEFT OUTER JOIN detailes  ON detailes.id = product_detailes.detail_id').select('detailes.name, detailes.id').where(product_id: params[:id]).reorder('')        

     respond_to do |format|  ## Add this
      format.json { render json: @ProductDetailes , status: :ok}
    end 
  end    

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_detail
      @product_detail = ProductDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_detail_params    
      params.require(:product_detail).permit(:product_id, :detail_id)
    end
end
