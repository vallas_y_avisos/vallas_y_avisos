class BillsController < ApplicationController
  before_action :set_bill, only: [:show, :edit, :update, :destroy]
  before_action :set_production_order
  before_action :configure_list

  # GET /bills
  # GET /bills.json
  def index
    if @production_order.present?
      @bills = @production_order.bills
    else
      @bills = Bill.all
    end
  end

  # GET /bills/1
  # GET /bills/1.json
  def show
  end

  # GET /bills/new
  def new
    @bill = Bill.new
    @bill.date_bill = Time.zone.now.strftime '%d/%m/%Y'

    if @production_order.present?
      @products = Product.products_of_quote(@production_order.quote)

      
      @bill.client_name = @production_order.try(:quote).try(:client).try(:name)
      @bill.client_adress = @production_order.try(:quote).try(:client).try(:address)
      @bill.client_phone = @production_order.try(:quote).try(:client).try(:phone)
      @bill.reference = @production_order.try(:quote).try(:reference)
      @bill.production_order = @production_order

      @production_order.quote.quote_details.each do |qd|
        bill_detail = @bill.bill_details.build
        bill_detail.quantity = qd.quantity
        bill_detail.description = qd.description
        bill_detail.bill_id = @bill
        bill_detail.product_id = qd.product_id
        bill_detail.price = qd.price
      end    
    else
      @products = Product.all
    end   
  end

  # GET /work_orders/search
  def search
    @bills = Remission.search_by_query(params[:search])
    @production_order = ProductionOrder.find(params[:search][:production_order_id]) if params[:search][:production_order_id].present?
   
    render :index
  end

  # GET /bills/1/edit
  def edit
    @products = Product.products_of_quote(@bill.production_order.quote) + Product.where("id IN (?)", @bill.bill_details.map(&:product_id))
    
    @bill.client_name = @bill.production_order.try(:quote).try(:client).try(:name)
    @bill.client_adress = @bill.production_order.try(:quote).try(:client).try(:address)
    @bill.client_phone = @bill.production_order.try(:quote).try(:client).try(:phone)
    @bill.reference = @bill.production_order.try(:quote).try(:reference)
  end

  # POST /bills
  # POST /bills.json
  def create
    @bill = Bill.new(bill_params)
    @bill.client_name = @production_order.try(:quote).try(:client).try(:name)
    @bill.client_adress = @production_order.try(:quote).try(:client).try(:address)
    @bill.client_phone = @production_order.try(:quote).try(:client).try(:phone)
    @bill.reference = @production_order.try(:quote).try(:reference)
    
    @bill.bill_details.each do |b|
      b.production_order_id = @production_order.try(:id)
    end
    
    respond_to do |format|
      if @bill.save
        format.html { redirect_to bills_path(@bill.production_order), notice: 'Bill was successfully created.' }
        format.json { render action: 'show', status: :created, location: @bill }
      else
        format.html { render action: 'new' }
        format.json { render json: @bill.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bills/1
  # PATCH/PUT /bills/1.json
  def update
    @bill.client_name = @production_order.try(:quote).try(:client).try(:name)
    @bill.client_adress = @production_order.try(:quote).try(:client).try(:address)
    @bill.client_phone = @production_order.try(:quote).try(:client).try(:phone)
    @bill.reference = @production_order.try(:quote).try(:reference)
    @bill.bill_details.each do |b|
      b.production_order_id = @production_order.try(:id)
    end
    respond_to do |format|
      if @bill.update(bill_params)
        format.html { redirect_to bills_path(@bill.production_order), notice: 'Bill was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @bill.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bills/1
  # DELETE /bills/1.json
  def destroy
    @bill.destroy
    respond_to do |format|
      format.html { redirect_to bills_path(@bill.production_order) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bill
      @bill = Bill.find(params[:id])
    end

    def set_production_order
      @production_order = ProductionOrder.find(params[:production_order_id]) if params[:production_order_id].present?
    end

    def configure_list
      @clients = Client.all
      @quotes = Quote.all
      @production_orders = ProductionOrder.all
      @products = Product.all

    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bill_params
      params.require(:bill).permit(:bill_number,:date_bill, :observations, :administrador_id, :production_order_id,
        bill_details_attributes: [ :id, :product_id, :quantity, :price, :description, :_destroy]) 
    end
end
