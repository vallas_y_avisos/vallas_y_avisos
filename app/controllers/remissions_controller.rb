class RemissionsController < ApplicationController
  before_action :set_remission, only: [:show, :edit, :update, :destroy, :sent]
  before_action :set_production_order
  before_action :configure_list

  # GET /remissions
  # GET /remissions.json
  def index
    if @production_order.present?
      @remissions = @production_order.remissions
    else
      @remissions = Remission.all
    end
  end

  # GET /remissions/1
  # GET /remissions/1.json
  def show
  end

  # GET /remissions/new
  def new
    @remission = Remission.new
    @remission.date_remission = Time.zone.now.strftime '%d/%m/%Y'

    if @production_order.present?
      @products = Product.products_of_quote(@production_order.quote)

      
      @remission.client_name = @production_order.try(:quote).try(:client).try(:name)
      @remission.client_adress = @production_order.try(:quote).try(:client).try(:address)
      @remission.client_phone = @production_order.try(:quote).try(:client).try(:phone)
      @remission.reference = @production_order.try(:quote).try(:reference)
      @remission.production_order = @production_order

      @production_order.quote.quote_details.each do |qd|
        remission_detail = @remission.remission_details.build
        remission_detail.quantity = qd.quantity
        remission_detail.remission_id = @remission
        remission_detail.product_id = qd.product_id
      end    
    else
      @products = Product.all
    end   
  end

  # GET /remissions/1/edit
  def edit
    @products = @products = Product.where("id IN (?)", @remission.remission_details.map(&:product_id)) + Product.products_of_quote(@remission.production_order.quote)
    @remission.client_name = @remission.production_order.try(:quote).try(:client).try(:name)
    @remission.client_adress = @remission.production_order.try(:quote).try(:client).try(:address)
    @remission.client_phone = @remission.production_order.try(:quote).try(:client).try(:phone)
    @remission.reference = @remission.production_order.try(:quote).try(:reference)
  end

  # GET /work_orders/search
  def search
    @remissions = Remission.search_by_query(params[:search])
    @production_order = ProductionOrder.find(params[:search][:production_order_id]) if params[:search][:production_order_id].present?
   
    render :index
  end

  #GET /production_orders/:id/review
  def sent
    if @remission.elaborated?
      if @remission.sent_status!
        flash[:notice] = "La Remisión ha cambiado el estatus a Entregada."
        redirect_to remissions_path 
      else
       flash[:warning] = "La Remisión no pudo ser entregada."
       redirect_to remissions_path 
      end
    else
      if @production_order.present?
        @remissions = @production_order.remissions
      else
        @remissions = Remission.all
      end
      render action: 'index' 
    end
  end

  # POST /remissions
  # POST /remissions.json
  def create
    @remission = Remission.new(remission_params)
    @remission.client_name = @production_order.try(:quote).try(:client).try(:name)
    @remission.client_adress = @production_order.try(:quote).try(:client).try(:address)
    @remission.client_phone = @production_order.try(:quote).try(:client).try(:phone)
    @remission.reference = @production_order.try(:quote).try(:reference)
    
    @remission.remission_details.each do |r|
      r.production_order_id = @production_order.try(:id)
    end
    respond_to do |format|
      if @remission.save
        format.html { redirect_to remissions_path(@remission.production_order), notice: "La Remisión fué creada satisfactoriamente con el número #{@remission.number_remission}." }
        format.json { render action: 'show', status: :created, location: @remission }
      else
        format.html { render action: 'new' }
        format.json { render json: @remission.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /remissions/1
  # PATCH/PUT /remissions/1.json
  def update
    @remission.client_name = @remission.production_order.try(:quote).try(:client).try(:name)
    @remission.client_adress = @remission.production_order.try(:quote).try(:client).try(:address)
    @remission.client_phone = @remission.production_order.try(:quote).try(:client).try(:phone)
    @remission.reference = @remission.production_order.try(:quote).try(:reference)
    @remission.remission_details.each do |r|
      r.production_order_id = @remission.production_order.try(:id)
    end
    respond_to do |format|
      if @remission.update(remission_params)
        format.html { redirect_to remissions_path(@production_order), notice: 'La Remisión fué actualizada satisfactoriamente.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @remission.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /remissions/1
  # DELETE /remissions/1.json
  def destroy
    @remission.destroy
    respond_to do |format|
      format.html { redirect_to remissions_path(@remission.production_order) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_remission
      @remission = Remission.find(params[:id])
    end

    def set_production_order
      @production_order = ProductionOrder.find(params[:production_order_id]) if params[:production_order_id].present?
    end

    def configure_list
      @clients = Client.all
      @quotes = Quote.all
      @production_orders = ProductionOrder.all

    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def remission_params
      params.require(:remission).permit(:number_remission,:date_remission, :observations, :delivery, :administrador_id, :production_order_id,
        remission_details_attributes: [ :id, :product_id, :quantity, :description, :_destroy]) 
    end
end
