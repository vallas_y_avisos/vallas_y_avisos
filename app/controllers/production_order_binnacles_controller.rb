class ProductionOrderBinnaclesController < ApplicationController
	before_action :set_production_order
	before_action :set_comment, only: [:show, :edit, :update, :destroy]
	def index
	end

	def new
		@production_order_binnacle = ProductionOrderBinnacle.new
    @production_order_binnacle.production_order = @production_order 
    @production_order_binnacle.created_at = Time.zone.now
    @production_order_binnacle.quote_id = @production_order.quote_id.to_s.rjust(5, '0')
    @production_order_binnacle.num_production_order = @production_order.try(:number)
    @production_order_binnacle.agency_name = @production_order.quote.client.try(:name)
    @production_order_binnacle.adviser_name = @production_order.quote.adviser.try(:name)
    @production_order_binnacle.reference = @production_order.quote.reference
    @production_order_binnacle.production_order_id = @production_order.try(:id)
    @production_order_binnacle.production_order_binnacle_anexes.build
    @production_order_binnacle.administrador_name=current_user.name
    
	end

	def edit
    @production_order_binnacle.quote_id = @production_order.quote.number
    @production_order_binnacle.num_production_order = @production_order.try(:number)
    @production_order_binnacle.agency_name = @production_order.quote.client.try(:name)
    @production_order_binnacle.adviser_name = @production_order.quote.adviser.try(:name)
    @production_order_binnacle.reference = @production_order.quote.reference
    @production_order_binnacle.production_order_id = @production_order.try(:id)
    @production_order_binnacle.administrador_name=@production_order_binnacle.administrador.try(:nombre) || current_user.name
	end

	def update
    respond_to do |format|
      if @production_order_binnacle.update(binnacle_order_params)
        format.html { redirect_to production_order_production_order_binnacles_path, notice: 'Seguimiento Actualizado.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
	end

	def create
		@production_order_binnacle = ProductionOrderBinnacle.new(binnacle_order_params)
    @production_order_binnacle.production_order =  @production_order
    @production_order_binnacle.title = 'Seguimiento de la Orden de Producción'
    @production_order_binnacle.administrador = current_user
    
    respond_to do |format|
      if @production_order_binnacle.save
        #format.html { redirect_to @comment, notice: 'Comment was successfully created.' }
        format.html { redirect_to production_order_path(@production_order), notice: 'El Seguimiento de la Orden de Producción se guardó satisfactoriamente.' }
        format.json { render action: 'show', status: :created, location: @comment }
      else
        format.html { render action: 'new' }
      end
    end
	end

	def destroy
    if @production_order_binnacle.destroy
      redirect_to  production_order_production_order_binnacles_path, notice: 'La Bitácora Fue eliminada satisfactoriamente.' 
    else  
      redirect_to  production_order_production_order_binnacles_path, notice: 'La Bitácora No Fue eliminada satisfactoriamente.' 
    end
  end

	def set_comment
		@production_order_binnacle = ProductionOrderBinnacle.find(params[:id])
	end

	def set_production_order
      @production_order = ProductionOrder.find(params[:production_order_id])
  end

   # Never trust parameters from the scary internet, only allow the white list through.
  def binnacle_order_params
    params.require(:production_order_binnacle).permit(:title,:description, :administrador_id,  :production_order_id, :updated_at, :num_production_order, :quote_id, :agency_name, :adviser_name, :reference,
      production_order_binnacle_anexes_attributes: [ :id, :type_attachments_id, :anexo, :_destroy] )
  end
end


