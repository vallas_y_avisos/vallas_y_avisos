class ProductOrdersMaterialesController < ApplicationController
  before_action :set_product_orders_material, only: [:show, :edit, :update, :destroy]
  before_action :configure_list
  # GET /product_orders_materiales
  # GET /product_orders_materiales.json
  def index

    #@product_orders_materiales = ProductOrdersMaterial.select(:production_order_id, :created_at).distinct
    @product_orders_materiales = ProductionOrder.where("production_orders.id in (select production_order_id from production_orders_materiales )")
     
  end

  # GET /product_orders_materiales/1
  # GET /product_orders_materiales/1.json
  def show
  end

  # GET /product_orders_materiales/new
  def new
    @product_orders_material = ProductOrdersMaterial.new
  end

  # GET /product_orders_materiales/1/edit
  def edit
    @product_orders_material_list = ProductOrdersMaterial.where(production_order_id: params[:id])
    #byebug
  end


  # POST /product_orders_materiales
  # POST /product_orders_materiales.json
  def create  

    parametros = product_orders_material_params["material"]
    
    parametros.each do |key, value|
      @product_orders_material = ProductOrdersMaterial.new()
      @product_orders_material.quantity = value["quantity"]
      @product_orders_material.production_order_id = value["production_order_id"]
      @product_orders_material.materials_unit = value["materials_unit"]
      @product_orders_material.description = value["description"]
      @product_orders_material.observation = value["observation"]
      @product_orders_material.materials_name = value["materials_name"]        
      @product_orders_material.save
      #byebug
    end

    redirect_to product_orders_materiales_url

  end

  # PATCH/PUT /product_orders_materiales/1
  # PATCH/PUT /product_orders_materiales/1.json
  def update
    parametros = product_orders_material_params["material"]
    
    parametros.each do |key, value|
      if(value["id"])
        @product_orders_material.quantity = value["quantity"]
        @product_orders_material.production_order_id = value["production_order_id"]
        @product_orders_material.materials_unit = value["materials_unit"]
        @product_orders_material.description = value["description"]
        @product_orders_material.observation = value["observation"]
        @product_orders_material.materials_name = value["materials_name"]        
        @product_orders_material.update
      else
        @product_orders_material = ProductOrdersMaterial.new()
        @product_orders_material.quantity = value["quantity"]
        @product_orders_material.production_order_id = value["production_order_id"]
        @product_orders_material.materials_unit = value["materials_unit"]
        @product_orders_material.description = value["description"]
        @product_orders_material.observation = value["observation"]
        @product_orders_material.materials_name = value["materials_name"]        
        @product_orders_material.save
      end
      

      #byebug
    end

    redirect_to product_orders_materiales_url
  end

  # DELETE /product_orders_materiales/1
  # DELETE /product_orders_materiales/1.json
  def destroy
    @product_orders_material.destroy
    respond_to do |format|
      format.html { redirect_to product_orders_materiales_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_orders_material             
       @product_orders_material = ProductionOrder.find(params[:id])
      #byebug
    end

    def configure_list
      @clients = Client.all       
      @production_orders = ProductionOrder.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_orders_material_params
     params.require(:product_orders_material).permit(material: [:production_order_id, :quantity, :materials_unit, :description, :observation, :materials_name, :id])
    end
end
