class ProductionOrdersController < ApplicationController
  before_action :set_production_order, only: [:billed, :billing, :review, :sent_to_cost, :sent_to_production, :ended, :ended_inventory, :ended_warehouse, :ended_machine, :sent_to_cost_by_price_adjustment, :details, :show, :edit, :show, :update, :destroy]
  before_action :configure_list

  # GET /production_orders
  # GET /production_orders.json
  def index
    if current_user.is?(:coordinador_produccion)
      @production_orders = ProductionOrder.where("administrador_id = ? ", current_user.id)
    else
      if current_user.is?(:vendedor)
        @production_orders = ProductionOrder.joins(quote: {adviser: :administrador}).where("administradores.id = ?",current_user.id)
      elsif current_user.is?(:analista)
        @production_orders = ProductionOrder.joins(:quote).where("quotes.coordinator_id = ?",current_user.id)
      elsif current_user.is?(:costo)
        @production_orders = ProductionOrder.all
      else
        @production_orders = ProductionOrder.where("administrador_id is not null")
      end
    end
    @type = "active"
  end

  # GET /production_orders/new
  def new
    @production_order = ProductionOrder.new
    @production_order.time_order = Time.zone.now.strftime("%H:%M:%S")
    @production_order.date_elaboration = Time.zone.now
    # @production_order.actual_delivey_date = Time.zone.now
    @production_order.production_order_anexes.build 
    if params[:quote_id].present?
      @quote = Quote.find params[:quote_id]
      @production_order.number = @quote.number
      @production_order.comission = @quote.try(:commission_quote)
      @production_order.unit_value = @quote.valor_total
      @production_order.client_name = @quote.client.try(:name)
      @production_order.nit_client = @quote.client.try(:nit)
      @production_order.phone_client = @quote.client.try(:phone)
      @production_order.address_client = @quote.client.try(:address)
      @production_order.adviser_name = @quote.adviser.try(:name)
      @production_order.reference = @quote.try(:reference)
      @production_order.way_pay = @quote.try(:method_payment)
      @production_order.quote_id = params[:quote_id]     
      @production_order.total_amount = @quote.valor_total
      
    end     
  end

  def new_orders
    @production_orders = ProductionOrder.where("administrador_id is null").where(status_order: "sent_to_production")
    @type = "new"
    render :index
  end

  def quotes_lists
    @quotes = Quote.where(status: 'approved').where.not(id: ProductionOrder.select(:quote_id)).order(id: :desc) 
  end

  # GET /production_orders/1/edit
  def edit
    @production_order.production_order_anexes.build 
    @production_order.client_name = @quote.try(:client).try(:name)
    @production_order.nit_client = @quote.try(:client).try(:nit)
    @production_order.comission = @quote.try(:commission_quote)
    @production_order.phone_client = @quote.try(:client).try(:phone)
    @production_order.address_client = @quote.try(:client).try(:address)
    @production_order.adviser_name = @quote.try(:adviser).try(:name)
    @production_order.reference = @quote.try(:reference)
    @production_order.way_pay = @quote.try(:method_payment)
    @type = "active"
  end

  # GET /production_orders/1/edit
  def show
    @production_order.client_name = @quote.try(:client).try(:name)
    @production_order.nit_client = @quote.try(:client).try(:nit)
    @production_order.phone_client = @quote.try(:client).try(:phone)
    @production_order.address_client = @quote.try(:client).try(:address)
    @production_order.adviser_name = @quote.try(:adviser).try(:name)
    @production_order.reference = @quote.try(:reference)
    @production_order.way_pay = @quote.try(:method_payment)

    respond_to do |format|
      format.html { render action: 'show' }
      format.pdf { render pdf: "orden_produccion_#{params[:id]}" }
    end

  end

  # GET /production_orders/search
  def search
    @production_orders = ProductionOrder.search_by_query(params[:search])
    @type = params[:type]
    if @type == 'active'
      @production_orders = @production_orders.where("production_orders.administrador_id is not null")
    else
      @production_orders = @production_orders.where("production_orders.administrador_id is null")
    end
    render :index
  end

  #GET /production_orders/:id/review
  def review
    if @production_order.elaborated?
      if @production_order.check!
        HistoryOrderProductionStatus.create :status =>'review', :production_order_id =>@production_order.id, :administrador_id =>current_user.id
        flash[:notice] = "La Orden fue enviada a Revisión."
        redirect_to production_orders_path 
      else
       flash[:warning] = "La Orden no pudo ser enviada a Revisión."
       redirect_to production_orders_path 
      end
    else
      @production_orders = ProductionOrder.all
      render action: 'index' 
    end
  end

  #GET sent_to_cost_production_order
  def sent_to_cost
    if @production_order.elaborated?
      if @production_order.sent_to_cost!
        HistoryOrderProductionStatus.create :status =>'sent_to_cost', :production_order_id =>@production_order.id, :administrador_id =>current_user.id
        flash[:notice] = "La Orden fue enviada a Costos."
        redirect_to production_orders_path
      else
       flash[:warning] = "La Orden no pudo ser enviada a Costos."
       redirect_to production_orders_path 
      end
    else
      @production_orders = ProductionOrder.all
      render action: 'index' 
    end
  end

   #GET sent_to_production_production_order
  def sent_to_production
    if @production_order.sent_to_cost?
      if @production_order.to_production!
        HistoryOrderProductionStatus.create :status =>'sent_to_production', :production_order_id =>@production_order.id, :administrador_id =>current_user.id
        flash[:notice] = "La Orden fue enviada a produccion."
        redirect_to production_orders_path 
      else
       flash[:warning] = "La Orden no pudo ser enviada a produccion."
       redirect_to production_orders_path 
      end
    else
      @production_orders = ProductionOrder.all
      render action: 'index' 
    end
  end

  #GET end_to_cost_production_order
  def ended
    if @production_order.sent_to_production?
      if @production_order.ended!
        HistoryOrderProductionStatus.create :status =>'end', :production_order_id =>@production_order.id, :administrador_id =>current_user.id
        flash[:notice] = "La Orden fue culminada."
        redirect_to production_orders_path 
      else
       flash[:warning] = "La Orden no pudo ser culminada."
       redirect_to production_orders_path 
      end
    else
      @production_orders = ProductionOrder.all
      render action: 'index' 
    end
  end

  #GET ended_inventory_to_cost_production_order
  def ended_inventory
    if @production_order.end?
      if @production_order.can_inventory_end?
        @production_order.inventory_closed = true
        @production_order.save
        flash[:notice] = "La Orden fue culminada."
        redirect_to production_orders_path 
      else
       flash[:warning] = "La Orden no pudo ser culminada."
       redirect_to production_orders_path 
      end
    else
      flash[:warning] = "La Orden no pudo ser culminada."
      @production_orders = ProductionOrder.all
      render action: 'index' 
    end
  end

  #GET ended_machine_to_cost_production_order
  def ended_machine
    if @production_order.end?
      if @production_order.can_machine_end?
        @production_order.machine_closed = true
        @production_order.save
        flash[:notice] = "La Orden fue culminada."
        redirect_to production_orders_path 
      else
       flash[:warning] = "La Orden no pudo ser culminada."
       redirect_to production_orders_path 
      end
    else
      flash[:warning] = "La Orden no pudo ser culminada."
      @production_orders = ProductionOrder.all
      render action: 'index' 
    end
  end

  #GET ended_warehouse_to_cost_production_order
  def ended_warehouse
    if @production_order.end?
      if @production_order.can_warehouse_end?
        @production_order.warehouse_closed = true
        @production_order.save
        flash[:notice] = "La Orden fue culminada."
        redirect_to production_orders_path 
      else
       flash[:warning] = "La Orden no pudo ser culminada."
       redirect_to production_orders_path 
      end
    else
      flash[:warning] = "La Orden no pudo ser culminada."
      @production_orders = ProductionOrder.all
      render action: 'index' 
    end
  end

  def sent_to_cost_by_price_adjustment
    if @production_order.sent_to_production?
      if @production_order.price_adjustment!
        HistoryOrderProductionStatus.create :status =>'sent_to_production', :production_order_id =>@production_order.id, :administrador_id =>current_user.id
        flash[:notice] = "La Orden fue enviada a Costos."
        redirect_to production_orders_path 
      else
       flash[:warning] = "La Orden no pudo ser enviada a Costos."
       redirect_to production_orders_path 
      end
    else
      @production_orders = ProductionOrder.all
      render action: 'index' 
    end
  end

  def billing
    if @production_order.end?
      if @production_order.billing!
        HistoryOrderProductionStatus.create :status =>'sent_to_billing', :production_order_id =>@production_order.id, :administrador_id =>current_user.id
        flash[:notice] = "La Orden fue enviada a Facturación."
        redirect_to production_orders_path 
      else
       flash[:warning] = "La Orden no pudo ser enviada a Facturación."
       redirect_to production_orders_path 
      end
    else
      @production_orders = ProductionOrder.all
      render action: 'index' 
    end
  end

  def billed
    if @production_order.sent_to_billing?
      if @production_order.billed!
        HistoryOrderProductionStatus.create :status =>'billed', :production_order_id =>@production_order.id, :administrador_id =>current_user.id
        flash[:notice] = "La Orden fue Facturada."
        redirect_to production_orders_path 
      else
       flash[:warning] = "La Orden no pudo ser facturada."
       redirect_to production_orders_path 
      end
    else
      @production_orders = ProductionOrder.all
      render action: 'index' 
    end
  end


  def details
    @quote = @production_order.quote
    @quote_detail = {"id" => @quote.try(:id), 
                      "quote_number" => @quote.try(:number), 
                      "agency_name" => @quote.try(:agency).try(:name), 
                      "client_name" => @quote.try(:client).try(:name), 
                      "client_phone" => @quote.try(:client).try(:phone), 
                      "adviser_name" => @quote.try(:adviser).try(:name), 
                      "quote_commission" => @quote.try(:commission_quote), 
                      "client_nit" => @quote.try(:client).try(:nit), 
                      "client_address" => @quote.try(:client).try(:address), 
                      "quote_reference" => @quote.try(:reference),
                      "production_order_number" => @production_order.try(:number),
                      "production_order_status" => @production_order.order_status,
                      "production_order_id" => @production_order.id}  
    
    respond_to do |format|  ## Add this
      format.json { render json: @quote_detail , status: :ok}
    end 
  end

  # POST /production_orders
  # POST /production_orders.json
  def create
    @quote = Quote.find params[:production_order][:quote_id] if params[:production_order][:quote_id].present?
    @production_order = ProductionOrder.new(production_order_params)
    @production_order.quote = @quote

    respond_to do |format|
      @production_order.validates_quantity = true
      if @production_order.save
        if @production_order.update(production_order_params_only_details)
          StatusQuote.create :status =>'Orden-Produccion', :quote_id =>@production_order.quote_id, :administrador_id =>current_user.id
          HistoryOrderProductionStatus.create :status =>'elaborated', :production_order_id =>@production_order.id, :administrador_id =>current_user.id
          Alert.create :type_alert =>"Orden-Produccion" , :description =>"Se Creo Orden De Produccion N# #{@production_order.number}", :model => 'quote', :model_id => @production_order.quote_id , :days => 0 
          format.html { redirect_to production_orders_path, notice: 'La Orden Fue creada satisfactoriamente.' }
          format.json { render action: 'show', status: :created, location: @production_order }
        else
          format.html { render action: 'new' }
          format.json { render json: @production_order.errors, status: :unprocessable_entity }          
        end
      else
        format.html { render action: 'new' }
        format.json { render json: @production_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /production_orders/1
  # PATCH/PUT /production_orders/1.json
  def update
    @quote = @production_order.quote
    @production_order.administrador_id_before = @production_order.administrador_id
    respond_to do |format|
      if current_user.is?(:costo)
        prms = production_order_params_by_costo
      elsif current_user.is?(:inventario) || current_user.is?(:almacen) || current_user.is?(:maquina)
        prms = production_order_params_to_departaments
      else
        prms = production_order_params
      end

      @production_order.validates_quantity = true
      if @production_order.update(prms)
        format.html { redirect_to production_orders_path, notice: 'La Orden Fue actualizada satisfactoriamente.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @production_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /production_orders/1
  # DELETE /production_orders/1.json
  def destroy
    @production_order.destroy
    respond_to do |format|
      format.html { redirect_to production_orders_path, notice: 'La Orden Fue eliminda satisfactoriamente.' }
      format.json { head :no_content }
    end
  end

  def list
    @production_order_client = ProductionOrder.joins(:quote).where("client_id = ?", params[:production_order_id])
    #byebug
    render :json => @production_order_client
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_production_order
      @production_order = ProductionOrder.find(params[:id])     
      @quote = @production_order.quote if @production_order.present?
    end

    def configure_list
      @advisers = Adviser.all
      @clients = Client.all
      @quotes = Quote.all
      @orders = ProductionOrder.all
      @coordinadores =  Administrador.where(rol:'coordinador_produccion')

    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def production_order_params
      params.require(:production_order).permit(:difficulty_quote_id, :reason, :another_reason, :final_delivery_date, :number,:date_elaboration, :delivery_date_printing, :actual_delivey_date, :administrador_id, :agency_percentage, :number_photos, :spreadsheet,
      :request_to, :advance, :contract_number, :way_pay, :sale_cc, :lease_cc, :service_cc, :code_fence, :sheet, :angle, :type_order, :lighting, :quantity,
      :unit_value, :site_installation, :address, :ask_for, :phone, :specifications, :addition, :reference, :cost_observations, :production_observations, :overcost,
      :sketch, :contractor, :quote_id, :quote_number, :comission, :remission, :time_order, :nit_client, :phone_client, :address_client, :client_name, :commission_quote, :adviser_name,
      production_order_anexes_attributes: [ :id, :anexo, :_destroy],
      production_order_comments_attributes: [ :id, :comment, :administrador_id, :_destroy],
      quote_details_attributes: [:id, :op_quantity, :_destroy])
    end
    def production_order_params_without_details
      params.require(:production_order).permit(:reason, :another_reason, :final_delivery_date, :number,:date_elaboration, :delivery_date_printing, :actual_delivey_date, :administrador_id, :agency_percentage, :number_photos, :spreadsheet,
      :request_to, :advance, :contract_number, :way_pay, :sale_cc, :lease_cc, :service_cc, :code_fence, :sheet, :angle, :type_order, :lighting, :quantity,
      :unit_value, :site_installation, :address, :ask_for, :phone, :specifications, :addition, :reference, :cost_observations, :production_observations, :overcost,
      :sketch, :contractor, :quote_id, :quote_number, :comission, :remission, :time_order, :nit_client, :phone_client, :address_client, :client_name, :commission_quote, :adviser_name,
      production_order_comments_attributes: [ :id, :comment, :administrador_id, :_destroy],production_order_anexes_attributes: [ :id, :anexo, :_destroy])
    end

    def production_order_params_only_details
      params.require(:production_order).permit(production_order_comments_attributes: [ :id, :comment, :administrador_id, :_destroy], quote_details_attributes: [:id, :op_quantity, :_destroy])
    end

    def production_order_params_by_costo
      params.require(:production_order).permit(:sale_cc, :lease_cc, :service_cc, :spreadsheet, production_order_comments_attributes: [ :id, :comment, :administrador_id, :_destroy], production_order_anexes_attributes: [ :id, :anexo, :_destroy], quote_details_attributes: [:id, :op_quantity, :price, :_destroy])
    end

    def production_order_params_to_departaments
      params.require(:production_order).permit(
          :inventory_elements_out, :inventory_elements_out_comment, :inventory_elements_in, :inventory_elements_in_comment, :inventory_material_destruction, :inventory_material_destruction_comment, :inventory_material_destruction_attachment,
          :warehouse_aditional_material, :warehouse_aditional_material_comment, :warehouse_material_changed, :warehouse_material_changed_comment,
          :machine_aditional_material, :machine_aditional_material_comment, :machine_material_changed, :machine_material_changed_comment, :machine_machine_changed, :machine_machine_changed_comment, :machine_art, :machine_art_comment
          )
    end

end
