class WorkOrdersController < ApplicationController
  before_action :set_work_order, only: [:show, :edit, :update, :destroy, :validate, :deliver]
  before_action :configure_list

  # GET /work_orders
  # GET /work_orders.json
  def index
    if params[:production_order_id].present?
      @production_order = ProductionOrder.find params[:production_order_id]
      @work_orders = @production_order.work_orders
    else
      @work_orders = WorkOrder.all
    end
    
  end

  # GET /work_orders/1
  # GET /work_orders/1.json
  def show
    #@production_order.client_name = @quote.try(:client).try(:name)
    #@production_order.nit_client = @quote.try(:client).try(:nit)
    #@production_order.phone_client = @quote.try(:client).try(:phone)
    #@production_order.address_client = @quote.try(:client).try(:address)
    #@production_order.adviser_name = @quote.try(:adviser).try(:name)
    #@production_order.reference = @quote.try(:reference)
    #@production_order.way_pay = @quote.try(:method_payment)

    respond_to do |format|
      format.html { render action: 'show' }
      format.pdf { render pdf: "orden_trabajo_#{params[:id]}" }
    end
  end

  # GET /work_orders/new
  def new
    @work_order = WorkOrder.new
    if params[:production_order_id].present?
      @production_order = ProductionOrder.find params[:production_order_id]
      @quote = @production_order.quote
      @work_order.client_name = @quote.client.try(:name)
      @work_order.reference = @quote.try(:reference)
      @work_order.quote_number = @quote.try(:number)
      @work_order.production_order_number = @production_order.try(:number)
      @work_order.production_order_id = params[:production_order_id]
      @work_order.address = @production_order.address

    end
  end

  # GET /work_orders/search
  def search
    @work_orders = WorkOrder.search_by_query(params[:search])
    render :index
  end

  # GET /work_orders/1/edit
  def edit
      @production_order = @work_order.production_order
      @quote = @production_order.try(:quote)
      @work_order.client_name = @quote.try(:client).try(:name)
      @work_order.reference = @quote.try(:reference)
      @work_order.quote_number = @quote.try(:number)
      @work_order.production_order_number = @production_order.try(:number)  
  end

  # POST /work_orders
  # POST /work_orders.json
  def create
    @work_order = WorkOrder.new(work_order_params)
    @work_order.administrador = current_user

    respond_to do |format|
      if @work_order.save
        format.html { redirect_to work_orders_path, notice: 'La Orden Fue creada satisfactoriamente.' }
        format.json { render action: 'show', status: :created, location: @work_order }
      else
        format.html { render action: 'new' }
        format.json { render json: @work_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /work_orders/1
  # PATCH/PUT /work_orders/1.json
  def update
    respond_to do |format|
      if @work_order.update(work_order_params)
        format.html { redirect_to work_orders_path, notice: 'La Orden Fue actualizada satisfactoriamente.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @work_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /work_orders/1
  # DELETE /work_orders/1.json
  def destroy
    @work_order.destroy
    respond_to do |format|
      format.html { redirect_to work_orders_path, notice: 'La Orden Fue eliminada satisfactoriamente.' }
      format.json { head :no_content }
    end
  end

   #GET deliver_work_order
  def deliver
    if @work_order.elaborated?
      if @work_order.deliver!
        flash[:notice] = "La Orden fue entregada."
        redirect_to work_orders_path 
      else
       flash[:warning] = "La Orden no pudo ser entregada."
       redirect_to work_orders_path
      end
    else
      render action: 'edit' 
    end
  end

   #GET deliver_work_order
  def validate 
    if @work_order.delivered?
      if @work_order.validate!
        flash[:notice] = "La Orden fue validada."
        redirect_to work_orders_path 
      else
       flash[:warning] = "La Orden no pudo ser validada."
       redirect_to work_orders_path
      end
    else
      render action: 'edit' 
    end
  end


  def configure_list
      @agencies = Agency.all
      @advisers = Adviser.all
      @clients = Client.all
      @quotes = Quote.all
      @products = Product.all
      @production_orders = ProductionOrder.all
    end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_work_order
      @work_order = WorkOrder.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def work_order_params
      params.require(:work_order).permit(:id, :application_date, :execution_date, :production_order_id, :cost_center, :contractor, :name, :overcost, :address, work_order_details_attributes: [ :id, :product_id, :quantity, :price, :description, :_destroy], work_order_comments_attributes: [ :id, :comment, :administrador_id, :_destroy])
    end
end
