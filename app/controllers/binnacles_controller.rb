class BinnaclesController < ApplicationController
	def index
    @binnacles = ProductionOrderBinnacle.all.order(:production_order_id)
	end

	def new
		@production_order_binnacle = ProductionOrderBinnacle.new
    @production_order_binnacle.created_at = Time.zone.now
    @production_order_binnacle.production_order_binnacle_anexes.build
    @production_order_binnacle.administrador_name=current_user.name
    
	end

	
	def create
		@production_order_binnacle = ProductionOrderBinnacle.new(binnacle_order_params)
    @production_order_binnacle.title = 'Seguimiento de la Orden de Producción'
    @production_order_binnacle.administrador = current_user
    
    respond_to do |format|
      if @production_order_binnacle.save
        #format.html { redirect_to @comment, notice: 'Comment was successfully created.' }
        format.html { redirect_to binnacles_path, notice: 'El Seguimiento de la Orden de Producción se guardó satisfactoriamente.' }
      else
        format.html { render action: 'new' }
      end
    end
	end


   # Never trust parameters from the scary internet, only allow the white list through.
  def binnacle_order_params
    params.require(:production_order_binnacle).permit(:title,:description, :administrador_id,  :production_order_id, :updated_at, :num_production_order, :quote_id, :agency_name, :adviser_name, :reference,
      production_order_binnacle_anexes_attributes: [ :id, :type_attachments_id, :anexo, :_destroy] )
  end
end


