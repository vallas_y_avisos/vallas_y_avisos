class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]
  before_action :configure_list
  # GET /comments
  # GET /comments.json
  def index
    @comments = Comment.all
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
  end

  # GET /comments/new
  def new
    @comment = Comment.new
    if params[:quote_id].present?
      @comment.quote_id = params[:quote_id]
      quote = Quote.find(params[:quote_id])
      @comment.status = quote.quote_status
      @comment.date = quote.created_at.strftime('%d/%m/%Y')
      @comment.time = quote.created_at.strftime('%H:%m')   
      @comment.client_name = quote.client.name
      @comment.nit_client = quote.client.nit
      @comment.phone_client = quote.client.phone
      @comment.address_client = quote.client.address
      @comment.adviser_name = quote.adviser.name
      @comment.reference = quote.reference
      @comment.comission = quote.commission_quote
    end
  end

  # GET /comments/1/edit
  def edit  

    @comment.quote_id = @comment.commentable_id.to_s.rjust(6, '0') 
    @comment.id = @comment.id.to_s.rjust(6, '0') 
    @comment.comment = @comment.comment 
    @comment.fecha = @comment.created_at.strftime('%d/%m/%Y')
    @comment.hora = @comment.created_at.strftime('%H:%m')   
    @comment.reference = @quote_detail['quote_reference']
    @comment.adviser_name = @quote_detail['adviser_name']
    @comment.address_client = @quote_detail['client_address']
    @comment.phone_client = @quote_detail['client_phone']
    @comment.nit_client = @quote_detail['client_nit']
    @comment.client_name = @quote_detail['client_name']
    @comment.comission = @quote_detail['commission_quote']
    @comment.agency_name = @quote_detail['agency_name']
    @comment.agency_name = @quote_detail['agency_name']
    @comment.date = @quote_detail['fecha'].strftime('%d/%m/%Y')
    @comment.time = @quote_detail['fecha'].strftime('%H:%m')
    @comment.quote_status_edit = @quote_detail['status']
                   
  end

  # POST /comments
  # POST /comments.json
  def create
    @comment = Comment.new(comment_params)
    @comment.commentable_id = params[:comment][:quote_id]
    @comment.commentable_type = 'Quote'
    @comment.title = 'Seguimiento de Cotizacion'
    @comment.administrador = current_user
    cotizacion = @comment.commentable_id 
    
    respond_to do |format|
      if @comment.save
        #format.html { redirect_to @comment, notice: 'Comment was successfully created.' }
        format.html { redirect_to edit_quote_path(cotizacion), notice: 'El Seguimiento de la cotizacion  se grabo satisfactoriamente.' }
        format.json { render action: 'show', status: :created, location: @comment }
      else
        format.html { render action: 'new' }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to comments_path, notice: 'Seguimiento Actualizado.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to comments_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
      @quote = Quote.find(@comment.commentable_id)
      @quote_detail = {"id" => @quote.id, 
                  "agency_name" => @quote.agency.try(:name), 
                  "date" => @quote.try(:created_at),
                  "client_name" => @quote.client.try(:name), 
                  "client_phone" => @quote.client.try(:phone), 
                  "adviser_name" => @quote.adviser.try(:name), 
                  "adviser_assessor_code" => @quote.adviser.try(:assessor_code),                       
                  "adviser_agency_id" => @quote.adviser.try(:agency_id),                       
                  "commission_quote" => @quote.try(:commission_quote), 
                  "client_nit" => @quote.client.try(:nit), 
                  "client_address" => @quote.client.try(:address), 
                  "status" => @quote.try(:status), 
                  "fecha" => @quote.try(:created_at), 
                  "quote_reference" => @quote.try(:reference)}  
      #byebug                  
    
    end


    def configure_list
       @quotes = Quote.where("status in ('delivered_customer', 'approved', 'rejected', 'cost_report')").order(id: :desc)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:title, :comment, :quote_id)
    end
end
