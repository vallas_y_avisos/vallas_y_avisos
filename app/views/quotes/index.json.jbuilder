json.array!(@quotes) do |quote|
  json.extract! quote, :number, :reference, :status
  json.url quote_url(quote, format: :json)
end
