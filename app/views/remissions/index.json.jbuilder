json.array!(@remissions) do |remission|
  json.extract! remission, 
  json.url remission_url(remission, format: :json)
end
