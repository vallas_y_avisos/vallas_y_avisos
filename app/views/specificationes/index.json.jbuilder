json.array!(@specificationes) do |specification|
  json.extract! specification, :name
  json.url specification_url(specification, format: :json)
end
