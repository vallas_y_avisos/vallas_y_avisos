json.array!(@materiales) do |material|
  json.extract! material, :description, :status
  json.url material_url(material, format: :json)
end
