json.array!(@difficulty_quotes) do |difficulty_quote|
  json.extract! difficulty_quote, :name, :quantity, :lapse, :type_lapse
  json.url difficulty_quote_url(difficulty_quote, format: :json)
end
