json.array!(@products) do |product|
  json.extract! product, :name, :price, :status
  json.url product_url(product, format: :json)
end
