json.array!(@advisers) do |adviser|
  json.extract! adviser, :name, :phone, :address, :type
  json.url adviser_url(adviser, format: :json)
end
