json.array!(@product_orders_materiales) do |product_orders_material|
  json.extract! product_orders_material, :quantity
  json.url product_orders_material_url(product_orders_material, format: :json)
end
