json.array!(@categories) do |categorie|
  json.extract! categorie, :name
  json.url categorie_url(categorie, format: :json)
end
