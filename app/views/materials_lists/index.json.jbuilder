json.array!(@materials_lists) do |materials_list|
  json.extract! materials_list, :op, :date_delivery, :cc, :denomination, :size_comp, :quantity, :length_comp, :width_comp, :date_elaboration, :prepared_by, :fototelones_value, :product_description
  json.url materials_list_url(materials_list, format: :json)
end
