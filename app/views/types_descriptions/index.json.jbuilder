json.array!(@types_descriptions) do |type_description|
  json.extract! type_description, :id, :description
end
