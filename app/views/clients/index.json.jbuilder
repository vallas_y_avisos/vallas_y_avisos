json.array!(@clients) do |client|
  json.extract! client, :name, :nit, :email, :contact, :office, :phone, :address
  json.url client_url(client, format: :json)
end
