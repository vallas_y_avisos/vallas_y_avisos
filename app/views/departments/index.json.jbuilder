json.array!(@departments) do |department|
  json.extract! department, :name, :color
  json.url department_url(department, format: :json)
end
