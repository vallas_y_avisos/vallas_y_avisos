# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170818015652) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "administradores", force: true do |t|
    t.string   "email",                  default: "",       null: false
    t.string   "encrypted_password",     default: "",       null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,        null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "nombre",                 default: "nombre", null: false
    t.integer  "dependencia_id"
    t.integer  "roles_mask"
    t.string   "rol"
    t.integer  "department_id"
    t.boolean  "is_active",              default: true
    t.integer  "adviser_id"
  end

  add_index "administradores", ["adviser_id"], name: "index_administradores_on_adviser_id", using: :btree
  add_index "administradores", ["department_id"], name: "index_administradores_on_department_id", using: :btree
  add_index "administradores", ["dependencia_id"], name: "index_administradores_on_dependencia_id", using: :btree
  add_index "administradores", ["email"], name: "index_administradores_on_email", unique: true, using: :btree
  add_index "administradores", ["reset_password_token"], name: "index_administradores_on_reset_password_token", unique: true, using: :btree

  create_table "advisers", force: true do |t|
    t.string   "name"
    t.string   "phone",         limit: 20
    t.string   "address"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email"
    t.string   "assessor_code"
    t.integer  "client_id"
  end

  add_index "advisers", ["client_id"], name: "index_advisers_on_client_id", using: :btree

  create_table "agencies", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone",      limit: 20
    t.string   "address"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "agencies", ["name"], name: "index_agencies_on_name", using: :btree

  create_table "alert_users", force: true do |t|
    t.boolean  "viewed"
    t.boolean  "sent"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "alert_id"
    t.integer  "administrador_id"
  end

  add_index "alert_users", ["administrador_id"], name: "index_alert_users_on_administrador_id", using: :btree
  add_index "alert_users", ["alert_id"], name: "index_alert_users_on_alert_id", using: :btree

  create_table "alerts", force: true do |t|
    t.string   "type_alert"
    t.string   "description"
    t.string   "model"
    t.integer  "model_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "days"
  end

  create_table "bill_details", force: true do |t|
    t.integer "quantity"
    t.text    "description"
    t.integer "bill_id"
    t.integer "product_id"
    t.float   "price"
  end

  add_index "bill_details", ["bill_id"], name: "index_bill_details_on_bill_id", using: :btree
  add_index "bill_details", ["product_id"], name: "index_bill_details_on_product_id", using: :btree

  create_table "bills", force: true do |t|
    t.string  "bill_number",         limit: 100
    t.date    "date_bill"
    t.text    "observations"
    t.integer "administrador_id"
    t.integer "production_order_id"
  end

  add_index "bills", ["administrador_id"], name: "index_bills_on_administrador_id", using: :btree
  add_index "bills", ["production_order_id"], name: "index_bills_on_production_order_id", using: :btree

  create_table "categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "clients", force: true do |t|
    t.string   "name"
    t.string   "nit",        limit: 30
    t.string   "email"
    t.string   "contact",    limit: 30
    t.string   "office"
    t.string   "phone",      limit: 20
    t.string   "address"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "agency"
    t.string   "code"
    t.string   "position"
    t.string   "city"
  end

  add_index "clients", ["name"], name: "index_clients_on_name", using: :btree

  create_table "comments", force: true do |t|
    t.string   "title",            limit: 50, default: ""
    t.text     "comment"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.string   "role",                        default: "comments"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "administrador_id"
  end

  add_index "comments", ["administrador_id"], name: "index_comments_on_administrador_id", using: :btree
  add_index "comments", ["commentable_id"], name: "index_comments_on_commentable_id", using: :btree
  add_index "comments", ["commentable_type"], name: "index_comments_on_commentable_type", using: :btree

  create_table "departments", force: true do |t|
    t.string   "name"
    t.string   "color"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "departments", ["name"], name: "index_departments_on_name", using: :btree

  create_table "difficulty_quotes", force: true do |t|
    t.string   "name"
    t.integer  "quantity"
    t.string   "lapse"
    t.string   "type_lapse"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_quote",      default: true
    t.boolean  "is_production", default: false
  end

  create_table "history_order_production_statuses", force: true do |t|
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "administrador_id"
    t.integer  "production_order_id"
  end

  add_index "history_order_production_statuses", ["administrador_id"], name: "index_history_order_production_statuses_on_administrador_id", using: :btree
  add_index "history_order_production_statuses", ["production_order_id"], name: "index_history_order_production_statuses_on_production_order_id", using: :btree

  create_table "materiales", force: true do |t|
    t.string   "description"
    t.integer  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "materiales_details", force: true do |t|
    t.text     "materials_name"
    t.float    "quantity"
    t.string   "materials_unit"
    t.text     "description"
    t.text     "observation"
    t.text     "materials_reference"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "materials_list_id"
  end

  add_index "materiales_details", ["materials_list_id"], name: "index_materiales_details_on_materials_list_id", using: :btree

  create_table "materials_lists", force: true do |t|
    t.string   "op"
    t.date     "date_delivery"
    t.string   "cc"
    t.text     "denomination"
    t.string   "size_comp"
    t.integer  "quantity"
    t.float    "length_comp"
    t.float    "width_comp"
    t.date     "date_elaboration"
    t.string   "prepared_by"
    t.string   "fototelones_value"
    t.text     "product_description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "production_orders_id"
    t.integer  "production_order_id"
    t.string   "status"
  end

  add_index "materials_lists", ["production_order_id"], name: "index_materials_lists_on_production_order_id", using: :btree
  add_index "materials_lists", ["production_orders_id"], name: "index_materials_lists_on_production_orders_id", using: :btree

  create_table "product_types", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_id"
    t.integer  "type_id"
  end

  add_index "product_types", ["product_id"], name: "index_product_types_on_product_id", using: :btree
  add_index "product_types", ["type_id"], name: "index_product_types_on_type_id", using: :btree

  create_table "production_order_anexes", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "production_order_id"
    t.string   "anexo_file_name"
    t.string   "anexo_content_type"
    t.integer  "anexo_file_size"
    t.datetime "anexo_updated_at"
  end

  add_index "production_order_anexes", ["production_order_id"], name: "index_production_order_anexes_on_production_order_id", using: :btree

  create_table "production_order_binnacle_anexes", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "production_order_binnacle_id"
    t.string   "anexo_file_name"
    t.string   "anexo_content_type"
    t.integer  "anexo_file_size"
    t.datetime "anexo_updated_at"
  end

  add_index "production_order_binnacle_anexes", ["production_order_binnacle_id"], name: "index_production_order_binnacle_id", using: :btree

  create_table "production_order_binnacles", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title"
    t.text     "description"
    t.integer  "production_order_id"
    t.integer  "administrador_id"
  end

  add_index "production_order_binnacles", ["administrador_id"], name: "index_production_order_binnacles_on_administrador_id", using: :btree
  add_index "production_order_binnacles", ["production_order_id"], name: "index_production_order_binnacles_on_production_order_id", using: :btree

  create_table "production_order_comments", force: true do |t|
    t.text     "comment"
    t.integer  "administrador_id"
    t.integer  "production_order_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "production_order_comments", ["administrador_id"], name: "index_production_order_comments_on_administrador_id", using: :btree
  add_index "production_order_comments", ["production_order_id"], name: "index_production_order_comments_on_production_order_id", using: :btree

  create_table "production_orders", force: true do |t|
    t.string   "number",                                                 limit: 100
    t.datetime "date_elaboration"
    t.datetime "actual_delivey_date"
    t.integer  "number_photos"
    t.string   "sale_cc",                                                limit: 100
    t.string   "lease_cc",                                               limit: 100
    t.string   "service_cc",                                             limit: 100
    t.string   "code_fence",                                             limit: 100
    t.integer  "sheet"
    t.integer  "angle"
    t.boolean  "lighting"
    t.integer  "quantity"
    t.float    "unit_value"
    t.text     "site_installation"
    t.text     "address"
    t.string   "ask_for",                                                limit: 100
    t.string   "phone",                                                  limit: 30
    t.text     "specifications"
    t.text     "addition"
    t.text     "cost_observations"
    t.text     "production_observations"
    t.text     "overcost"
    t.text     "sketch"
    t.string   "contractor",                                             limit: 100
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "remission_id"
    t.integer  "quote_id"
    t.time     "time_order"
    t.string   "status_order"
    t.integer  "administrador_id"
    t.string   "remission"
    t.datetime "final_delivery_date"
    t.datetime "start_date_production"
    t.string   "reason"
    t.string   "another_reason"
    t.boolean  "inventory_closed",                                                   default: false
    t.boolean  "inventory_elements_out"
    t.string   "inventory_elements_out_comment"
    t.boolean  "inventory_elements_in"
    t.string   "inventory_elements_in_comment"
    t.boolean  "inventory_material_destruction"
    t.string   "inventory_material_destruction_attachment_file_name"
    t.string   "inventory_material_destruction_attachment_content_type"
    t.integer  "inventory_material_destruction_attachment_file_size"
    t.datetime "inventory_material_destruction_attachment_updated_at"
    t.string   "inventory_material_destruction_comment"
    t.boolean  "warehouse_closed",                                                   default: false
    t.boolean  "warehouse_aditional_material"
    t.string   "warehouse_aditional_material_comment"
    t.boolean  "warehouse_material_changed"
    t.string   "warehouse_material_changed_comment"
    t.boolean  "machine_closed",                                                     default: false
    t.boolean  "machine_aditional_material"
    t.string   "machine_aditional_material_comment"
    t.boolean  "machine_material_changed"
    t.string   "machine_material_changed_comment"
    t.boolean  "machine_machine_changed"
    t.string   "machine_machine_changed_comment"
    t.boolean  "machine_art"
    t.string   "machine_art_comment"
    t.integer  "difficulty_quote_id"
    t.string   "spreadsheet_file_name"
    t.string   "spreadsheet_content_type"
    t.integer  "spreadsheet_file_size"
    t.datetime "spreadsheet_updated_at"
  end

  add_index "production_orders", ["administrador_id"], name: "index_production_orders_on_administrador_id", using: :btree
  add_index "production_orders", ["difficulty_quote_id"], name: "index_production_orders_on_difficulty_quote_id", using: :btree
  add_index "production_orders", ["quote_id"], name: "index_production_orders_on_quote_id", using: :btree
  add_index "production_orders", ["remission_id"], name: "index_production_orders_on_remission_id", using: :btree

  create_table "production_orders_materiales", force: true do |t|
    t.integer  "quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "production_order_id"
    t.string   "materials_unit"
    t.string   "description"
    t.string   "observation"
    t.string   "materials_name"
  end

  add_index "production_orders_materiales", ["production_order_id"], name: "index_production_orders_materiales_on_production_order_id", using: :btree

  create_table "products", force: true do |t|
    t.string   "name"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "categorie_id"
  end

  add_index "products", ["categorie_id"], name: "index_products_on_categorie_id", using: :btree

  create_table "quote_details", force: true do |t|
    t.integer  "version_number"
    t.integer  "quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "quote_id"
    t.float    "price"
    t.text     "description"
    t.integer  "product_id"
    t.integer  "type_id"
    t.string   "specificationes"
    t.integer  "op_quantity",     default: 0
  end

  add_index "quote_details", ["product_id"], name: "index_quote_details_on_product_id", using: :btree
  add_index "quote_details", ["quote_id"], name: "index_quote_details_on_quote_id", using: :btree
  add_index "quote_details", ["type_id"], name: "index_quote_details_on_type_id", using: :btree

  create_table "quote_details_types", force: true do |t|
    t.integer  "quote_detail_id"
    t.integer  "type_description_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "quote_details_types", ["quote_detail_id"], name: "index_quote_details_types_on_quote_detail_id", using: :btree
  add_index "quote_details_types", ["type_description_id"], name: "index_quote_details_types_on_type_description_id", using: :btree

  create_table "quotes", force: true do |t|
    t.string   "number",                   limit: 100
    t.text     "reference"
    t.float    "commission_quote"
    t.string   "form_delivery",            limit: 100
    t.string   "method_payment",           limit: 100
    t.datetime "date_delivery_costs"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "adviser_id"
    t.integer  "client_id"
    t.integer  "agency_id"
    t.string   "status"
    t.integer  "administrador_id"
    t.date     "date_delivered_customer"
    t.string   "rejected_by",                          default: ""
    t.integer  "difficulty_quote_id"
    t.integer  "coordinator_id"
    t.text     "cost_observations"
    t.datetime "begin_analysis"
    t.string   "spreadsheet_file_name"
    t.string   "spreadsheet_content_type"
    t.integer  "spreadsheet_file_size"
    t.datetime "spreadsheet_updated_at"
  end

  add_index "quotes", ["administrador_id"], name: "index_quotes_on_administrador_id", using: :btree
  add_index "quotes", ["adviser_id"], name: "index_quotes_on_adviser_id", using: :btree
  add_index "quotes", ["agency_id"], name: "index_quotes_on_agency_id", using: :btree
  add_index "quotes", ["client_id"], name: "index_quotes_on_client_id", using: :btree
  add_index "quotes", ["difficulty_quote_id"], name: "index_quotes_on_difficulty_quote_id", using: :btree

  create_table "quotes_anexes", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "quote_id"
    t.string   "anexo_file_name"
    t.string   "anexo_content_type"
    t.integer  "anexo_file_size"
    t.datetime "anexo_updated_at"
  end

  add_index "quotes_anexes", ["quote_id"], name: "index_quotes_anexes_on_quote_id", using: :btree

  create_table "remission_details", force: true do |t|
    t.integer "quantity"
    t.string  "description"
    t.integer "remission_id"
    t.integer "product_id"
  end

  add_index "remission_details", ["product_id"], name: "index_remission_details_on_product_id", using: :btree
  add_index "remission_details", ["remission_id"], name: "index_remission_details_on_remission_id", using: :btree

  create_table "remissions", force: true do |t|
    t.string   "number_remission",    limit: 100
    t.date     "date_remission"
    t.text     "observations"
    t.text     "delivery"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "administrador_id"
    t.integer  "production_order_id"
    t.string   "status"
  end

  add_index "remissions", ["administrador_id"], name: "index_remissions_on_administrador_id", using: :btree
  add_index "remissions", ["production_order_id"], name: "index_remissions_on_production_order_id", using: :btree

  create_table "remissions_details", force: true do |t|
    t.integer  "quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "remission_id"
    t.integer  "product_id"
  end

  add_index "remissions_details", ["product_id"], name: "index_remissions_details_on_product_id", using: :btree
  add_index "remissions_details", ["remission_id"], name: "index_remissions_details_on_remission_id", using: :btree

  create_table "status_quotes", force: true do |t|
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "quote_id"
    t.integer  "administrador_id"
  end

  add_index "status_quotes", ["administrador_id"], name: "index_status_quotes_on_administrador_id", using: :btree
  add_index "status_quotes", ["quote_id"], name: "index_status_quotes_on_quote_id", using: :btree

  create_table "types", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "types_attachments", force: true do |t|
    t.string   "name"
    t.boolean  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "types_descriptions", force: true do |t|
    t.integer  "type_id"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "types_descriptions", ["type_id"], name: "index_types_descriptions_on_type_id", using: :btree

  create_table "versions", force: true do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  create_table "work_order_comments", force: true do |t|
    t.text     "comment"
    t.integer  "administrador_id"
    t.integer  "work_order_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "work_order_comments", ["administrador_id"], name: "index_work_order_comments_on_administrador_id", using: :btree
  add_index "work_order_comments", ["work_order_id"], name: "index_work_order_comments_on_work_order_id", using: :btree

  create_table "work_order_details", force: true do |t|
    t.integer  "quantity"
    t.float    "price"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "work_order_id"
    t.integer  "product_id"
    t.text     "description"
  end

  add_index "work_order_details", ["product_id"], name: "index_work_order_details_on_product_id", using: :btree
  add_index "work_order_details", ["work_order_id"], name: "index_work_order_details_on_work_order_id", using: :btree

  create_table "work_orders", force: true do |t|
    t.date     "application_date"
    t.string   "cost_center",         limit: 100
    t.date     "execution_date"
    t.string   "contractor"
    t.text     "overcost"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "production_order_id"
    t.text     "address"
    t.integer  "administrador_id"
    t.string   "status_order"
    t.string   "name"
  end

  add_index "work_orders", ["administrador_id"], name: "index_work_orders_on_administrador_id", using: :btree

  add_foreign_key "administradores", "departments", name: "administradores_department_id_fk"

  add_foreign_key "alert_users", "administradores", name: "alert_users_administrador_id_fk"
  add_foreign_key "alert_users", "alerts", name: "alert_users_alert_id_fk"

  add_foreign_key "bill_details", "bills", name: "bill_details_bill_id_fk"
  add_foreign_key "bill_details", "products", name: "bill_details_product_id_fk"

  add_foreign_key "bills", "administradores", name: "bills_administrador_id_fk"
  add_foreign_key "bills", "production_orders", name: "bills_production_order_id_fk"

  add_foreign_key "comments", "administradores", name: "comments_administrador_id_fk"

  add_foreign_key "history_order_production_statuses", "administradores", name: "history_order_production_statuses_administrador_id_fk"
  add_foreign_key "history_order_production_statuses", "production_orders", name: "history_order_production_statuses_production_order_id_fk"

  add_foreign_key "materiales_details", "materials_lists", name: "materiales_details_materials_list_id_fk"

  add_foreign_key "materials_lists", "production_orders", name: "materials_lists_production_order_id_fk"

  add_foreign_key "product_types", "products", name: "product_types_product_id_fk"
  add_foreign_key "product_types", "types", name: "product_types_type_id_fk"

  add_foreign_key "production_order_anexes", "production_orders", name: "anexe_orders_productions_production_order_id_fk"

  add_foreign_key "production_order_binnacle_anexes", "production_order_binnacles", name: "production_order_binnacle_id_fk"

  add_foreign_key "production_order_binnacles", "administradores", name: "production_order_binnacles_administrador_id_fk"
  add_foreign_key "production_order_binnacles", "production_orders", name: "production_order_binnacles_production_order_id_fk"

  add_foreign_key "production_orders", "administradores", name: "production_orders_administrador_id_fk"
  add_foreign_key "production_orders", "quotes", name: "production_orders_quote_id_fk"

  add_foreign_key "production_orders_materiales", "production_orders", name: "production_orders_materiales_production_order_id_fk"

  add_foreign_key "products", "categories", name: "products_categorie_id_fk"

  add_foreign_key "quote_details", "products", name: "quote_details_product_id_fk"
  add_foreign_key "quote_details", "quotes", name: "quote_details_quote_id_fk"

  add_foreign_key "quotes", "administradores", name: "quotes_administrador_id_fk"
  add_foreign_key "quotes", "advisers", name: "quotes_adviser_id_fk"
  add_foreign_key "quotes", "agencies", name: "quotes_agency_id_fk"
  add_foreign_key "quotes", "clients", name: "quotes_client_id_fk"
  add_foreign_key "quotes", "difficulty_quotes", name: "quotes_difficulty_quote_id_fk"

  add_foreign_key "quotes_anexes", "quotes", name: "quotes_anexes_quote_id_fk"

  add_foreign_key "remission_details", "products", name: "remission_details_product_id_fk"
  add_foreign_key "remission_details", "remissions", name: "remission_details_remission_id_fk"

  add_foreign_key "remissions", "administradores", name: "remissions_administrador_id_fk"
  add_foreign_key "remissions", "production_orders", name: "remissions_production_order_id_fk"

  add_foreign_key "remissions_details", "products", name: "remissions_details_product_id_fk"
  add_foreign_key "remissions_details", "remissions", name: "remissions_details_remission_id_fk"

  add_foreign_key "status_quotes", "administradores", name: "status_quotes_administrador_id_fk"
  add_foreign_key "status_quotes", "quotes", name: "status_quotes_quote_id_fk"

  add_foreign_key "work_order_details", "products", name: "work_order_details_product_id_fk"
  add_foreign_key "work_order_details", "work_orders", name: "work_order_details_work_order_id_fk"

  add_foreign_key "work_orders", "administradores", name: "work_orders_administrador_id_fk"
  add_foreign_key "work_orders", "production_orders", name: "work_orders_production_order_id_fk"

end
