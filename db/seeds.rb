# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

departamento1 = Department.create(:name=>"Ventas", :color=>"azul")
departamento2 = Department.create(:name=>"Costo", :color=>"azul")
# Carga de Administrador
Administrador.create(:email => "admin@vallasyavisos.com", :password => "12345678", :password_confirmation => "12345678", :roles_mask => 1, :nombre => "Administrador", :rol => "admin", :department_id=>departamento1.id)
puts "Usuario Administrador Creado:"
puts "Correo: admin@vallasyavisos.com"
puts "password: 12345678"

Administrador.create(:email => "vendedor@vallasyavisos.com", :password => "12345678", :password_confirmation => "12345678", :roles_mask => 1, :nombre => "vendedor", :rol => "vendedor", :department_id=>departamento1.id)
puts "Usuario vendedor Creado:"
puts "Correo: vendedor@vallasyavisos.com"
puts "password: 12345678"

Administrador.create(:email => "costo@vallasyavisos.com", :password => "12345678", :password_confirmation => "12345678", :roles_mask => 1, :nombre => "costos", :rol => "costo", :department_id=>departamento2.id)
puts "Usuario jefe costo Creado:"
puts "Correo: costo@vallasyavisos.com"
puts "password: 12345678"

Administrador.create(:email => "coordinador_costo@vallasyavisos.com", :password => "12345678", :password_confirmation => "12345678", :roles_mask => 1, :nombre => "costos", :rol => "analista", :department_id=>departamento2.id)
puts "Usuario coordinador costo Creado:"
puts "Correo: coordinador_costo@vallasyavisos.com"
puts "password: 12345678"

Administrador.create(:email => "produccion@vallasyavisos.com", :password => "12345678", :password_confirmation => "12345678", :roles_mask => 1, :nombre => "produccion", :rol => "produccion", :department_id=>departamento2.id)
puts "Usuario Jefe produccion Creado:"
puts "Correo: produccion@vallasyavisos.com"
puts "password: 12345678"

Administrador.create(:email => "coordinador_produccion@vallasyavisos.com", :password => "12345678", :password_confirmation => "12345678", :roles_mask => 1, :nombre => "produccion", :rol => "coordinador_produccion", :department_id=>departamento2.id)
puts "Usuario Coordinador produccion Creado:"
puts "Correo: coordinador_produccion@vallasyavisos.com"
puts "password: 12345678"

Administrador.create(:email => "almacen@vallasyavisos.com", :password => "12345678", :password_confirmation => "12345678", :roles_mask => 1, :nombre => "almacen", :rol => "almacen", :department_id=>departamento2.id)
puts "Usuario almacen Creado:"
puts "Correo: almacen@vallasyavisos.com"
puts "password: 12345678"

Administrador.create(:email => "maquina@vallasyavisos.com", :password => "12345678", :password_confirmation => "12345678", :roles_mask => 1, :nombre => "maquina", :rol => "maquina", :department_id=>departamento2.id)
puts "Usuario maquina Creado:"
puts "Correo: maquina@vallasyavisos.com"
puts "password: 12345678"


Administrador.create(:email => "inventario@vallasyavisos.com", :password => "12345678", :password_confirmation => "12345678", :roles_mask => 1, :nombre => "inventario", :rol => "inventario", :department_id=>departamento2.id)
puts "Usuario inventario Creado:"
puts "Correo: inventario@vallasyavisos.com"
puts "password: 12345678"


Administrador.create(:email => "facturacion@vallasyavisos.com", :password => "12345678", :password_confirmation => "12345678", :roles_mask => 1, :nombre => "facturacion", :rol => "facturacion", :department_id=>departamento2.id)
puts "Usuario facturacion Creado:"
puts "Correo: facturacion@vallasyavisos.com"
puts "password: 12345678"

Administrador.create(:email => "seguimiento@vallasyavisos.com", :password => "12345678", :password_confirmation => "12345678", :roles_mask => 1, :nombre => "seguimiento", :rol => "seguimiento", :department_id=>departamento2.id)
puts "Usuario seguimiento Creado:"
puts "Correo: seguimiento@vallasyavisos.com"
puts "password: 12345678"



cliente1 = Client.create(:code => "001", :name => "PUBLICIDAD DIGESY", :nit => "J-1234568-9", :email=>"publicidadg@digesy.com", :contact=>"maria perez", :office=>"Vendedora", :phone=>"0426536285", :address=>"caracas")

cliente2 = Client.create(:code => "002", :name => "DIGITAL SPRINT", :nit => "J-9852625-6", :email=>"digital@gmail.com", :contact=>"pedro perez", :office=>"Vendedora", :phone=>"0426536285", :address=>"caracas")

cliente3 = Client.create(:code => "003", :name => "PRINT ALL C.A", :nit => "J-684684684-8", :email=>"PRINTALL@GMAIL.com", :contact=>"LUIS AGUILAR", :office=>"Vendedora", :phone=>"0426536285", :address=>"caracas")

cliente4 = Client.create(:code => "004", :name => "PUBLICIDAD FELIZ", :nit => "J-95455525-4", :email=>"publicidadfeliz@hotmail.com", :contact=>"wilfredo contreras", :office=>"Vendedora", :phone=>"0426536285", :address=>"caracas")

Adviser.create(:assessor_code => "123", :name => "JONATHAN RODRIGUEZ",  :phone=>"0426536285", :address=>"caracas", :client_id=>cliente1.id, :email=>"correo@correo.com") 
Adviser.create(:assessor_code => "456", :name => "CARLOS YUNCOSA",  :phone=>"0426536285", :address=>"caracas", :client_id=>cliente2.id, :email=>"correo@correo.com")
adviser = Adviser.create(:assessor_code => "789", :name => "WERNER SILVA",  :phone=>"0426536285", :address=>"caracas", :client_id=>cliente3.id, :email=>"correo@correo.com")
Adviser.create(:assessor_code => "258", :name => "ANTONIO AGUILLON",  :phone=>"0426536285", :address=>"caracas", :client_id=>cliente4.id, :email=>"correo@correo.com")


vendedor = Administrador.find_by_email("vendedor@vallasyavisos.com")
vendedor.adviser = adviser
vendedor.save

categoria1 = Category.create(:name => "Ventas")
puts "Categoria creada:"
puts "nombre: Venta"

categoria2 =  Category.create(:name => "Servicios")
puts "Categoria creada:"
puts "nombre: Servicios"

type1 = Type.create(:name => "EN FACHADA")
puts "Tipo creado:"
puts "nombre: EN FACHADA"

type2 = Type.create(:name => "EN TIERRA")
puts "Tipo creado:"
puts "nombre: EN TIERRA"

type3 =  Type.create(:name => "TUBO")
puts "Tipo creado:"
puts "nombre: TUBO"

type4 = Type.create(:name => "LAMINA")
puts "Tipo creado:"
puts "nombre: LAMINA"

type5 =  Type.create(:name => "ACRILICO")
puts "Tipo creado:"
puts "nombre: ACRILICO"

type6 = Type.create(:name => "POLIESTIRENO")
puts "Tipo creado:"
puts "nombre: POLIESTIRENO"

type7 = Type.create(:name => "BASTIDOR")
puts "Tipo creado:"
puts "nombre: BASTIDOR"

type8 = Type.create(:name => "LUMINOSO")
puts "Tipo creado:"
puts "nombre: LUMINOSO"

type9 = Type.create(:name => "MDF")
puts "Tipo creado:"
puts "nombre: MDF"

[type1, type2, type3].each do |type|

  TypeDescription.create(:description => "TAMAÑO 4X3", type_id: type.id)
  TypeDescription.create(:description => "TAMAÑO 4X6", type_id: type.id)
  TypeDescription.create(:description => "TAMAÑO 6X4", type_id: type.id)
  TypeDescription.create(:description => "TAMAÑO 8X4", type_id: type.id)
  TypeDescription.create(:description => "TAMAÑO 10X4", type_id: type.id)
  TypeDescription.create(:description => "TAMAÑO 12X4", type_id: type.id)
  TypeDescription.create(:description => "TAMAÑO OTRO", type_id: type.id)

  TypeDescription.create(:description => "LAMINAS CAL 20", type_id: type.id)
  TypeDescription.create(:description => "LAMINAS CAL 22", type_id: type.id)
  TypeDescription.create(:description => "LAMINAS CAL 24", type_id: type.id)
  TypeDescription.create(:description => "LAMINAS CAL 26", type_id: type.id)
  TypeDescription.create(:description => "LAMINAS CAL OTRO", type_id: type.id)

  TypeDescription.create(:description => "TAMAÑO LAMINA 1,86 X 0,86", type_id: type.id)
  TypeDescription.create(:description => "TAMAÑO LAMINA 1,90 X 0,90", type_id: type.id)
  TypeDescription.create(:description => "TAMAÑO LAMINA 2X1", type_id: type.id)
  TypeDescription.create(:description => "TAMAÑO LAMINA OTRO", type_id: type.id)

  TypeDescription.create(:description => "TORRES 4 MTS", type_id: type.id)
  TypeDescription.create(:description => "TORRES 6 MTS", type_id: type.id)
  TypeDescription.create(:description => "TORRES 7 MTS", type_id: type.id)
  TypeDescription.create(:description => "TORRES OTRO", type_id: type.id)

  TypeDescription.create(:description => "ANGULO 1", type_id: type.id)
  TypeDescription.create(:description => "ANGULO 1/2 X 1/8", type_id: type.id)
  TypeDescription.create(:description => "ANGULO 2 X 1/8", type_id: type.id)
  TypeDescription.create(:description => "ANGULO OTRO", type_id: type.id)

  TypeDescription.create(:description => "REFUERZO 1/2 X 1/8", type_id: type.id)

  TypeDescription.create(:description => "PINTURA ANTICORROSIVO GRIS", type_id: type.id)
  TypeDescription.create(:description => "PINTURA ANTICORROSIVO NEGRO", type_id: type.id)
  TypeDescription.create(:description => "PINTURA ANTICORROSIVO OTRO", type_id: type.id)

  TypeDescription.create(:description => "INSTALACION TIERRA", type_id: type.id)
  TypeDescription.create(:description => "INSTALACION TERRAZA", type_id: type.id)
  TypeDescription.create(:description => "INSTALACION FACHADA", type_id: type.id)
  TypeDescription.create(:description => "INSTALACION PISO CONCRETO", type_id: type.id)
  TypeDescription.create(:description => "INSTALACION OTRO", type_id: type.id)

end

  # BASTIDORES
  TypeDescription.create(:description => "HIERRO TUBO CUADRADO DE 1'' ", type_id: type7.id)
  TypeDescription.create(:description => "HIERRO PLATINA DE 1/2", type_id: type7.id)
  TypeDescription.create(:description => "HIERRO OTRO", type_id: type7.id)
  TypeDescription.create(:description => "ALUMINIO TUBO CUADRADO DE 1''", type_id: type7.id)
  TypeDescription.create(:description => "ALUMINIO PLATINA DE 1/2", type_id: type7.id)
  TypeDescription.create(:description => "ALUMINIO OTRO", type_id: type7.id)

  TypeDescription.create(:description => "PINTURA ANTICORROSIVO GRIS", type_id: type7.id)
  TypeDescription.create(:description => "PINTURA ANTICORROSIVO NEGRO", type_id: type7.id)
  TypeDescription.create(:description => "PINTURA ANTICORROSIVO SOLO PARA HIERRO", type_id: type7.id)

  TypeDescription.create(:description => "LONA", type_id: type7.id)


  TypeDescription.create(:description => "ACABADOS TUNEL DE 8 CMS POR CONTORNO", type_id: type7.id)
  TypeDescription.create(:description => "ACABADOS OTROS", type_id: type7.id)

  TypeDescription.create(:description => "INSTALACION FACHADA", type_id: type7.id)
  TypeDescription.create(:description => "INSTALACION OTRO", type_id: type7.id)


  # ACRILICOS
  TypeDescription.create(:description => "ACRILICO 4MM", type_id: type5.id)
  TypeDescription.create(:description => "ACRILICO 6MM", type_id: type5.id)
  TypeDescription.create(:description => "ACRILICO OTRO", type_id: type5.id)
  TypeDescription.create(:description => "TIPO DE ACRILICO ESPEJO", type_id: type5.id)
  TypeDescription.create(:description => "TIPO DE ACRILICO CRISTAL", type_id: type5.id)
  TypeDescription.create(:description => "TIPO DE ACRILICO OTRO", type_id: type5.id)
  TypeDescription.create(:description => "COLOR ACRILICO BLANCO", type_id: type5.id)
  TypeDescription.create(:description => "COLOR ACRILICO GRIS", type_id: type5.id)
  TypeDescription.create(:description => "ILUMINACION INTERNA", type_id: type5.id)
  TypeDescription.create(:description => "ILUMINACION INTERNA LED", type_id: type5.id)
  TypeDescription.create(:description => "ILUMINACION INTERNA T8", type_id: type5.id)
  TypeDescription.create(:description => "INSTALACION FACHADA", type_id: type5.id)
  TypeDescription.create(:description => "INSTALACION TERRAZA", type_id: type5.id)
  TypeDescription.create(:description => "INSTALACION OTRO", type_id: type5.id)

  # MDF
  TypeDescription.create(:description => "ESPESOR 5MM", type_id: type9.id)
  TypeDescription.create(:description => "ESPESOR 7MM", type_id: type9.id)
  TypeDescription.create(:description => "ESPESOR 9MM", type_id: type9.id)
  TypeDescription.create(:description => "ESPESOR OTRO", type_id: type9.id)
  TypeDescription.create(:description => "DECORADO CON VINILO IMPRESO", type_id: type9) .id 
  TypeDescription.create(:description => "DECORADO CON VINILO PLOTER", type_id: type9) .id 
  TypeDescription.create(:description => "DECORADO CON OTRO", type_id: type9) .id 
  TypeDescription.create(:description => "SOPORTE BUJES", type_id: type9) .id 
  TypeDescription.create(:description => "SOPORTE CERRUCHOS", type_id: type9) .id 
  TypeDescription.create(:description => "SOPORTE CINTA DOBLE FAZ", type_id: type9) .id 
  TypeDescription.create(:description => "SOPORTE OTRO", type_id: type9) .id 
  TypeDescription.create(:description => "INSTALACION PARED", type_id: type9.id)
  TypeDescription.create(:description => "INSTALACION COLGADO", type_id: type9.id)
  TypeDescription.create(:description => "INSTALACION OTRO", type_id: type9.id)

  # AVISO LUMINOS
  TypeDescription.create(:description => "ALFAGIA 16CMS", type_id: type8.id)
  TypeDescription.create(:description => "ALFAGIA 20CMS", type_id: type8.id)
  TypeDescription.create(:description => "ALFAGIA OTRO", type_id: type8.id)
  TypeDescription.create(:description => "ILUMINACION OTRA", type_id: type8.id)
  TypeDescription.create(:description => "ILUMINACION LED", type_id: type8.id)
  TypeDescription.create(:description => "ILUMINACION T8", type_id: type8.id)

  TypeDescription.create(:description => "CONEXION TIMER", type_id: type8.id)
  TypeDescription.create(:description => "CONEXION FOTOCELDA ", type_id: type8.id)
  TypeDescription.create(:description => "CONEXION OTRA", type_id: type8.id)

  TypeDescription.create(:description => "INSTALACION TIERRA", type_id: type8.id)
  TypeDescription.create(:description => "INSTALACION TERRAZA", type_id: type8.id)
  TypeDescription.create(:description => "INSTALACION FACHADA", type_id: type8.id)
  TypeDescription.create(:description => "INSTALACION OTRO", type_id: type8.id)

  TypeDescription.create(:description => "LONA TRASLUCIDA", type_id: type8) .id 
  TypeDescription.create(:description => "ACABADOS LONA TUNEL DE 6 CMS POR CONTORNO", type_id: type8) .id 

prod1= Product.create(:name => "VALLA", :status => "A", :categorie_id => categoria1.id)
prod2= Product.create(:name => "AVISO", :status => "A", :categorie_id => categoria1.id)
prod3= Product.create(:name => "SEÑAL", :status => "A", :categorie_id => categoria1.id)
prod4= Product.create(:name => "TOTEM", :status => "A", :categorie_id => categoria1.id)
prod5= Product.create(:name => "CERRAMIENTO", :status => "A", :categorie_id => categoria1.id)
prod6= Product.create(:name => "INSTALACION", :status => "A", :categorie_id => categoria2.id)
prod7= Product.create(:name => "TRASLADO", :status => "A", :categorie_id => categoria2.id)
prod8= Product.create(:name => "MANTENIMIENTO", :status => "A", :categorie_id => categoria2.id)
prod9= Product.create(:name => "DECORACION", :status => "A", :categorie_id => categoria2.id)


ProductType.create(:product_id => prod1.id , :type_id =>   type1.id );
ProductType.create(:product_id => prod1.id , :type_id =>   type2.id );
ProductType.create(:product_id => prod1.id , :type_id =>   type3.id );
ProductType.create(:product_id => prod2.id , :type_id =>   type4.id );
ProductType.create(:product_id => prod2.id , :type_id =>   type5.id );
ProductType.create(:product_id => prod2.id , :type_id =>   type6.id );
ProductType.create(:product_id => prod2.id , :type_id =>   type7.id );
ProductType.create(:product_id => prod2.id , :type_id =>   type8.id );
ProductType.create(:product_id => prod2.id , :type_id =>   type9.id );

DifficultyQuote.create(:name => 'Alta' , :quantity =>  8 ,:lapse => 'Dias', :type_lapse =>  'Habiles');
DifficultyQuote.create(:name => 'Alta' , :quantity =>  8 ,:lapse => 'Dias', :type_lapse =>  'Habiles');
DifficultyQuote.create(:name => 'Alta' , :quantity =>  8 ,:lapse => 'Dias', :type_lapse =>  'Habiles');
