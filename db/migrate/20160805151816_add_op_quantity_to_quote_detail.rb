class AddOpQuantityToQuoteDetail < ActiveRecord::Migration
  def change
    add_column :quote_details, :op_quantity, :integer, default: 0
  end
end
