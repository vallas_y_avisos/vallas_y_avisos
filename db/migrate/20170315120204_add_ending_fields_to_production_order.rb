class AddEndingFieldsToProductionOrder < ActiveRecord::Migration
  def change
    add_column :production_orders, :reason, :string
    add_column :production_orders, :another_reason, :string
  end
end
