class CreateDifficultyQuotes < ActiveRecord::Migration
  def change
    create_table :difficulty_quotes do |t|
      t.string :name
      t.integer :quantity
      t.string :lapse
      t.string :type_lapse

      t.timestamps
    end
  end
end
