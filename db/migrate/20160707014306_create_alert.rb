class CreateAlert < ActiveRecord::Migration
  def change
    create_table :alerts do |t|
      t.string :type_alert
      t.string :description
      t.string :model
      t.integer :model_id     
      t.timestamps 
    end
  end
end
