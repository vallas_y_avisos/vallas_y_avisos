class DelDateTimeQuotes < ActiveRecord::Migration
  def self.up
    remove_column :quotes, :date
    remove_column :quotes, :time_quote
    remove_column :quotes, :date_fingering
  end
end
