class FixDateColumnsToDatetime < ActiveRecord::Migration
  def change
    change_column :production_orders, :start_date_production, :datetime
    change_column :production_orders, :final_delivery_date, :datetime
    
  end
end
