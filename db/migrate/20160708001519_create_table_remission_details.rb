class CreateTableRemissionDetails < ActiveRecord::Migration
  def change
    create_table :remission_details do |t|
    	t.integer :quantity
    	t.string  :description

    	t.timestamp
    end
    add_reference :remission_details, :remission, index: true
    add_foreign_key :remission_details, :remissions
    add_reference :remission_details, :product, index: true
    add_foreign_key :remission_details, :products
  end
end
