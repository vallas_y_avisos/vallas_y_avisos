class AddCodigoEmailAdvisers < ActiveRecord::Migration
  def self.up
    add_column :advisers, :email, :string
    add_column :advisers, :assessor_code, :string
    remove_column :advisers, :type_advisers
  end
end
