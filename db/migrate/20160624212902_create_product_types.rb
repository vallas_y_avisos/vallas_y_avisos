class CreateProductTypes < ActiveRecord::Migration
  def change
    create_table :product_types do |t|

      t.timestamps
    end
    add_reference :product_types, :product, index: true
    add_foreign_key :product_types, :products

    add_reference :product_types, :type, index: true
    add_foreign_key :product_types, :types    
  end
end
