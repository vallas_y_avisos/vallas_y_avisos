class DeleteAtributesProductionOrder < ActiveRecord::Migration
  def self.up
    remove_column :production_orders, :reference
    remove_column :production_orders, :agency_percentage
    
    remove_reference :production_orders, :agency
    remove_reference :production_orders, :adviser
    remove_reference :production_orders, :client

  end
  def self.down
    add_column :production_orders, :reference, :string
    add_column :production_orders, :agency_percentage, :float

    add_reference :production_orders, :agency, index: true
    add_foreign_key :production_orders, :agencies

    add_reference :production_orders, :adviser, index: true
    add_foreign_key :production_orders, :advisers

    add_reference :production_orders, :client, index: true
    add_foreign_key :production_orders, :clients
  end
end
