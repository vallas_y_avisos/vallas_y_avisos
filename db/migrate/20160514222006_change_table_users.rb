class ChangeTableUsers < ActiveRecord::Migration
  def self.up
  	remove_column :users, :apellido
  	rename_column :users, :nombre, :name
  	add_reference :users, :department, index: true, foreign_key: true
  end
  def self.down
    add_column :users, :apellido, :string, null: false, limit: 30, default: 'Apellido'
    remove_reference :users, :department
    rename_column :users, :name, :nombre
  end
end
