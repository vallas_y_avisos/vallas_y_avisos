class CreateAgencies < ActiveRecord::Migration
	def self.up
    create_table :agencies do |t|
      t.string  :name
      t.string  :email
      t.string  :phone, :limit=>20
      t.string  :address

      t.timestamps
    end
    add_index :agencies, [:name]
  end

  def self.down
    remove_index :agencies, [:name]
    drop_table :agencies
  end
end

