class DestroyUser < ActiveRecord::Migration
  def self.up
  	#remove_foreign_key :comments, :user
  	remove_column :comments, :user_id
  	add_reference  :comments, :administrador, index: true
    add_foreign_key :comments, :administradores

    remove_foreign_key :production_orders, :user
  	add_reference  :production_orders, :administrador, index: true
    add_foreign_key :production_orders, :administradores

    remove_foreign_key :quotes, :user
  	add_reference  :quotes, :administrador, index: true
    add_foreign_key :quotes, :administradores

    remove_foreign_key :remissions, :user
  	add_reference  :remissions, :administrador, index: true
    add_foreign_key :remissions, :administradores

    remove_foreign_key :work_orders, :user
  	add_reference  :work_orders, :administrador, index: true
    add_foreign_key :work_orders, :administradores

  	drop_table :users

  end 
  def self.down 

    create_table(:users) do |t|
      ## Database authenticatable
      t.string :email,              :null => false, :default => ""
      t.string :encrypted_password, :null => false, :default => ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, :default => 0, :null => false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      ## Confirmable
      t.string   :confirmation_token
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      t.string   :unconfirmed_email # Only if using reconfirmable

      ## Lockable
      # t.integer  :failed_attempts, :default => 0, :null => false # Only if lock strategy is :failed_attempts
      # t.string   :unlock_token # Only if unlock strategy is :email or :both
      # t.datetime :locked_at


      t.timestamps
    end

    add_index :users, :email,                :unique => true
    add_index :users, :reset_password_token, :unique => true
    # add_index :users, :confirmation_token,   :unique => true
    # add_index :users, :unlock_token,         :unique => true

    remove_foreign_key :comments, :administrador
  	add_reference  :comments, :user, index: true
    add_foreign_key :comments, :users

    remove_foreign_key :production_orders, :administrador
  	add_reference  :production_orders, :user, index: true
    add_foreign_key :production_orders, :users

	remove_foreign_key :quotes, :administrador
  	add_reference  :quotes, :user, index: true
    add_foreign_key :quotes, :users    

    remove_foreign_key :remissions, :administrador
  	add_reference  :remissions, :user, index: true
    add_foreign_key :remissions, :users    

    remove_foreign_key :work_orders, :administrador
  	add_reference  :work_orders, :user, index: true
    add_foreign_key :work_orders, :users  
  end
end
