class DeleteUnitValueQuoteDetails < ActiveRecord::Migration
  def self.up
    remove_column :quote_details, :unit_value
  end
end
