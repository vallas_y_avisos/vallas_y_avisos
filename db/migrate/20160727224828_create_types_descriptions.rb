class CreateTypesDescriptions < ActiveRecord::Migration
  def change
    create_table :types_descriptions do |t|
      t.references :type, index: true
      t.string :description

      t.timestamps
    end
  end
end
