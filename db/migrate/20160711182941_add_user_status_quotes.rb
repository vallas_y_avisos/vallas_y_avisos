class AddUserStatusQuotes < ActiveRecord::Migration
  def change
    add_reference :status_quotes, :administrador, index: true
    add_foreign_key :status_quotes, :administradores
  end
end

