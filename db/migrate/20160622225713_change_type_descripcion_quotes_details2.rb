class ChangeTypeDescripcionQuotesDetails2 < ActiveRecord::Migration
  def up
    change_table :quote_details do |t|
      t.change :description, :text
    end
  end
end
