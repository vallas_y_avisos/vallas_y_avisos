class CreateTableProductionOrderBinnacles < ActiveRecord::Migration
  def change
  	create_table :production_order_binnacles do |t|
      t.timestamps
      t.string :title
      t.text :description
  	end
  	add_reference :production_order_binnacles, :production_order, index: true
  	add_foreign_key :production_order_binnacles, :production_orders

  	add_reference :production_order_binnacles, :administrador, index: true
  	add_foreign_key :production_order_binnacles, :administradores
  end
end
