class ChangeTypeInProductionOrder < ActiveRecord::Migration
  def self.up
  	rename_column :production_orders, :type, :type_order
  end
  def self.down
    rename_column :production_orders, :type_order, :type
  end
end
