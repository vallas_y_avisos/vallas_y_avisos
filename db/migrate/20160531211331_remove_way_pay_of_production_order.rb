class RemoveWayPayOfProductionOrder < ActiveRecord::Migration
  def self.up
    remove_column :production_orders, :way_pay
  end
  def self.down
    add_column :production_orders, :way_pay, :string, :limit => 10
  end
end
