class CreateWorkOrderesDetailes < ActiveRecord::Migration
  def change
    create_table :work_order_details do |t|
    	t.integer :quantity
    	t.float   :price

    	t.timestamps
    end
    add_reference :work_order_details, :work_order, index: true
    add_foreign_key :work_order_details, :work_orders

    add_reference :work_order_details, :product, index: true
    add_foreign_key :work_order_details, :products
  end

  def self.down  
    remove_foreign_key :work_order_details, :work_orders
    remove_foreign_key :work_order_details, :products

    drop_table :work_order_details

  end
end
