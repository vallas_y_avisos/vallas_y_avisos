class UpdateDescriptionToBillDetail < ActiveRecord::Migration
  def change
    change_column :bill_details, :description, :text
  end
end
