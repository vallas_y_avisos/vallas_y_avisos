class AddColumnStatusInProductionOrders < ActiveRecord::Migration
  def self.up
  	add_column :production_orders, :status, :string
  end
  def self.down
  	remove_column :production_orders, :status
  end
end
