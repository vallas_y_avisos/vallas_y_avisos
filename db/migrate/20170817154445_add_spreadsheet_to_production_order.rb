class AddSpreadsheetToProductionOrder < ActiveRecord::Migration
  def change
    add_attachment :production_orders, :spreadsheet
  end
end
