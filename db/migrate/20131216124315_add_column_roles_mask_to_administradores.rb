class AddColumnRolesMaskToAdministradores < ActiveRecord::Migration
  def change
    add_column :administradores, :roles_mask, :integer
  end
end