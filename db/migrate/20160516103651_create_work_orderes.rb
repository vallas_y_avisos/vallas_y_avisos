class CreateWorkOrderes < ActiveRecord::Migration
  def change
    create_table :work_orders do |t|
    	t.string :number, :limit => 100
    	t.date :application_date
    	t.string :cost_center, :limit => 100
    	t.date :execution_date
    	t.string :reference
    	t.string :contractor
    	t.text :overcost

      t.timestamps
    end
    add_reference :work_orders, :client, index: true
    add_foreign_key :work_orders, :clients

    add_reference :work_orders, :user, index: true
    add_foreign_key :work_orders, :users

  end
  def self.down  
    remove_foreign_key :work_orders, :clients
    remove_foreign_key :work_orders, :users

    drop_table :work_orders

  end
end
