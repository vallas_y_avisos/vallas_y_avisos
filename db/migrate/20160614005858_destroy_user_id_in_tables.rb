class DestroyUserIdInTables < ActiveRecord::Migration
  def self.up
  	remove_column :production_orders, :user_id
  	remove_column :quotes, :user_id
    remove_column :remissions, :user_id
    remove_column :work_orders, :user_id
  end 
end
