class AddProductionOrderIdMaterialsList < ActiveRecord::Migration
  def change
    add_reference :materials_lists, :production_order, index: true
    add_foreign_key :materials_lists, :production_orders
  end
end
