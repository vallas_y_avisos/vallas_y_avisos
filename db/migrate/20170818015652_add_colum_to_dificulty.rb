class AddColumToDificulty < ActiveRecord::Migration
  def change
    add_column :difficulty_quotes, :is_quote, :boolean, default: true
    add_column :difficulty_quotes, :is_production, :boolean, default: false
  end
end
