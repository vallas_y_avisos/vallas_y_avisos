class CreateTableProductionOrderBinnacleAnexes < ActiveRecord::Migration
  def change
  	create_table :production_order_binnacle_anexes do |t|
      t.timestamps
  	end
  	add_reference :production_order_binnacle_anexes, :production_order_binnacle, index: {:name => "index_production_order_binnacle_id"}
  	add_foreign_key :production_order_binnacle_anexes, :production_order_binnacles, name: 'production_order_binnacle_id_fk'
  end
end
