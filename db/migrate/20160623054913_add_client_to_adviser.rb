class AddClientToAdviser < ActiveRecord::Migration
  def change
    add_reference :advisers, :client, index: true
    remove_reference :advisers, :agency
  end
end
