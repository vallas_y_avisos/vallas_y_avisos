class AddAdviserIdToAdministrador < ActiveRecord::Migration
  def change
    add_reference :administradores, :adviser, index: true, null: true
  end
end
