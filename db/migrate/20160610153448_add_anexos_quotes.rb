class AddAnexosQuotes < ActiveRecord::Migration
  def self.up    
    add_attachment :quotes_anexes, :anexo       
  end
  def self.down
    remove_attachment :quotes_anexes, :anexo   
  end
end
