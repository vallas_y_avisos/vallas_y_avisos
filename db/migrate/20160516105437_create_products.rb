class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
    	t.string :name
    	t.float :price
    	t.string :status

      t.timestamps
    end
  end
  def self.down  
    drop_table :products
  end
end
