class AddPositionAndCityToClient < ActiveRecord::Migration
  def change
    add_column :clients, :position, :string
    add_column :clients, :city, :string
  end
end
