class ChangeStatusInOrder < ActiveRecord::Migration
  def self.up
  	rename_column :production_orders, :status, :status_order
  end
  def self.down
    rename_column :production_orders, :status_order, :status
  end
end
