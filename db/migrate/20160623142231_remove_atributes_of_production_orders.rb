class RemoveAtributesOfProductionOrders < ActiveRecord::Migration
  def self.up
    remove_column :production_orders, :delivery_date_printing
    remove_column :production_orders, :request_to
    remove_column :production_orders, :advance
    remove_column :production_orders, :type_order
 end
  def self.down
    add_column :production_orders, :delivery_date_printing, :date
    add_column :production_orders, :request_to, :string
    add_column :production_orders, :advance, :float
    add_column :production_orders, :type_order, :string
  end
end


