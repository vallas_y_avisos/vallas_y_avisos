class FixDateColumnsToDatetimev2 < ActiveRecord::Migration
  def change
    change_column :production_orders, :date_elaboration, :datetime
    change_column :production_orders, :actual_delivey_date, :datetime
  end
end
