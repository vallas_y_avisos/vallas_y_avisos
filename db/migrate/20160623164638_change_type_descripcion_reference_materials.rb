class ChangeTypeDescripcionReferenceMaterials < ActiveRecord::Migration
  def up
    change_table :materiales_details do |t|
      t.change :description, :text
      t.change :observation, :text
      t.change :materials_reference, :text
      t.change :materials_name, :text
    end
  end
end
