class AddReferenceWorkOrder < ActiveRecord::Migration
  def self.up
  	add_reference :work_orders, :production_order
  	remove_reference :work_orders, :client
  	add_foreign_key :work_orders, :production_orders
  	add_column :work_orders, :address, :text
    add_column :work_order_details, :description, :text
    remove_column :work_orders, :reference
    remove_column :work_orders, :number
  end
  def self.down
    add_reference :work_orders, :client
  	remove_reference :work_orders, :production_order
  	remove_column :work_orders, :address
    remove_column :work_order_details, :description
    add_column :work_orders, :reference, :string
    add_column :work_orders, :number, :string
  end
end
