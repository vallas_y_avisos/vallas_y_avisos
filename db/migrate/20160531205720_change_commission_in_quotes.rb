class ChangeCommissionInQuotes < ActiveRecord::Migration
  def self.up
  	rename_column :quotes, :commission, :commission_quote
  end
  def self.down
    rename_column :quotes, :commission_quote, :commission
  end
end
