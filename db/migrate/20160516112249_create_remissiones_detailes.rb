class CreateRemissionesDetailes < ActiveRecord::Migration
  def change
    create_table :remissions_details do |t|
    	t.integer :quantity

      t.timestamps
    end
    add_reference :remissions_details, :remission, index: true
    add_foreign_key :remissions_details, :remissions

    add_reference :remissions_details, :product, index: true
    add_foreign_key :remissions_details, :products
  end

  def self.down  
    remove_foreign_key :remissions_details, :remissions
    remove_foreign_key :remissions_details, :products

    drop_table :remissions_details

  end
end
