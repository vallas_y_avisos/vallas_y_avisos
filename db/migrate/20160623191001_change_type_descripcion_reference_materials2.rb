class ChangeTypeDescripcionReferenceMaterials2 < ActiveRecord::Migration
  def up
    change_table :materials_lists do |t|
      t.change :denomination, :text
      t.change :product_description, :text   
    end
  end
end
