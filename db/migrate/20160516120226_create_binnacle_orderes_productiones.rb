class CreateBinnacleOrderesProductiones < ActiveRecord::Migration
  def change
    create_table :binnacle_orders_productions do |t|
    	t.text :description

      t.timestamps
    end
    add_reference :binnacle_orders_productions, :production_order, index: true
    add_foreign_key :binnacle_orders_productions, :production_orders
  end

  def self.down  
    remove_foreign_key :binnacle_orders_productions, :production_orders

    drop_table :binnacle_orders_productions

  end
end
