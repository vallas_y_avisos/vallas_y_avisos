class AddWarehouseClosingFieldsToProductionOrder < ActiveRecord::Migration
  def change
    add_column :production_orders, :warehouse_closed, :boolean, default: false
    
    add_column :production_orders, :warehouse_aditional_material, :boolean
    add_column :production_orders, :warehouse_aditional_material_comment, :string

    add_column :production_orders, :warehouse_material_changed, :boolean
    add_column :production_orders, :warehouse_material_changed_comment, :string

    add_column :production_orders, :machine_closed, :boolean, default: false
    
    add_column :production_orders, :machine_aditional_material, :boolean
    add_column :production_orders, :machine_aditional_material_comment, :string

    add_column :production_orders, :machine_material_changed, :boolean
    add_column :production_orders, :machine_material_changed_comment, :string

    add_column :production_orders, :machine_machine_changed, :boolean
    add_column :production_orders, :machine_machine_changed_comment, :string

    add_column :production_orders, :machine_art, :boolean
    add_column :production_orders, :machine_art_comment, :string

  end
end
