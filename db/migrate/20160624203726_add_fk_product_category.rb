class AddFkProductCategory < ActiveRecord::Migration
  def up
    add_reference :products, :categorie, index: true
    add_foreign_key :products, :categories
    remove_column :products, :price
  end
end
