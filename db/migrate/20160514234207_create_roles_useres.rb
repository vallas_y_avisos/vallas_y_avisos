class CreateRolesUseres < ActiveRecord::Migration
  def self.up
    create_table :roles_users do |s|
      s.timestamps
    end
    add_reference :roles_users, :rol, index: true
    add_reference :roles_users, :user, index: true
    add_foreign_key :roles_users, :roles
    add_foreign_key :roles_users, :users
  end
  def self.down  
    remove_reference :roles_users, :rol
    remove_reference :roles_users, :user
    #remove_foreign_key :roles_users, :roles
    #remove_foreign_key :roles_users, :users

    drop_table :roles_users

  end
end



