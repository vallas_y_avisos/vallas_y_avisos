class AddStatusOrderProduction < ActiveRecord::Migration
  def change
  	create_table :history_order_production_statuses do |t|
      t.string :status   
      t.timestamps 
    end

    add_reference :history_order_production_statuses, :administrador, index: true
    add_foreign_key :history_order_production_statuses, :administradores
    add_reference :history_order_production_statuses, :production_order, index: true
    add_foreign_key :history_order_production_statuses, :production_orders
  end
end
