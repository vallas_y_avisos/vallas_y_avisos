class AddColumnAsignedToAnalistToQuote < ActiveRecord::Migration
  def change
    add_column :quotes, :begin_analysis, :timestamp, default: nil
  end
end
