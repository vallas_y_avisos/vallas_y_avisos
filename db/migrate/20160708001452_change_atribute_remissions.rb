class ChangeAtributeRemissions < ActiveRecord::Migration
  def change
  	remove_column :remissions, :reference
  	remove_reference :remissions, :client

  	rename_column :remissions, :number, :number_remission
  	rename_column :remissions, :date, :date_remission

  	add_reference :remissions, :production_order, index: true
    add_foreign_key :remissions, :production_orders
  end
end
