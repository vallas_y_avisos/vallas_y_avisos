class AddDificultyToProductionOrder < ActiveRecord::Migration
  def change
    add_reference :production_orders, :difficulty_quote, index: true
  end
end
