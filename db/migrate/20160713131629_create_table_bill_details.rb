class CreateTableBillDetails < ActiveRecord::Migration
  def change
    create_table :bill_details do |t|
    	t.integer :quantity
    	t.string  :description

    	t.timestamp
    end
    add_reference :bill_details, :bill, index: true
    add_foreign_key :bill_details, :bills
    add_reference :bill_details, :product, index: true
    add_foreign_key :bill_details, :products
  end
end
