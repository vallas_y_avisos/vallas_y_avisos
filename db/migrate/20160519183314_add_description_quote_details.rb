class AddDescriptionQuoteDetails < ActiveRecord::Migration
  def self.up
    add_column :quote_details, :description, :string
  end
end
