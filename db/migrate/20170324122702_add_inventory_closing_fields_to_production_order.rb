class AddInventoryClosingFieldsToProductionOrder < ActiveRecord::Migration
  def change
    add_column :production_orders, :inventory_closed, :boolean, default: false
    
    add_column :production_orders, :inventory_elements_out, :boolean
    add_column :production_orders, :inventory_elements_out_comment, :string

    add_column :production_orders, :inventory_elements_in, :boolean
    add_column :production_orders, :inventory_elements_in_comment, :string

    add_column :production_orders, :inventory_material_destruction, :boolean
    add_attachment :production_orders, :inventory_material_destruction_attachment
    add_column :production_orders, :inventory_material_destruction_comment, :string
  end
end
