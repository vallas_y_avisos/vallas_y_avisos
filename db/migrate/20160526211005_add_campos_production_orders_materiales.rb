class AddCamposProductionOrdersMateriales < ActiveRecord::Migration
  def self.up
    add_column :production_orders_materiales, :materials_unit, :string
    add_column :production_orders_materiales, :description, :string
    add_column :production_orders_materiales, :observation, :string
  end
end
