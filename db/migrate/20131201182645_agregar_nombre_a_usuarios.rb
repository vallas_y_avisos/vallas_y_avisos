class AgregarNombreAUsuarios < ActiveRecord::Migration
  def change
    add_column :usuarios, :nombre, :string, null: false, limit: 30, default: 'Nombre'
    add_column :usuarios, :apellido, :string, null: false, limit: 30, default: 'Apellido'
  end
end