class AddTableWorkOrderComments < ActiveRecord::Migration
  def self.up
  	create_table :work_order_comments do |t|
      t.text :comment
      t.references :administrador
      t.references :work_order
      t.timestamps
    end

    add_index :work_order_comments, :work_order_id
    add_index :work_order_comments, :administrador_id
  end
  def self.down
    drop_table :work_order_comments
  end
end
