class AddProductIdQuotesDetailes < ActiveRecord::Migration
  def self.up
    add_reference :quote_details, :product, index: true
    add_foreign_key :quote_details, :products
  end
end
