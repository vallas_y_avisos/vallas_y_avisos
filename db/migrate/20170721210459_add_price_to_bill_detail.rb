class AddPriceToBillDetail < ActiveRecord::Migration
  def change
    add_column :bill_details, :price, :float
  end
end
