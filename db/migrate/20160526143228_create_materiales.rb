class CreateMateriales < ActiveRecord::Migration
  def change
    create_table :materiales do |t|
      t.string :description
      t.integer :status

      t.timestamps
    end
  end
end
