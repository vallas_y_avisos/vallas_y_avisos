class AddRolesInAdministradores < ActiveRecord::Migration
   def self.up
  	add_column :administradores, :rol, :string
  	add_reference :administradores, :department, index: true
    add_foreign_key :administradores, :departments

  end 
  def self.down
    remove_column :administradores, :rol
    remove_reference :administradores, :department
  end
end
