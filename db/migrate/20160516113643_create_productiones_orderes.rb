class CreateProductionesOrderes < ActiveRecord::Migration
  def change
    create_table :production_orders do |t|
    	t.string :number, :limit => 100
    	t.date :date_elaboration
    	t.date :delivery_date_printing
    	t.date :actual_delivey_date
    	t.float :agency_percentage
    	t.string :reference
    	t.integer :number_photos
    	t.string :request_to, :limit => 100
    	t.float :advance
    	t.string :contract_number, :limit => 100
    	t.string :way_pay, :limit => 100
    	t.string :sale_cc, :limit => 100
    	t.string :lease_cc, :limit => 100
    	t.string :service_cc, :limit => 100
    	t.string :code_fence, :limit => 100
    	t.integer :sheet
    	t.integer :angle
    	t.string :type, :limit => 100
    	t.boolean :lighting
    	t.integer :quantity
    	t.float :unit_value
    	t.text :site_installation
    	t.text :address
    	t.string :ask_for, :limit => 100
    	t.string :phone, :limit => 30
    	t.text :specifications
    	t.text :addition
    	t.text :cost_observations
    	t.text :production_observations
    	t.text :overcost
    	t.text :sketch
    	t.string :contractor, :limit => 100

      t.timestamps
    end
    add_reference :production_orders, :agency, index: true
    add_foreign_key :production_orders, :agencies

    add_reference :production_orders, :adviser, index: true
    add_foreign_key :production_orders, :advisers

    add_reference :production_orders, :client, index: true
    add_foreign_key :production_orders, :clients

    add_reference :production_orders, :remission, index: true
    add_foreign_key :production_orders, :remissions

    add_reference :production_orders, :quote, index: true
    add_foreign_key :production_orders, :quotes

    add_reference :production_orders, :user, index: true
    add_foreign_key :production_orders, :users

  end

  def self.down  
    remove_foreign_key :production_orders, :agencies
    remove_foreign_key :production_orders, :advisers
    remove_foreign_key :production_orders, :clients
    remove_foreign_key :production_orders, :remissions
    remove_foreign_key :production_orders, :quotes
    remove_foreign_key :production_orders, :users

    drop_table :production_orders

  end
end



