class DeleteTypeAdvisers < ActiveRecord::Migration
  def self.up
    remove_column :advisers, :type
  end
end
