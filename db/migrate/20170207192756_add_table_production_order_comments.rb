class AddTableProductionOrderComments < ActiveRecord::Migration
   def self.up
  	create_table :production_order_comments do |t|
      t.text :comment
      t.references :administrador
      t.references :production_order
      t.timestamps
    end

    add_index :production_order_comments, :production_order_id
    add_index :production_order_comments, :administrador_id
  end
  def self.down
    drop_table :production_order_comments
  end
end
