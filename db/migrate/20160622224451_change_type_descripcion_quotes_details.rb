class ChangeTypeDescripcionQuotesDetails < ActiveRecord::Migration
  def up
    change_table :quote_details do |t|
      t.change :description, :string, :limit => 10000
    end
  end
end
