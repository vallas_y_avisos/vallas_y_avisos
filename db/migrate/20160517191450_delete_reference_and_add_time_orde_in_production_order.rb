class DeleteReferenceAndAddTimeOrdeInProductionOrder < ActiveRecord::Migration
  def self.up
  	add_column :production_orders, :time_order, :time
  end
  def self.down
    remove_column :production_orders, :time_order
  end
end
