class RemoveContractNumberOfProductionOrders < ActiveRecord::Migration
  def self.up
    remove_column :production_orders, :contract_number
  end
  def self.down
  	add_column :production_orders, :contract_number, :string, :limit => 100
  end
end
