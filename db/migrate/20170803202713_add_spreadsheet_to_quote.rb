class AddSpreadsheetToQuote < ActiveRecord::Migration
  def change
    add_attachment :quotes, :spreadsheet
  end
end
