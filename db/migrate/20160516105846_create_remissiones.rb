class CreateRemissiones < ActiveRecord::Migration
  def change
    create_table :remissions do |t|
    	t.string :number, :limit =>100
    	t.date :date
    	t.string :reference
    	t.text  :observations
    	t.text   :delivery

      t.timestamps
    end
    add_reference :remissions, :client, index: true
    add_foreign_key :remissions, :clients

    add_reference :remissions, :user, index: true
    add_foreign_key :remissions, :users
  end

  def self.down  
    remove_foreign_key :remissions, :clients
    remove_foreign_key :remissions, :users

    drop_table :remissions

  end
end
