class CreateQuoteDetailsTypes < ActiveRecord::Migration
  def change
    create_table :quote_details_types do |t|
      t.references :quote_detail, index: true
      t.references :type_description, index: true

      t.timestamps
    end
  end
end
