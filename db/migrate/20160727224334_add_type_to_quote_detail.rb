class AddTypeToQuoteDetail < ActiveRecord::Migration
  def change
    add_reference :quote_details, :type, index: true, null: true
  end
end
