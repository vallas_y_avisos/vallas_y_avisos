class CreateAlertUsers < ActiveRecord::Migration
  def change
    create_table :alert_users do |t|
      t.boolean :viewed
      t.boolean :sent     
      t.timestamps 
    end
    add_reference :alert_users, :alert, index: true
    add_foreign_key :alert_users, :alerts
    add_reference :alert_users, :administrador, index: true
    add_foreign_key :alert_users, :administradores
  end
end
