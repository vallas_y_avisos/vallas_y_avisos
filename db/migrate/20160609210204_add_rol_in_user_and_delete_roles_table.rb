class AddRolInUserAndDeleteRolesTable < ActiveRecord::Migration
  def self.up
  	drop_table :roles_users
  	drop_table :roles
  	add_column :users, :rol, :string

  end
  def self.down
    create_table :roles do |t|
      t.string   :name
      t.string  :color
     
      t.timestamps
    end
    add_index :roles, [:name]

    create_table :roles_users do |s|
      s.timestamps
    end
    add_reference :roles_users, :rol, index: true
    add_reference :roles_users, :user, index: true
    add_foreign_key :roles_users, :roles
    add_foreign_key :roles_users, :users
  
    remove_column :users, :rol
  end

  
end


