class AddNameMaterialListMateriales < ActiveRecord::Migration
  def self.up
    add_column :production_orders_materiales, :materials_name, :string
    remove_column :production_orders_materiales, :material_id
  end
end
