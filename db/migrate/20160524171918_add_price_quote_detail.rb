class AddPriceQuoteDetail < ActiveRecord::Migration
  def self.up
    add_column :quote_details, :price, :float
    remove_column :quote_details, :description
  end
end
