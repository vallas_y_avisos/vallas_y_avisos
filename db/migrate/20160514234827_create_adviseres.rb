class CreateAdviseres < ActiveRecord::Migration
	def self.up
    create_table :advisers do |t|
      t.string :name
      t.string :phone, :limit => 20
      t.string :address
      t.string :type, :limit => 20

      t.timestamps
    end
    add_reference :advisers, :agency, index: true
    add_foreign_key :advisers, :agencies
    
  end
  def self.down  
    remove_foreign_key :advisers, :agencies
    drop_table :advisers

  end
end