class AddWorkOrderStatus < ActiveRecord::Migration
  def self.up
    add_column :work_orders, :status_order, :string
  end
  def self.down
    remove_column :work_orders, :status_order
  end
end
