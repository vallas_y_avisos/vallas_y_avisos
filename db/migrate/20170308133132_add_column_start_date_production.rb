class AddColumnStartDateProduction < ActiveRecord::Migration
  def change
  	add_column :production_orders, :start_date_production, :date
  end
end
