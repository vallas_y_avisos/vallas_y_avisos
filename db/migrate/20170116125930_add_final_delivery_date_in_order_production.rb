class AddFinalDeliveryDateInOrderProduction < ActiveRecord::Migration
  def change
  	add_column :production_orders, :final_delivery_date, :date
  end
end
