class CreateProductOrdersMateriales < ActiveRecord::Migration
  def change
    create_table :production_orders_materiales do |t|
      t.integer :quantity

      t.timestamps
    end
    add_reference :production_orders_materiales, :production_order, index: true
    add_foreign_key :production_orders_materiales, :production_orders

    add_reference :production_orders_materiales, :material, index: true
    add_foreign_key :production_orders_materiales, :materiales
    end
  end
