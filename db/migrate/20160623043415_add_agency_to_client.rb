class AddAgencyToClient < ActiveRecord::Migration
  def change
    add_column :clients, :agency, :boolean
  end
end
