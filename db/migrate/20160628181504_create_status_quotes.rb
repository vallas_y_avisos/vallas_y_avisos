class CreateStatusQuotes < ActiveRecord::Migration
  def change
    create_table :status_quotes do |t|
      t.string :status

      t.timestamps
    end
    add_reference :status_quotes, :quote, index: true
    add_foreign_key :status_quotes, :quotes
  end
end
