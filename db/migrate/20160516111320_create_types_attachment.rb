class CreateTypesAttachment < ActiveRecord::Migration
  def change
    create_table :types_attachments do |t|
    	t.string :name
    	t.boolean :status 

      t.timestamps
    end
  end
  def self.down  
    drop_table :types_attachments
  end
end
