class AddColumnStatusInMaterialsList < ActiveRecord::Migration
  def self.up
    add_column :materials_lists, :status, :string
  end
  def self.down
    remove_column :materials_lists, :status
  end
end
