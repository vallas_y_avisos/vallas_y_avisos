class DifficultyCoordQuotes < ActiveRecord::Migration
  def change
    add_reference :quotes, :difficulty_quote, index: true
    add_foreign_key :quotes, :difficulty_quotes
    add_column :quotes, :coordinator_id, :integer
  end
end
