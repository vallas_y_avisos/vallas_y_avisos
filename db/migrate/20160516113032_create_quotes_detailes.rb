class CreateQuotesDetailes < ActiveRecord::Migration
  def change
    create_table :quote_details do |t|
    	t.integer :version_number
    	t.integer :quantity
    	t.float :unit_value

      t.timestamps
    end
    add_reference :quote_details, :quote, index: true
    add_foreign_key :quote_details, :quotes

    add_reference :quote_details, :product, index: true
    add_foreign_key :quote_details, :products

  end

  def self.down  
    remove_foreign_key :quote_details, :quotes
    remove_foreign_key :quote_details, :products

    drop_table :quote_details

  end
end
