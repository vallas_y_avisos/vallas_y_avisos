class CreateMaterialesDetails < ActiveRecord::Migration
  def change
    create_table :materiales_details do |t|
      t.string :materials_name
      t.float :quantity
      t.string :materials_unit
      t.string :description
      t.string :observation
      t.string :materials_reference

      t.timestamps
    end
    add_reference :materiales_details, :materials_list, index: true
    add_foreign_key :materiales_details, :materials_lists
  end
end
