class CreateMaterialsListDetails < ActiveRecord::Migration
  def change
    create_table :materials_list_details do |t|
      t.float :quantity
      t.string :materials_unit
      t.string :description
      t.string :observation
      t.string :materials_reference

      t.timestamps
    end
    add_reference :materials_list_details, :materials_list, index: true
    add_foreign_key :materials_list_details, :materials_lists

    add_reference :materials_list_details, :material, index: true
    add_foreign_key :materials_list_details, :materiales
  end
end
