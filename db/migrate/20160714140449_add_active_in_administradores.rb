class AddActiveInAdministradores < ActiveRecord::Migration
  def change
  	add_column :administradores, :is_active, :boolean, :default => true
  end
end
