class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
    	t.string :number, :limit => 100
    	t.date :date
    	t.text :reference
    	t.float :commission
    	t.string :form_delivery, :limit => 100
    	t.string :method_payment,:limit => 100
    	t.timestamp :date_delivery_costs 
    	t.timestamp :date_fingering

      t.timestamps
    end
    add_reference :quotes, :adviser, index: true
    add_foreign_key :quotes, :advisers

    add_reference :quotes, :client, index: true
    add_foreign_key :quotes, :clients

    add_reference :quotes, :agency, index: true
    add_foreign_key :quotes, :agencies

    add_reference :quotes, :user, index: true
    add_foreign_key :quotes, :users

  end
  def self.down  
    remove_foreign_key :quotes, :clients
    remove_foreign_key :quotes, :users
    remove_foreign_key :quotes, :advisers
    remove_foreign_key :quotes, :agencies

    drop_table :quotes

  end
end
