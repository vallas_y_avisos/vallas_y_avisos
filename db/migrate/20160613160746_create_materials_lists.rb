class CreateMaterialsLists < ActiveRecord::Migration
  def change
    create_table :materials_lists do |t|
      t.string :op
      t.date :date_delivery
      t.string :cc
      t.string :denomination
      t.string :size_comp
      t.integer :quantity
      t.float :length_comp
      t.float :width_comp
      t.date :date_elaboration
      t.string :prepared_by
      t.string :fototelones_value
      t.string :product_description

      t.timestamps
    end
  end
end
