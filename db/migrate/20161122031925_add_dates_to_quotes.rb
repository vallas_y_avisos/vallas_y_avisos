class AddDatesToQuotes < ActiveRecord::Migration
  def change
    add_column :quotes, :date_delivered_customer, :date, default: nil
    add_column :quotes, :rejected_by, :string, default: ""
  end
end
