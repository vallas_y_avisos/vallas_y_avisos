class CreateTableBills < ActiveRecord::Migration
  def change
    create_table :bills do |t|
    	t.string :bill_number, :limit => 100
    	t.date  :date_bill
    	t.text :observations 
  
    	t.timestamp
    end
     add_reference :bills, :administrador, index: true
    add_foreign_key :bills, :administradores
    add_reference :bills, :production_order, index: true
    add_foreign_key :bills, :production_orders
  end
end
