class CreateAnexeOrderesProductiones < ActiveRecord::Migration
  def change
    create_table :anexe_orders_productions do |t|

      t.timestamps
    end
    add_reference :anexe_orders_productions, :production_order, index: true
    add_foreign_key :anexe_orders_productions, :production_orders

    add_reference :anexe_orders_productions, :type_attachments, index: true
    add_foreign_key :anexe_orders_productions, :types_attachments

  end
  def self.down  
    remove_foreign_key :anexe_orders_productions, :production_orders
    remove_foreign_key :anexe_orders_productions, :types_attachments

    drop_table :anexe_orders_productions

  end
end
