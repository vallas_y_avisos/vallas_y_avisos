class FixTableAnexesOrdersProduction < ActiveRecord::Migration
  def self.up
  	rename_table :anexe_orders_productions, :production_order_anexes
  	add_attachment :production_order_anexes, :anexo
  	
    
  end
  def self.down
  	remove_attachment :production_order_anexes, :anexo
  	rename_table :production_order_anexes, :anexe_orders_productions
  end
end


