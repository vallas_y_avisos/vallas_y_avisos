class CreateClients < ActiveRecord::Migration
  def self.up
    create_table :clients do |t|
      t.string :name
      t.string :nit, :limit => 30
      t.string :email
      t.string :contact, :limit => 30
      t.string :office
      t.string :phone, :limit => 20
      t.string :address
      

      t.timestamps
    end
    add_index :clients, [:name]
  end
  def self.down  
   	remove_index :clients, [:name]
    drop_table :clients

  end
end