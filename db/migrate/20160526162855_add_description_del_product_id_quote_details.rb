class AddDescriptionDelProductIdQuoteDetails < ActiveRecord::Migration
  def self.up
    add_column :quote_details, :description, :string
    remove_column :quote_details, :product_id
  end
end
