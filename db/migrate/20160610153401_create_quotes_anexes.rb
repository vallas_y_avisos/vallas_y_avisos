class CreateQuotesAnexes < ActiveRecord::Migration
  def change
    create_table :quotes_anexes do |t|

      t.timestamps
    end
    add_reference :quotes_anexes, :quote, index: true
    add_foreign_key :quotes_anexes, :quotes 
  end
end
