namespace :migracion do

  desc "Migra las fuentes de financiamiento"
  task :fuentes_fondos => :environment do
    FondoFinanciamiento.destroy_all  
    FuenteFinanciamiento.destroy_all
    File.readlines(File.join(Rails.root, "db", "csv", "fuentes.csv")).each do |l|
      data = l.chomp.split(';')      
      FuenteFinanciamiento.create nombre: data[1], id: data[0]
      puts "Creada fuente de financiamiento #{data[1]}"
    end
    File.readlines(File.join(Rails.root, "db", "csv", "fondos.csv")).each do |l|
      data = l.chomp.split(';')
      FondoFinanciamiento.create nombre: data[0], fuente_financiamiento_id: data[1]
      puts "Creado fondo de financiamiento #{data[0]}"
    end    
  end

end