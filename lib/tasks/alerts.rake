namespace :alerts do
  desc "TODO"
  task alerts_sent_to_cost: :environment do

     
      @quotes = Quote.where("status in ('sent_to_cost')").order(id: :desc)       
      @quotes.each do |value|                    
        @elaborated = StatusQuote.where("quote_id= ? and status= 'elaborated'", value.id)
        @sent_to_cost = StatusQuote.where("quote_id= ? and status= 'sent_to_cost'", value.id)
        if @elaborated.count > 0 && @sent_to_cost.count > 0
            if @elaborated[0].created_at  && @sent_to_cost[0].created_at               
              @dias =  ((@sent_to_cost[0].created_at - @elaborated[0].created_at)/86400).to_i       
              if @dias >=3
                @alert = Alert.where("type_alert='Demora' and model_id= ?", value.id )
                if   @alert.count == 0
                  Alert.create :type_alert =>"Demora" , :description =>"Demora en Cotizacion  ", :model => 'quote', :model_id => value.id, :days => @dias 
                end  
              end  
            end  
        end   #byebug 
      end  #fin del do de  @quotes          
 

  end  #fin task alerts_sent_to_cost

  desc "TODO"
  task alerts_alerts_approved: :environment do

    
      @quotes = Quote.where("status in ('approved')").order(id: :desc)          
      @quotes.each do |value|                    
        @delivered_customer = StatusQuote.where("quote_id= ? and status= 'delivered_customer'", value.id)
        @approved = StatusQuote.where("quote_id= ? and status= 'approved'", value.id)
        if @delivered_customer.count > 0 && @approved.count > 0
            if @delivered_customer[0].created_at  && @approved[0].created_at                
              @dias =  ((@approved[0].created_at - @delivered_customer[0].created_at)/86400).to_i                     
              @alert = Alert.where("type_alert='Tiempo-Aprobar' and model_id= ?", value.id )
              if   @alert.count == 0
                Alert.create :type_alert =>"Tiempo-Aprobar" , :description =>"Los Dias que Tardo el Cliente en Aprobar la Cotizacion desde el momento que se le envio fue de:   ", :model => 'quote', :model_id => value.id , :days => @dias     
              end      
            end 
         end 
      end  #fin del do de  @quotes          
   

  end #fin alerts_alerts_approved

  desc "TODO"
  task alerts_delivered_customer: :environment do

   
      @quotes = Quote.where("status in ('delivered_customer')").order(id: :desc) 
      if @quotes.count > 0       
          @quotes.each do |value|                            
              @dias =  ((Time.now - value.created_at)/86400).to_i        
              #byebug
              if @dias >=10 
                @alert = Alert.where("type_alert='Recordatorio' and model_id= ?", value.id )
                if   @alert.count == 0
                  Alert.create :type_alert =>"Recordatorio" , :description =>"Recuerde volver a llamar al cliente hasta obtenerse una respuesta clara entre aprobada o no aprobada y el por qué ", :model => 'quote', :model_id => value.id , :days => @dias     
                end   
              end     
          end  #fin del do de  @quotes      
      end                
   

  end #fin  task alerts_delivered_customer

  desc "TODO"
  task alerts_users_admin: :environment do

         
      #@alerts =   Alert.where("type_alert in ('Demora','production-order')")    
      @alerts =   Alert.all   
      @alerts.each do |alerts| 
        #creo las alertas para los administradores
        @user_administradores = Administrador.where("rol='admin'")
         @user_administradores.each do |administradores| 
            #busco que la salerta q ya esten registradas
            @alert_user = AlertUser.where("alert_id=? and administrador_id= ?", alerts.id, administradores.id )
            if @alert_user.count == 0
              ActionCorreo.enviar_alertas_email(alerts, administradores).deliver
              AlertUser.create :viewed =>FALSE , :sent =>TRUE, :alert_id => alerts.id, :administrador_id => administradores.id  
            end  # fin del if           
         end #fin de @user_administradores       
      end  # fin de      @alerts         
   

  end #fin task alerts_users_admin

  desc "TODO"
  task alerts_users: :environment do

        
      #@alerts =   Alert.where("type_alert in ('Demora','production-order')")      
      @alerts =   Alert.all
      @alerts.each do |alerts|        
        #creo las alertas para los usuarios involucrados en el cambio de estatus de la cotizacion
        @user_quotes_status = Administrador.joins('LEFT OUTER JOIN status_quotes  ON status_quotes.administrador_id = administradores.id').select('administradores.id, administradores.email, administradores.nombre').distinct.where('status_quotes.quote_id=?',alerts.model_id).reorder('')
         @user_quotes_status.each do |user_quotes| 
            #busco que la salerta q ya esten registradas
            @alert_user = AlertUser.where("alert_id=? and administrador_id= ?", alerts.id, user_quotes.id )          
            if @alert_user.count == 0
              ActionCorreo.enviar_alertas_email(alerts, user_quotes).deliver
              @alerta = AlertUser.create :viewed =>FALSE , :sent =>TRUE, :alert_id => alerts.id, :administrador_id => user_quotes.id  
            end  # fin del if           
         end #fin de @user_quotes                 
      end  # fin de      @alerts        
   

  end  #fin task alerts_users

end #fin namespace :alerts 
